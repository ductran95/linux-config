########################################################################################
 __  __ _       _                 _ 
|  \/  (_)_ __ (_)_ __ ___   __ _| |
| |\/| | | '_ \| | '_ ` _ \ / _` | |
| |  | | | | | | | | | | | | (_| | |
|_|  |_|_|_| |_|_|_| |_| |_|\__,_|_|
                                    
 _   _                  _                 _    ____             __ _           
| | | |_   _ _ __  _ __| | __ _ _ __   __| |  / ___|___  _ __  / _(_) __ _ ___ 
| |_| | | | | '_ \| '__| |/ _` | '_ \ / _` | | |   / _ \| '_ \| |_| |/ _` / __|
|  _  | |_| | |_) | |  | | (_| | | | | (_| | | |__| (_) | | | |  _| | (_| \__ \
|_| |_|\__, | .__/|_|  |_|\__,_|_| |_|\__,_|  \____\___/|_| |_|_| |_|\__, |___/
       |___/|_|                                                      |___/    
#########################################################################################

# You have to change this based on your monitor 
#monitor=DP-1,1920x1080@144,0x0,1
#monitor=,highrr,auto,1
#monitor=,highres,auto,1
monitor = ,preferred,auto,1

# Elements
$hypr_border_size = 2
$hypr_gaps_in = 5
$hypr_gaps_out = 10
$hypr_rounding = 0

# Colors
$gradient_angle = 45deg
$active_border_col_1 = 0xFFB4A1DB
$active_border_col_2 = 0xFFD04E9D
$inactive_border_col_1 = 0xFF343A40
$inactive_border_col_2 = 0xFF343A40
$active_shadow_col = 0x66000000
$inactive_shadow_col = 0x66000000
$group_border_col = 0xFFDB695B
$group_border_active_col = 0xFF4BC66D

#-- General ----------------------------------------------------
general {
	sensitivity = 1.0
	border_size = $hypr_border_size
	no_border_on_floating = false
	gaps_in = $hypr_gaps_in
	gaps_out = $hypr_gaps_out
	col.active_border = $active_border_col_1
	col.inactive_border = $inactive_border_col_1
	cursor_inactive_timeout = 30
	layout = dwindle
	no_cursor_warps = false
	apply_sens_to_raw = false
}

#-- Decoration -------------------------------------------------
decoration {
	rounding = $hypr_rounding
	multisample_edges = true
	active_opacity = 1.0
	inactive_opacity = 1.0
	fullscreen_opacity = 1.0
	blur = false
	blur_size = 8
	blur_passes = 1
	blur_ignore_opacity = false
	blur_new_optimizations =  true
	blur_xray = false
	drop_shadow = true
	shadow_range = 25
	shadow_render_power = 3
	col.shadow = $active_shadow_col
	col.shadow_inactive = $inactive_shadow_col
	shadow_offset = [0 0]
	shadow_scale = 1.0
	dim_inactive = false
	dim_strength = 0.5
	#screen_shader =
}

#-- Animations -------------------------------------------------
animations {
	enabled = true
	animation = windowsIn,1,5,default,popin 0%
	animation = windowsOut,1,5,default,popin
	animation = windowsMove,1,5,default,slide
	animation = fadeIn,1,8,default
	animation = fadeOut,1,8,default
	animation = fadeSwitch,1,8,default
	animation = fadeShadow,1,8,default
	animation = fadeDim,1,8,default
	animation = border,1,10,default
	animation = workspaces,1,5,default,slide
}

#-- Input: Keyboard, Mouse, Touchpad ---------------------------
input {
	kb_model =
	kb_layout =
	kb_variant =
	kb_options =
	kb_rules =
	kb_file =
	numlock_by_default = true
	repeat_rate = 25
	repeat_delay = 600
	sensitivity = 0.5
	accel_profile = adaptive
	force_no_accel = true
	left_handed = false
	scroll_method = 2fg
	scroll_button = 0
	natural_scroll = false
	follow_mouse = 1
	float_switch_override_focus = 1
	touchpad {
		disable_while_typing = true
		natural_scroll = false
		scroll_factor = 1.0
		middle_button_emulation = false
		clickfinger_behavior = false
		tap-to-click = true
		drag_lock = false
	}
	touchdevice {
		transform = 0
		output =
	}
}

#-- Workspace Gestures -----------------------------------------
gestures {
	workspace_swipe = true
	workspace_swipe_fingers = 3
	workspace_swipe_distance = 300
	workspace_swipe_invert = true
	workspace_swipe_min_speed_to_force = 30
	workspace_swipe_cancel_ratio = 0.5
	workspace_swipe_create_new = true
	workspace_swipe_forever = false
}

#-- Miscellaneous ----------------------------------------------
misc {
	disable_hyprland_logo = true
	disable_splash_rendering = false
	# no_vfr = true
	mouse_move_enables_dpms = false
	always_follow_on_dnd = true
	layers_hog_keyboard_focus = true
	animate_manual_resizes = false
	disable_autoreload = false
	enable_swallow = false
	swallow_regex =
	focus_on_activate = true
	no_direct_scanout = false
}

#-- Binds ------------------------------------------------------
binds {
	pass_mouse_when_bound = false
	scroll_event_delay = 300
	workspace_back_and_forth = false
	allow_workspace_cycles = false
}

#-- Layout : Dwindle -------------------------------------------
dwindle {
	pseudotile = false
	# col.group_border = $group_border_col
	# col.group_border_active = $group_border_active_col
	force_split = 0
	preserve_split = false
	special_scale_factor = 0.8
	split_width_multiplier = 1.0
	no_gaps_when_only = false
	use_active_for_splits = true
}

#-- Layout : Master --------------------------------------------
master {
	special_scale_factor = 0.8
	new_is_master = true
	new_on_top = false
	no_gaps_when_only = false
	orientation = left
	inherit_fullscreen = true
}


# Blur for waybar 
blurls=waybar
blurls=lockscreen


########################################################################################

\ \        / (_)         | |                   |  __ \     | |          
  \ \  /\  / / _ _ __   __| | _____      _____  | |__) |   _| | ___  ___ 
   \ \/  \/ / | | '_ \ / _` |/ _ \ \ /\ / / __| |  _  / | | | |/ _ \/ __|
    \  /\  /  | | | | | (_| | (_) \ V  V /\__ \ | | \ \ |_| | |  __/\__ \
     \/  \/   |_|_| |_|\__,_|\___/ \_/\_/ |___/ |_|  \_\__,_|_|\___||___/

########################################################################################


# Float Necessary Windows
windowrule=float,Rofi
windowrule=float,pavucontrol
windowrule = float, yad|nm-connection-editor|pavucontrolk
windowrule = float, polkit-kde-authentication-agent-1|kvantummanager
windowrule = float, feh|Viewnior|Gpicview|Gimp|MPlayer
windowrule = float, VirtualBox Manager|qemu|Qemu-system-x86_64
windowrule = float, title:File Operation Progress
windowrule = float, title:Confirm to replace files
windowrulev2 = float,class:^()$,title:^(Picture in picture)$
windowrulev2 = float,class:^(brave)$,title:^(Save File)$
windowrulev2 = float,class:^(brave)$,title:^(Open File)$
windowrulev2 = float,class:^(LibreWolf)$,title:^(Picture-in-Picture)$
windowrulev2 = float,class:^(blueman-manager)$
windowrulev2 = float,class:^(org.twosheds.iwgtk)$
windowrulev2 = float,class:^(blueberry.py)$
windowrulev2 = float,class:^(xdg-desktop-portal-gtk)$
windowrulev2 = float,class:^(geeqie)$

windowrule = animation slide down,foot-full
windowrule = animation slide up,wlogout

^.*nvim.*$
windowrule=tile,librewolf
windowrule=tile,spotify
windowrule=opacity 1,neovim

###########################################
  ____  _           _ _                 
 |  _ \(_)         | (_)                
 | |_) |_ _ __   __| |_ _ __   __ _ ___ 
 |  _ <| | '_ \ / _` | | '_ \ / _` / __|
 | |_) | | | | | (_| | | | | | (_| \__ \
 |____/|_|_| |_|\__,_|_|_| |_|\__, |___/
                               __/ |    
                              |___/     

###########################################

#-- Keybindings ------------------------------------------------
$notifycmd = notify-send -h string:x-canonical-private-synchronous:hypr-cfg -u low
$term        =  kitty
$menu        = rofi -show drun
#$menu        = ~/.config/hypr/scripts/menu
$volume      = ~/.config/hypr/scripts/volume
$backlight   = ~/.config/hypr/scripts/brightness
$lockscreen  = swaylock
$wlogout     = ~/.config/hypr/scripts/wlogout
$colorpicker = ~/.config/hypr/scripts/colorpicker
$files       = dolphin
$editor      = kate
$browser     = vivaldi-stable

# -- Terminal --
bind = CTRL_ALT,    T, exec, $term
bind = SUPER_SHIFT, Return, exec, $term -f
bind = SUPER_ALT,   Return, exec, $term -s

# -- Apps --
bind = SUPER, E, exec, $files
bind = SUPER, K, exec, $editor
bind = SUPER, B, exec, $browser
bind = SUPER, M, exec, ksysguard
bind = SUPER, P, exec, psensor

# -- Wofi --
bind  = SUPER, D,       exec, $menu

# -- Misc --
bind = SUPER,    N, exec, nm-connection-editor
bind = SUPER,    X, exec, $wlogout
bind = SUPER,    L, exec, $lockscreen
bind = SUPER_CTRL,    P, exec, $colorpicker

# -- Function keys --
bind = ,XF86MonBrightnessUp,   exec, $backlight up
bind = ,XF86MonBrightnessDown, exec, $backlight down
bind = ,XF86AudioRaiseVolume,  exec, $volume up
bind = ,XF86AudioLowerVolume,  exec, $volume down
bind = ,XF86AudioMute,         exec, $volume mute
bind = ,XF86AudioMicMute,      exec, $volume mute
bind = ,XF86AudioNext,         exec, mpc next
bind = ,XF86AudioPrev,         exec, mpc prev
bind = ,XF86AudioPlay,         exec, mpc toggle
bind = ,XF86AudioStop,         exec, mpc stop

# -- Screenshots --
bind = ,      Print, exec, $screenshot --now
bind = ALT,   Print, exec, $screenshot --in5
bind = SHIFT, Print, exec, $screenshot --in10
bind = CTRL,  Print, exec, $screenshot --win
bind = SUPER, Print, exec, $screenshot --area

# -- Hyprland --
bind = SUPER,       Q,      killactive,
bind = SUPER,       C,      killactive,
bind = CTRL_ALT,    Delete, exit,
bind = SUPER,       F,      fullscreen, 0
bind = SUPER,       F,      exec, $notifycmd 'Fullscreen Mode'
bind = SUPER,       S,      pseudo,
bind = SUPER,       S,      exec, $notifycmd 'Pseudo Mode'
bind = SUPER_SHIFT  ,F   ,  fullscreen, 1
bind = SUPER,       Return, togglefloating,
bind = SUPER,       Return, centerwindow,

# Change Focus
bind = SUPER, left,  movefocus, l
bind = SUPER, right, movefocus, r
bind = SUPER, up,    movefocus, u
bind = SUPER, down,  movefocus, d

# Move Active
bind = SUPER_SHIFT, left,  movewindow, l
bind = SUPER_SHIFT, right, movewindow, r
bind = SUPER_SHIFT, up,    movewindow, u
bind = SUPER_SHIFT, down,  movewindow, d

# Resize Active
binde = SUPER_CTRL, left,  resizeactive, -20 0
binde = SUPER_CTRL, right, resizeactive, 20 0
binde = SUPER_CTRL, up,    resizeactive, 0 -20
binde = SUPER_CTRL, down,  resizeactive, 0 20

# Move Active (Floating Only)
binde = SUPER_ALT, left,  moveactive, -20 0
binde = SUPER_ALT, right, moveactive, 20 0
binde = SUPER_ALT, up,    moveactive, 0 -20
binde = SUPER_ALT, down,  moveactive, 0 20

# Switch between windows
bind = ALT, Tab, cyclenext,
bind = ALT, Tab, bringactivetotop,
bind = SUPER, Tab, workspace, m+1
bind = SUPER_SHIFT, Tab, workspace, m-1

# Workspaces
bind = SUPER, 1, workspace, 1
bind = SUPER, 2, workspace, 2
bind = SUPER, 3, workspace, 3
bind = SUPER, 4, workspace, 4
bind = SUPER, 5, workspace, 5
bind = SUPER, 6, workspace, 6
bind = SUPER, 7, workspace, 7
bind = SUPER, 8, workspace, 8
bind = SUPER, 9, workspace, 9

# Send to Workspaces
bind = SUPER_SHIFT, 1, movetoworkspace, 1
bind = SUPER_SHIFT, 2, movetoworkspace, 2
bind = SUPER_SHIFT, 3, movetoworkspace, 3
bind = SUPER_SHIFT, 4, movetoworkspace, 4
bind = SUPER_SHIFT, 5, movetoworkspace, 5
bind = SUPER_SHIFT, 6, movetoworkspace, 6
bind = SUPER_SHIFT, 7, movetoworkspace, 7
bind = SUPER_SHIFT, 8, movetoworkspace, 8
bind = SUPER_SHIFT, 9, movetoworkspace, 9

# Change Workspace Mode
bind = SUPER_CTRL, F, workspaceopt, allfloat
bind = SUPER_CTRL, F, exec, $notifycmd 'Toggled All Float Mode'
bind = SUPER_CTRL, S, workspaceopt, allpseudo
bind = SUPER_CTRL, S, exec, $notifycmd 'Toggled All Pseudo Mode'

# Misc
bind = SUPER_SHIFT, P, pin,
bind = SUPER_SHIFT, P, exec, $notifycmd 'Toggled Pin'
bind = SUPER_SHIFT, S, swapnext
bind = SUPER_SHIFT, O, toggleopaque
bindl = ,switch:Lid Switch, exec, $lockscreen

#-- Mouse Buttons ----------------------------------------------
bindm=SUPER, mouse:272, movewindow
bindm=SUPER, mouse:273, resizewindow

#-- Startup ----------------------------------------------------
# Polkit
exec-once=/usr/lib/polkit-kde-authentication-agent-1

# Status bar :)
exec-once=eww open bar

#Notification
exec-once=dunst

# Wallpaper
exec-once=swaybg -o \* -i ~/.config/hypr/wallpapers/landscape.jpg -m fill

# For keyboard
exec-once=fcitx5 -D

# Bluetooth
exec-once=blueman-applet # Make sure you have installed blueman

# Environment
env = QT_QPA_PLATFORM,wayland
env = QT_QPA_PLATFORMTHEME,qt5ct
env = EDITOR,nano
env = GTK_USE_PORTAL,1
env = PATH,"$PATH:/home/ductran/.dotnet/tools"
env = DATAGRIP_JDK,/opt/jbr
env = RIDER_JDK,/opt/jbr
env = WEBIDE_JDK,/opt/jbr
env = GOLAND_JDK,/opt/jbr
env = INPUT_METHOD,fcitx
env = GTK_IM_MODULE,fcitx
env = QT_IM_MODULE,fcitx
env = XMODIFIERS,@im=fcitx