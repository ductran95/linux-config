# My Arch install on AWS EC2
http://arch-ami-list.drzee.net


# Create EC2

# Update pacman
```bash
sudo pacman-key --init
sudo pacman-key --populate
sudo reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syyu
```

# Install Base devel
```bash
sudo pacman -S base-devel --needed
sudo pacman -S nano
sudo pacman -S git
sudo pacman -S htop
sudo pacman -S neofetch
sudo pacman -S speedtest-cli
```

# Localization
```bash
sudo nano /etc/locale.gen

uncomment en_SG.UTF-8 vi_VN.UTF-8
```
```bash
sudo locale-gen
sudo nano /etc/locale.conf

LANG=en_SG.UTF-8
LC_TIME=vi_VN.UTF-8
LC_ADDRESS=C
LC_COLLATE=C
LC_MONETARY=C
LC_NUMERIC=C
```

# Swapfile (optional)
```bash
sudo dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress
sudo chmod 0600 /swapfile
sudo mkswap -U clear /swapfile
sudo swapon /swapfile
sudo nano /etc/fstab
```

```bash
/swapfile none swap defaults 0 0
```

# Optimize makepfg
```bash
sudo nano /etc/makepkg.conf

MAKEFLAGS="-j$(nproc)"
```

# Yay
```bash
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin/
makepkg -si PKGBUILD
yay --editmenu --nodiffmenu --save
```

# Go
```bash
sudo pacman -S go go-tools
```

# Java
```bash
sudo pacman -S jdk-openjdk
```

# .NET Core
```bash
yay -S dotnet-host-bin dotnet-sdk-bin aspnet-runtime-bin aspnet-targeting-pack-bin
```

# Nodejs
```bash
sudo pacman -S nodejs npm
```

# PostgreSQL
```bash
sudo pacman -S postgresql
```

```bash
sudo su postgres -l
initdb --locale=en_SG.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
```

```bash
sudo nano /var/lib/postgres/data/pg_hba.conf

local   all             postgres                                 peer
host    all             all              0.0.0.0/0               scram-sha-256
host    all             all              ::0/0                   scram-sha-256
```

```bash
sudo nano /var/lib/postgres/data/postgresql.conf

password_encryption = scram-sha-256
listen_addresses = '*'
```

```bash
sudo systemctl start postgresql
sudo su postgres -l
createuser --interactive --pwprompt
exit
sudo systemctl stop postgresql
sudo systemctl enable postgresql
```

# MS SQL Server
- Download https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt

```bash
curl -o gpg-pubkey.txt https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt
sudo gpg --import ./gpg-pubkey.txt
```

```bash
yay -S --mflags --skipinteg libldap24
yay -S mssql-server
```

```bash
sudo /opt/mssql/bin/mssql-conf setup
sudo systemctl stop mssql-server
sudo systemctl enable mssql-server
```

- [Installation guidance for SQL Server on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver15)


# MongoDB
```bash
yay -S mongodb-bin
```

```bash
sudo systemctl start mongodb
mongosh
use admin
```

```bash
db.createUser(
  {
    user: "ductran",
    pwd: "Password",
    roles: [ { role: 'root', db: 'admin' }, "readWriteAnyDatabase" ]
  }
)
```

```bash
sudo nano /etc/mongodb.conf
```

```bash
security:
  authorization: "enabled"
net:
  #bindIp: 127.0.0.1
  bindIpAll: true
```

```bash
sudo systemctl stop mongodb
sudo systemctl enable mongodb
```

# Docker
```bash
sudo pacman -S docker docker-compose
```

```bash
sudo usermod -aG docker arch
sudo systemctl enable docker
```
