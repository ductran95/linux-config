# My Debian 12 ARM64 installation
- Disk space:
  - Root: 150G
  - Data: 50G

# AWS (optional)
```bash
wget https://gitlab.com/ductran95/linux-config/-/raw/master/cloud/install.sh
chmod +x install.sh
./install.sh aws
chsh -s /usr/bin/bash
```

# Install
```bash
wget https://gitlab.com/ductran95/linux-config/-/raw/master/cloud/install.sh
chmod +x install.sh
./install.sh debian
```

# Oracle
```bash
./install.sh oracle
```

# Block drive
```bash
lsblk
sudo cfdisk /dev/sdb
lsblk
sudo mkfs.ext4 -m 0 /dev/sdb1
sudo mkdir /mnt/media
sudo chmod 777 -R /mnt/media
sudo mount /dev/sdb1 /mnt/media
lsblk -dno UUID /dev/sdb1
sudo nano /etc/fstab

UUID=8474c121-a65c-4359-a132-72a41809c3a3 /mnt/media ext4 nofail 0 0
```

# Rclone
```bash
./install.sh media
sudo nano /etc/fuse.conf

uncomment user_allow_other
```

* Reboot

* setup ssh tunnel port: 53682
```bash
ssh -L 53682:127.0.0.1:53682 oracle
```

* follow https://rclone.org/onedrive/
* follow https://rclone.org/drive/

* Google Drive data
```bash
./install.sh rclone-gdrive
```

* Sharepoint
```bash
./install.sh rclone-sharepoint
```

* Copy command (tmux use Ctrl+B+D to detach)
```bash
sudo chmod 777 /home/debian/.config/rclone/rclone.conf
tmux new -s rclone
rclone copy gdrive:Ebook sharepoint:Ebook --ignore-size --ignore-existing --progress --retries-sleep 5s --multi-thread-streams 10
tmux a -t rclone
```

# Arch mirror
```bash
./install.sh mirror
```

# Cloudflare Certificate: copy file to /opt/certs
- tvduc95.ovh.key
- tvduc95.ovh.pem
```bash
sudo mkdir /opt/certs
sudo chmod -R 777 /opt/certs/*
./install.sh ssl
```
# Reboot

# Portainer
```bash
git clone -b portainer https://gitlab.com/ductran95/docker-config.git portainer
cd portainer
docker compose up -d
```

# Portainer stacks
- traefik: https://gitlab.com/ductran95/docker-config.git
- redis: https://gitlab.com/ductran95/docker-config.git
- postgresql: https://gitlab.com/ductran95/docker-config.git
- mongodb: https://gitlab.com/ductran95/docker-config.git
  Note: check mongodb.key, mongodb.pem permission before deploy
- bitwarden: https://gitlab.com/ductran95/docker-config.git
- sso: https://gitlab.com/ductran95/docker-config.git
- database-manager: https://gitlab.com/ductran95/docker-config.git
- open-telemetry: https://gitlab.com/ductran95/docker-config.git
  Note: create DB uptrace in postgresql before start
- adguard: https://gitlab.com/ductran95/docker-config.git
- sftpgo: https://gitlab.com/ductran95/docker-config.git
- jellyfin: https://gitlab.com/ductran95/docker-config.git
- homepage: https://gitlab.com/ductran95/docker-config.git
  Note: create Portainer access token before start
- shopee-management: https://ductran95@dev.azure.com/ductran95/ShopeeManagement/_git/ShopeeManagement
  Note: add Gitlab Registry before start

# Adblock list
- https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
- https://raw.githubusercontent.com/bigdargon/hostsVN/master/option/hosts-VN
- https://adaway.org/hosts.txt
- https://v.firebog.net/hosts/AdguardDNS.txt
- https://v.firebog.net/hosts/Easylist.txt
- https://v.firebog.net/hosts/static/w3kbl.txt


# Enable root
```bash
  sudo -i
  passwd
  nano /etc/ssh/sshd_config

  PermitRootLogin yes
```

# Create new user (optional)
```
  sudo useradd -m -g users test
  sudo cp -r ~/.ssh /home/test
  sudo chown -R test:users /home/test/.ssh
  sudo chmod -R 771 /home/test/.ssh
```

# SSL local (optional)
```bash
mkdir ~/ssl
cd ~/ssl
nano server.conf
```

```bash
[req]
default_bits       = 2048
default_keyfile    = server.key
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = v3_ca

[req_distinguished_name]
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default          = 127.0.0.1
commonName_max              = 64

[req_ext]
subjectAltName = @alt_names

[v3_ca]
subjectAltName = @alt_names
basicConstraints = critical, CA:true
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
keyUsage = keyCertSign, cRLSign, digitalSignature,keyEncipherment

[alt_names]
DNS.1   = localhost
DNS.2   = 127.0.0.1
```

```bash
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout server.key -out server.crt -config server.conf
openssl pkcs12 -export -out server.pfx -inkey server.key -in server.crt
openssl verify -CAfile server.crt server.crt
sudo cp server.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates
openssl verify server.crt
sudo chmod -R 777 ~/ssl
```

# Docker swarm mode (optional)
```bash
docker swarm init
```

# Portainer in Swarm mode (optional)
```bash
docker secret create portainer.sslcert /home/debian/ssl/server.crt
docker secret create portainer.sslkey /home/debian/ssl/server.key
curl -L https://downloads.portainer.io/ee2-17/portainer-agent-stack.yml -o portainer-agent-stack.yml
docker stack deploy -c portainer-agent-stack.yml portainer
sudo mkdir -p /mnt/portainer
sudo chmod -R 777 /mnt/portainer
```

# Redis local (optional)
```bash
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt update
sudo apt install redis -y
sudo systemctl enable redis-server
sudo systemctl start redis-server
```

```bash
sudo nano /etc/redis/redis.conf

bind *

protected-mode no

requirepass 12345678

user ductran on +@all ~* >12345678
```

# PostgreSQL local (optional)
```bash
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt update
sudo apt install postgresql -y

sudo systemctl enable postgresql
sudo systemctl start postgresql

sudo su postgres -l
createuser --interactive --pwprompt
```

```bash
sudo nano /etc/postgresql/15/main/pg_hba.conf

# IPv4 local connections:
host    all             all             0.0.0.0/0            scram-sha-256
# IPv6 local connections:
host    all             all             ::0/0                 scram-sha-256
```

```bash
sudo nano /etc/postgresql/15/main/postgresql.conf

listen_addresses = '*'

password_encryption = scram-sha-256

# DB Version: 15
# OS Type: linux
# DB Type: web
# Total Memory (RAM): 16 GB
# CPUs num: 4
# Connections num: 1000
# Data Storage: ssd

max_connections = 1000
shared_buffers = 4GB
effective_cache_size = 12GB
maintenance_work_mem = 1GB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 1.1
effective_io_concurrency = 200
work_mem = 2097kB
min_wal_size = 1GB
max_wal_size = 4GB
max_worker_processes = 4
max_parallel_workers_per_gather = 2
max_parallel_workers = 4
max_parallel_maintenance_workers = 2
```


# MongoDB local (optional)
```bash
wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
sudo apt update
sudo apt install mongodb-org -y
sudo systemctl enable mongod
sudo systemctl start mongod
```

```bash
openssl rand -base64 756 > mongodb.key
sudo mv mongodb.key /var/lib/mongodb/mongodb.key
sudo chown mongodb:root /var/lib/mongodb/mongodb.key
sudo chmod 400 /var/lib/mongodb/mongodb.key
```

```bash
mongosh

use admin

db.createUser(
  {
    user: "ductran",
    pwd: "12345678",
    roles: [ { role: 'root', db: 'admin' }, "readWriteAnyDatabase" ]
  }
)

```

```bash
sudo nano /etc/mongod.conf

net:
  port: 27017
  bindIp: 0.0.0.0  # Enter 0.0.0.0,:: to bind to all IPv4 and IPv6 addresses or, alternatively, use the net.bindIpAll setting.
  
security:
  authorization: enabled
  keyFile: /var/lib/mongodb/mongodb.key

replication:
  replSetName: rs0

```

```bash
sudo systemctl restart mongod
```

```bash
mongosh

use admin

db.auth("ductran", "12345678")

rs.initiate({
   _id : "rs0",
   members: [
      { _id: 0, host: "127.0.0.1:27017" }
   ]
})

```

# Nginx (optional)
```bash
sudo apt install nginx -y
```

# Let’s Encrypt (optional)
```bash
sudo snap install core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly --nginx
sudo certbot renew --dry-run
```

# Rancher (optional)
```bash
mkdir ~/rancher
cd ~/rancher
nano docker-compose.yml
```

```bash
version: '3.3'

services:
  rancher:
    image: rancher/rancher:latest
    restart: always
    ports:
      - "80:80/tcp"
      - "443:443/tcp"
    volumes:
      - "rancher-data:/var/lib/rancher"
    environment:
      - CATTLE_BOOTSTRAP_PASSWORD=12345678
    privileged: true

volumes:
  rancher-data:
```

# Iptables (optional)
```bash
ip link show
sudo apt install iptables-persistent -y
sudo systemctl enable netfilter-persistent.service
```

```bash
sudo iptables-restore /etc/iptables/rules.v4
sudo iptables-save -f /etc/iptables/rules.v4
```

# Unixbench (optional)
```bash
sudo apt install build-essential libx11-dev libgl1-mesa-dev libxext-dev
wget https://github.com/kdlucas/byte-unixbench/archive/refs/tags/v5.1.3.tar.gz
tar xvfz v5.1.3.tar.gz
cd byte-unixbench-5.1.3/UnixBench
./Run
./Run -c 4
```

# MongoDB benchmark (optional)
```bash
sudo apt install openjdk-17-jdk
git clone https://github.com/idealo/mongodb-performance-test
cd mongodb-performance-test
java -jar ./latest-version/mongodb-performance-test.jar -h localhost -u ductran -p 12345678 -adb admin -db bench -c perf -m insert -o 1000000 -d 1800
java -jar ./latest-version/mongodb-performance-test.jar -h localhost -u ductran -p 12345678 -adb admin -db bench -c perf -m update_many -o 1000000 -d 1800 -t 10
java -jar ./latest-version/mongodb-performance-test.jar -h localhost -u ductran -p 12345678 -adb admin -db bench -c perf -m iterate_many -o 1000000 -d 1800 -t 10
```

```bash
sudo apt install openjdk-17-jdk
git clone https://github.com/tmcallaghan/sysbench-mongodb
cd sysbench-mongodb
wget https://oss.sonatype.org/content/repositories/releases/org/mongodb/mongo-java-driver/3.9.1/mongo-java-driver-3.9.1.jar
wget https://oss.sonatype.org/content/repositories/releases/org/mongodb/mongodb-driver-core/3.9.1/mongodb-driver-core-3.9.1.jar
wget https://oss.sonatype.org/content/repositories/releases/org/mongodb/mongodb-driver-legacy/3.9.1/mongodb-driver-legacy-3.9.1.jar
./run.simple.bash
```

```bash
sudo apt install default-jdk
sudo apt install maven
git clone https://gitlab.com/ongresinc/txbenchmark
cd txbenchmark
mvn clean package
java -jar cli/target/benchmark-1.3.jar --benchmark-target=mongo --target-database-host=localhost --target-database-port=27017 --target-database-user=ductran --target-database-password=12345678 --target-database-name=bench --disable-transaction --iterations=10 --log-level=debug
```

```bash
sudo apt install default-jdk
sudo apt install maven
git clone https://github.com/mongodb-labs/YCSB
cd YCSB/ycsb-mongodb/mongodb

sudo nano pom.xml

  <repositories>
        <repository>
            <id>mapr</id>
            <url>https://repository.mapr.com/nexus/content/groups/mapr-public/</url>
        </repository>
  </repositories>
    <dependency>
      <groupId>com.yahoo.ycsb</groupId>
      <artifactId>core</artifactId>
      <version>${project.version}-mapr-1607</version>
    </dependency>


mvn clean package
./bin/ycsb load mongodb -s -P workloads/workloada > outputLoad.txt
./bin/ycsb run mongodb -s -P workloads/workloada > outputRun.txt
```

```bash
sudo apt install python3
sudo apt install pip
git clone https://github.com/mongodb/mongo-perf
cd mongo-perf
pip install --upgrade pip setuptools==57.5.0
pip install -r requirements.txt
python3 benchrun.py -f testcases/* -t 1 2 4 --includeFilter insert update --trialTime 10
```
