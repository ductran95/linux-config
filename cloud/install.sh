#!/bin/bash
set -x

install-debian() {
    sudo apt update
    sudo apt upgrade -y

    # Install util
    sudo apt install unzip -y
    sudo apt install git -y
    sudo apt install gnupg -y
    sudo apt install neofetch -y
    sudo apt install htop -y
    sudo apt install tmux -y
    sudo apt install iperf3 -y
    git config --global credential.helper store

    # PATH
    echo "export PATH=/usr/sbin:$PATH" | sudo tee -a $HOME/.profile
    echo "export EDITOR=nano" | sudo tee -a $HOME/.profile
    source $HOME/.profile

    # Docker
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update
    sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
    sudo usermod -aG docker $USER
    sudo systemctl enable docker.service
    sudo systemctl enable containerd.service

    # Speedtest
    curl -s https://packagecloud.io/install/repositories/ookla/speedtest-cli/script.deb.sh | sudo bash
    sudo apt install speedtest -y
}

install-ssl() {
  # SSL
  mkdir -p /opt/certs/postgresql
  mkdir -p /opt/certs/otlp
  mkdir -p /opt/certs/mongodb
  mkdir -p /opt/certs/letsencrypt

  cd /opt/certs/postgresql
  cp ../tvduc95.ovh.pem server.crt
  cp ../tvduc95.ovh.key server.key
  sudo chown 0:999 server.crt server.key
  sudo chmod 640 server.crt server.key

  cd /opt/certs/otlp
  cp ../tvduc95.ovh.pem server.crt
  cp ../tvduc95.ovh.key server.key
  sudo chmod 777 server.crt server.key

  cd /opt/certs/mongodb
  openssl rand -base64 756 > mongodb.key
  sudo chown 999:999 mongodb.key
  sudo chmod 400 mongodb.key
  cp ../tvduc95.ovh.pem server.crt
  cp ../tvduc95.ovh.key server.key
  cat {server.crt,server.key} | tee mongodb.pem
  sudo chown 0:999 mongodb.pem
  sudo chmod 640 mongodb.pem
  sudo chown 999:999 mongodb.key
  sudo chmod 400 mongodb.key
}

install-media() {
    # Rclone
    sudo apt install rclone -y
    sudo apt install fuse -y

    # Media folder
    sudo mkdir -p /mnt/media/movies
    sudo mkdir -p /mnt/media/tv
    sudo mkdir -p /mnt/media/cache

    sudo chmod 777 -R /mnt/media/movies
    sudo chmod 777 -R /mnt/media/tv
    sudo chmod 777 -R /mnt/media/cache
}

install-oracle() {
    # Oracle
    sudo apt install snapd -y
    sudo snap install oracle-cloud-agent --classic
}

install-aws() {
    # AWS
    sudo apt update
    sudo apt upgrade -y

    sudo groupadd debian
    sudo useradd -m -g debian -G admin,adm,dialout,cdrom,floppy,sudo,audio,dip,video,plugdev,netdev debian
    sudo cp -r ~/.ssh /home/debian
    sudo chown -R debian:debian /home/debian/.ssh
    sudo chmod -R 700 /home/debian/.ssh
    sudo chmod -R 600 /home/debian/.ssh/authorized_keys
    sudo passwd -d debian
}

install-mirror() {
  sudo apt install rsync -y
  sudo apt install cron -y

  # Cron
  sudo mkdir /opt/cronjobs
  sudo mkdir /opt/logs
  sudo chmod 777 -R /opt/cronjobs
  sudo chmod 777 -R /opt/logs

  wget https://gitlab.com/ductran95/linux-config/-/raw/master/cloud/syncrepo-archlinux.sh
  
  sudo mkdir -p /mnt/mirror/archlinux
  sudo chmod 777 -R /mnt/mirror/archlinux

  sudo mv ./syncrepo-archlinux.sh /usr/bin/syncrepo-archlinux.sh
  sudo chmod 777 /usr/bin/syncrepo-archlinux.sh

  #write out current crontab
  sudo crontab -l > /tmp/sucron
  #echo new cron into cron file
  echo "22 * * * * /usr/bin/syncrepo-archlinux.sh" >> /tmp/sucron
  #install new cron file
  sudo crontab /tmp/sucron
  rm /tmp/sucron
}

install-rclone-gdrive() {
  sudo mkdir /mnt/gdrive
  sudo chmod 777 -R /mnt/gdrive

  rclone config create gdrive drive scope=drive

  sudo rclone --vfs-cache-mode writes --vfs-cache-max-size 30G mount gdrive: /mnt/gdrive --allow-other --config /home/debian/.config/rclone/rclone.conf --cache-dir /mnt/media/cache --daemon --disable copy

  wget https://gitlab.com/ductran95/linux-config/-/raw/master/cloud/rclone-gdrive.service
  
  sudo mv ./rclone-gdrive.service /etc/systemd/system/rclone-gdrive.service
  sudo chmod 777 /etc/systemd/system/rclone-gdrive.service

  sudo systemctl enable rclone-gdrive
}

install-rclone-sharepoint() {
  sudo mkdir /mnt/sharepoint
  sudo chmod 777 -R /mnt/sharepoint

  rclone config create sharepoint onedrive --all

  sudo rclone --vfs-cache-mode writes --vfs-cache-max-size 30G mount sharepoint: /mnt/sharepoint --allow-other --config /home/debian/.config/rclone/rclone.conf --cache-dir /mnt/media/cache --daemon

  wget https://gitlab.com/ductran95/linux-config/-/raw/master/cloud/rclone-sharepoint.service
  
  sudo mv ./rclone-sharepoint.service /etc/systemd/system/rclone-sharepoint.service
  sudo chmod 777 /etc/systemd/system/rclone-sharepoint.service

  sudo systemctl enable rclone-sharepoint
}


case $1 in
debian)
  install-debian
  ;;
oracle)
  install-oracle
  ;;
aws)
  install-aws
  ;;
media)
  install-media
  ;;
ssl)
  install-ssl
  ;;
mirror)
  install-mirror
  ;;
rclone-gdrive)
  install-rclone-gdrive
  ;;
rclone-sharepoint)
  install-rclone-sharepoint
  ;;
*)
  echo "command not found"
  ;;
esac
