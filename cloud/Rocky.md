# My Oracle Linux 8 ARM64 installation

# Update
```bash
sudo dnf update
```

# Repolist
```bash
sudo dnf install nano
```

```bash
sudo nano /etc/dnf/dnf.conf

max_parallel_downloads=10
fastestmirror=True
```

```bash
sudo dnf update --refresh
sudo dnf config-manager --set-enabled crb
sudo dnf install epel-release
```

# Utilites
```bash
sudo dnf install wget
sudo dnf install git
```

# Neofetch
```bash
sudo dnf install neofetch
```

# Htop
```bash
sudo dnf install htop
```

# PostgreSQL
```bash
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -qy module disable postgresql
sudo dnf install -y postgresql15-server
sudo /usr/pgsql-15/bin/postgresql-15-setup initdb
sudo systemctl enable postgresql-15
sudo systemctl start postgresql-15

sudo su postgres -l
createuser --interactive --pwprompt
```

```bash
sudo nano /var/lib/pgsql/15/data/pg_hba.conf

# IPv4 local connections:
host    all             all             0.0.0.0/0            scram-sha-256
# IPv6 local connections:
host    all             all             ::0/0                 scram-sha-256
```

```bash
sudo nano /var/lib/pgsql/15/data/postgresql.conf

listen_addresses = '*'

password_encryption = scram-sha-256

ssl = on
ssl_ca_file = ''
ssl_cert_file = 'server.crt'
#ssl_crl_file = ''
#ssl_crl_dir = ''
ssl_key_file = 'server.key'
```

```bash
sudo su postgres -l
cd /var/lib/pgsql/15/data

openssl req -new -x509 -days 3650 -nodes -text -out server.crt -keyout server.key -subj "/CN=138.2.72.30"
chmod og-rwx server.key

openssl req -new -nodes -text -out root.csr -keyout root.key -subj "/CN=138.2.72.30"
chmod og-rwx root.key

openssl x509 -req -in root.csr -text -days 3650 -extfile /etc/pki/tls/openssl.cnf -extensions v3_ca -signkey root.key -out root.crt

openssl req -new -nodes -text -out server.csr -keyout server.key -subj "/CN=138.2.72.30"
chmod og-rwx server.key

openssl x509 -req -in server.csr -text -days 3650 -CA root.crt -CAkey root.key -CAcreateserial -out server.crt

```

# MongoDB
```bash
sudo nano /etc/yum.repos.d/mongodb-org-6.0.repo 

[mongodb-org-6.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/6.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-6.0.asc
```

```bash
sudo dnf install mongodb-org
sudo systemctl start mongod
sudo systemctl enable mongod
```

```bash
mongosh

use admin

db.createUser(
  {
    user: "ductran",
    pwd: "12345678",
    roles: [ { role: 'root', db: 'admin' }, "readWriteAnyDatabase" ]
  }
)

```

```bash
sudo nano /etc/mongod.conf

net:
  port: 27017
  bindIp: 0.0.0.0  # Enter 0.0.0.0,:: to bind to all IPv4 and IPv6 addresses or, alternatively, use the net.bindIpAll setting.


security:
  authorization: enabled
```

# Redis
```bash
sudo dnf install redis
sudo systemctl start redis
sudo systemctl enable redis
```

```bash
sudo nano /etc/redis.conf

user ductran on +@all ~* >12345678
```

# Docker
```bash
sudo yum-config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker rocky
```

# Disable SELinux & Firewall
```bash
sudo systemctl disable firewalld
sudo setenforce 0
```

```bash
sudo nano /etc/selinux/config

SELINUX=permissive
```

# Open port
* 5432
* 27017
* 6379

# Unixbench
```bash
sudo dnf install gcc gcc-c++ make perl-Time-HiRes
wget https://github.com/kdlucas/byte-unixbench/archive/refs/tags/v5.1.3.tar.gz
tar xvfz v5.1.3.tar.gz
cd byte-unixbench-5.1.3/UnixBench
./Run
./Run -c 4
```