# My Legion 5 Debian KDE installation

# Boot Debian
*  Edit boot/grub/grub.cfg: comment: insmod play play 960 440 1 0 4 440 1
*  Boot Graphic Installer

# Install from ISO
*  Language: English
*  Country: other => Vietnam
*  Locale: Singapore
*  Hostname: legion5
*  Domain name:
*  User full name: Duc Tran
*  User name: ductran
*  Partition: manual => disk 2 => select Free Space => Automatically Partition => All files in one 
*  Mirror: Vietnam
*  Desktop: KDE Plasma
*  Standard system utilities
*  Reboot

# Update user
```bash
su --login
adduser ductran sudo
cd /tmp
wget https://gitlab.com/ductran95/linux-config/-/raw/master/debian/install.sh
chmod +x install.sh
./install.sh config
./install.sh grub-theme
nano /etc/sudoers
(comment Defaults        secure_path)
exit
```

# Setting
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen Locking --> Lock screen automatically: off
*  Workspace Behavior --> Virtual Desktops --> 2 rows
*  Workspace Behavior --> Virtual Desktops --> Main, Develop, Multimedia, Setting
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: Meta+R
*  Shortcut --> Global Shortcut --> KWin --> Make window fullscreen: Meta+F11
*  Shortcut --> Global Shortcut --> KWin --> Maximize window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimize window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Setup window shortcut: Meta+S
*  Shortcut --> Global Shortcut --> KWin --> Show Desktop Grid: Meta+W
*  Shortcut --> Global Shortcut --> KWin --> Suspend Compositing: Untick
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 1: Meta+1
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 2: Meta+2
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 3: Meta+3
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 4: Meta+4
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 5: Meta+5
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 6: Meta+6
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 7: Meta+7
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 8: Meta+8
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 9: Meta+9
*  Shortcut --> Global Shortcut --> KWin --> Toggle Overview: Meta+A
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (All Desktop): Meta+G
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (Current Desktop): Meta+H
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (Window Class): Untick
*  Shortcut --> Global Shortcut --> KWin --> Toggle Window titlebar and frame: Meta+F
*  Shortcut --> Global Shortcut --> KWin --> Walk through desktop: Meta+Tab
*  Shortcut --> Global Shortcut --> KWin --> Walk through desktop (Reverse): Meta+Shift+Tab
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 1: Meta+Shift+1
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 2: Meta+Shift+2
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 3: Meta+Shift+3
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 4: Meta+Shift+4
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 5: Meta+Shift+5
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 6: Meta+Shift+6
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 7: Meta+Shift+7
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 8: Meta+Shift+8
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 9: Meta+Shift+9
*  Shortcut --> Global Shortcut --> Plasma --> Activate Application Laucher: untick Alt+F1
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Add prgram => Konsole
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Acceleration profile: Flat
*  Input Devices --> Touchpad --> Acceleration profile: Flat
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Sleep
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 5 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 15 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown
*  Display and Monitor --> Display Configuration --> Scale: 100%

# Reboot

# Install app
```bash
sudo apt update
sudo apt upgrade
wget https://gitlab.com/ductran95/linux-config/-/raw/master/debian/install.sh
chmod +x install.sh
./install.sh driver
./install.sh kde
./install.sh app (Note: select Yes for Opera)
./install.sh dev
./install.sh disk
./install.sh env-var
```

# Setting
*  Window Management --> Task Switcher --> Thumbnail Grid
*  Startup and Shutdown --> Autostart --> Add program: Yakuake, psensor, System Monitor

# Install global theme with Discover
*  Qogir dark kde theme
*  Qogir light kde theme
*  Dexy-Global
*  Aritim-Dark
*  Aritim-Light

# Install Login Screen with Discover
*  Dexy-Color-SDDM

# Install Splash Screen with Discover
*  Dexy-Color-Splash
*  Peace-Color-Splash
*  Gently-Color-Splash

# Install GTK theme with Discover
*  Qogir theme
*  Dexy-GTK
*  Aritim-Dark
*  Aritim-Light

# Install Konsole theme with Discover
*  Aritim-Dark
*  Aritim-Light

# Install Plasma Widget with Discover
*  Control Centre

# Install rice
```bash
./install.sh rice
```

# Setting theme
*  Global theme: Aritim Dark
*  Application style: Breeze
*  GTK Application style: Aritim Dark GTK
*  Plasma style: Aritim Dark Flat Solid
*  Color: Aritim Dark
*  Decorator: Aritim Dark
*  Icon: Papirus
*  Splash screen: Peace-Color-Splash
*  SDDM: Dexy-Color-SDDM

# Fix GTK
```bash
./install.sh fix-gtk
```

# Config Konsole
*  Switch default theme: Aritim Dark

# Reboot

# Install patch dev
```bash
./install.sh patch-dev
```

# Config fcitx
*  Input device --> Virtual Keyboard --> Fcitx 5
*  Regional Setting --> Input Method: Add Unikey, Pinyin, setup shortcut

# Setting
*  User --> Change avatar
*  Config Desktop Wallpaper --> Slideshow
*  Workspace Behavior --> Screen Locking --> Appearance

# Customize
*  Taskbar: Left
*  System Tray:
   - Show: Notification
   - Hide: Update, Yakuake, psensor, Disk, Display Config, KDE Connect, Clipboard, Media Player, Vault, Weather
   - Disabled: others
*  Add widget: Battery, Audio Volume, Network, Bluetooth, Touchpad, Control Center, Input Method Panel
*  Favorite: System Settings, Dolphin, System Monitor, Discover, Vivaldi, Chrome, Firefox, Opera, Skype, Telegram, Thunderbird, qBittorrent, Strawberry, mpv, jellyfin, stellarium, rider, goland, webstorm, datagrip, GitKraken, Postman, Visual Studio Code, Authy
*  Taskbar: Auto hide

# Plasmoid
*  Analog clock
*  System Monitor
*  Hard disk activity
*  Network speed

*  Update System Monitor plasmoid

```bash
edit with kate ~/.config/plasma-org.kde.plasma.desktop-appletsrc

Add sensor

highPrioritySensorIds=["cpu/all/usage"]
lowPrioritySensorIds=["memory/physical/application"]

Update color

cpu/all/usage=170,85,255
disk/all/read=245,134,41
disk/all/write=57,171,152
network/all/download=131,199,93
network/all/upload=255,92,65
```

# Maintenance

* Remove old kernel
```bash
sudo apt list -i linux-image* linux-headers*
sudo apt --purge remove linux-image-6.1.0-15-amd64
```

*  Update
```bash
./install.sh update
```

*  Pipewire
```bash
aplay -l
cat /proc/asound/cards
cat /proc/asound/card2/stream0
cat /proc/asound/card2/pcm0p/sub0/info
cat /proc/asound/card2/pcm0p/sub0/hw_params

pactl info

pw-top
pw-cli list-objects Node
pw-cli info 59

wpctl status
wpctl inspect 58
```