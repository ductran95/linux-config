#!/bin/bash
set -ex

install-config() {
    echo "Installing Config"

    sed -i '/# en_US.UTF-8 UTF-8/s/^# //g' /etc/locale.gen
    sed -i '/# vi_VN UTF-8/s/^# //g' /etc/locale.gen
    locale-gen
    echo -e 'LANG="en_SG.UTF-8"\nLANGUAGE="en_SG:en"\nLC_TIME=vi_VN.UTF-8\nLC_ADDRESS=C\nLC_COLLATE=C\nLC_MONETARY=C\nLC_NUMERIC=C' | tee /etc/default/locale
    echo 'APT::Sandbox::User "root";' | tee /etc/apt/apt.conf.d/10-sandbox
    echo 'options iwlwifi enable_ini=0' | tee /etc/modprobe.d/iwlwifi.conf
    sed -i 's/PATH="\/usr\/local\/bin:\/usr\/bin:\/bin:\/usr\/local\/games:\/usr\/games"/PATH="\/usr\/local\/sbin:\/usr\/local\/bin:\/usr\/sbin:\/usr\/bin:\/sbin:\/bin:\/usr\/local\/games:\/usr\/games"/g' /etc/profile

    wget https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/bluetooth.service.d/override.conf
    mkdir -p /etc/systemd/system/bluetooth.service.d
    mv override.conf /etc/systemd/system/bluetooth.service.d/override.conf
    
    echo "Install Config completed"
}

install-grub-theme() {
    echo "Installing GRUB Theme"

    cd /tmp
    git clone --depth=1 https://github.com/vinceliuice/grub2-themes.git
    cd grub2-themes
    ./install.sh -b -t tela
    echo "Install GRUB Theme completed"
}

install-driver() {
    echo "Installing Drivers"

    sudo apt install -y mesa-utils vulkan-tools clinfo
    sudo apt install -y wayland-utils
    sudo apt install -y pipewire-audio
    sudo apt install -y linux-cpupower

    echo "Install Drivers completed"
}

install-kde() {
    echo "Installing KDE app"

    sudo apt install -y yakuake ksystemlog filelight kgpg kompare krdc freerdp2-wayland
    sudo apt install -y krita kolourpaint elisa kwave kalzium krename
    sudo apt install -y kwin-addons plasma-runners-addons plasma-wallpapers-addons plasma-widgets-addons
    #sudo apt install -y ksysguard

    echo "Install KDE app completed"
}

install-app() {
    echo "Installing App"

    sudo apt install -y curl
    sudo apt install -y neofetch htop
    sudo apt install -y lm-sensors psensor
    sudo apt install -y calibre qbittorrent
    sudo apt install -y mpv strawberry
    sudo apt install -y subtitleeditor
    sudo apt install -y filezilla thunderbird
    sudo apt install -y stellarium
    sudo apt install -y telegram-desktop
    sudo apt install -y openvpn network-manager-openvpn
    sudo apt install -y desktop-file-utils

    curl -fSsL https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | sudo tee /usr/share/keyrings/google-chrome.gpg >> /dev/null
    echo 'deb [signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list

    curl -fsSL https://repo.vivaldi.com/archive/linux_signing_key.pub | gpg --dearmor | sudo tee /usr/share/keyrings/vivaldi.gpg >> /dev/null
    echo 'deb [signed-by=/usr/share/keyrings/vivaldi.gpg] https://repo.vivaldi.com/archive/deb/ stable main' | sudo tee /etc/apt/sources.list.d/vivaldi.list

    curl -fsSL https://ppa.ablaze.one/KEY.gpg | sudo gpg --dearmor -o /usr/share/keyrings/Floorp.gpg
    sudo curl -sS --compressed -o /etc/apt/sources.list.d/Floorp.list 'https://ppa.ablaze.one/Floorp.list'

    # curl -fsSL https://repo.skype.com/data/SKYPE-GPG-KEY | gpg --dearmor | sudo tee /usr/share/keyrings/skype.gpg >> /dev/null
    # echo 'deb [signed-by=/usr/share/keyrings/skype.gpg] https://repo.skype.com/deb stable main' | sudo tee /etc/apt/sources.list.d/skype.list

    sudo apt update
    
    sudo apt install -y google-chrome-stable
    sudo apt install -y vivaldi-stable
    sudo apt install -y floorp
    # sudo apt install -y skypeforlinux

    install-discord
    install-jellyfin-media-player
    install-bitwarden
    
    sudo apt install -y fonts-liberation fonts-firacode fonts-roboto fonts-terminus fonts-jetbrains-mono fonts-powerline fonts-wqy-microhei fonts-wqy-zenhei
    
    install-meslo
    install-cascadia
    install-source-code-pro

    sudo apt install -y fcitx5 fcitx5-chinese-addons fcitx5-frontend-gtk3 fcitx5-frontend-qt5 fcitx5-unikey kde-config-fcitx5

    sudo apt install -y qt5-style-kvantum qt5-style-kvantum-themes
    sudo apt install -y papirus-icon-theme

    sudo apt install -y zsh zplug
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
    sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k\/powerlevel10k"/g' ~/.zshrc

    # sudo rm -f /etc/apt/sources.list.d/skype-stable.list

    echo "Install App completed"
}

install-bitwarden() {
    curl -o bitwarden.deb -L "https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=deb"
    sudo apt install -y ./bitwarden.deb
    rm -f ./bitwarden.deb
}

install-discord() {
    curl -o discord.deb -L "https://discord.com/api/download?platform=linux&format=deb"
    sudo apt install -y ./discord.deb
    rm -f ./discord.deb
}

install-jellyfin-media-player() {
    version=$(curl --silent "https://api.github.com/repos/jellyfin/jellyfin-media-player/releases/latest" | jq -r '.tag_name' | sed -E 's/v//g')
    curl -o jellyfin-media-player.deb -L https://github.com/jellyfin/jellyfin-media-player/releases/latest/download/jellyfin-media-player_$version-1_amd64-bookworm.deb
    sudo apt install -y ./jellyfin-media-player.deb
    rm -f ./jellyfin-media-player.deb
}

install-meslo() {
    curl -OL https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Meslo.tar.xz
    sudo tar -xvf ./Meslo.tar.xz -C /usr/local/share/fonts
    rm -f ./Meslo.tar.xz
}

install-cascadia() {
    version=$(curl --silent "https://api.github.com/repos/microsoft/cascadia-code/releases/latest" | jq -r '.tag_name' | sed -E 's/v//g')
    curl -o CascadiaCode.zip -L https://github.com/microsoft/cascadia-code/releases/latest/download/CascadiaCode-$version.zip
    unzip ./CascadiaCode.zip -d ./cascadia
    sudo cp ./cascadia/otf/static/* /usr/local/share/fonts
    rm -rf ./cascadia
    rm -f ./CascadiaCode.zip
}

install-source-code-pro() {
    otfLink=$(curl --silent "https://api.github.com/repos/adobe-fonts/source-code-pro/releases/latest" | jq -r '.assets[0].browser_download_url')
    curl -o SourceCodePro.zip -L $otfLink
    unzip ./SourceCodePro.zip -d ./SourceCodePro
    sudo cp ./SourceCodePro/OTF/* /usr/local/share/fonts
    rm -rf ./SourceCodePro
    rm -f ./SourceCodePro.zip
}

install-dev() {
    echo "Installing Develop tool"

    sudo apt install -y clang llvm lldb lld

    sudo apt install -y android-tools-adb android-tools-fastboot
    sudo apt install -y codeblocks
    sudo apt install -y libnss3-tools
  
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/docker.gpg >> /dev/null
    echo "deb [signed-by=/usr/share/keyrings/docker.gpg] https://download.docker.com/linux/debian bookworm stable" | sudo tee /etc/apt/sources.list.d/docker.list
    
    curl -OL https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb
    sudo apt install -y ./packages-microsoft-prod.deb
    rm -f ./packages-microsoft-prod.deb

    curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /usr/share/keyrings/packages.microsoft.gpg >> /dev/null
    echo "deb [signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" | sudo tee /etc/apt/sources.list.d/vscode.list

    sudo gpg -k
    sudo gpg --no-default-keyring --keyring /usr/share/keyrings/k6-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
    echo "deb [signed-by=/usr/share/keyrings/k6-archive-keyring.gpg] https://dl.k6.io/deb stable main" | sudo tee /etc/apt/sources.list.d/k6.list

    curl -fsSL https://pgp.mongodb.com/server-7.0.asc | gpg --dearmor | sudo tee /usr/share/keyrings/mongodb-server-7.0.gpg >> /dev/null
    echo "deb [ signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] http://repo.mongodb.org/apt/debian bullseye/mongodb-org/7.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list

    curl -fsSL https://s3.eu-central-1.amazonaws.com/jetbrains-ppa/0xA6E8698A.pub.asc | gpg --dearmor | sudo tee /usr/share/keyrings/jetbrains-ppa-archive-keyring.gpg >> /dev/null
    echo "deb [signed-by=/usr/share/keyrings/jetbrains-ppa-archive-keyring.gpg] http://jetbrains-ppa.s3-website.eu-central-1.amazonaws.com any main" | sudo tee /etc/apt/sources.list.d/jetbrains-ppa.list
    
    sudo apt update

    sudo apt install -y dotnet-sdk-8.0
    sudo apt install -y dotnet-sdk-7.0
    sudo apt install -y dotnet-sdk-6.0

    sudo apt install -y code

    sudo apt install -y k6

    sudo apt install -y mongodb-mongosh mongodb-database-tools

    install-golang
    install-gcm
    install-gitkraken
    install-azuredatastudio
    install-mongodb-compass
    install-redis-insight
    install-fnm
    install-postman

    sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo usermod -aG docker $USER

    sudo apt install -y datagrip
    sudo apt install -y webstorm
    sudo apt install -y goland
    sudo apt install -y rider

    git config --global user.name "Duc Tran"
    git config --global user.email tv.duc95@gmail.com
    git config --global core.editor "nano -w"
    git config --global core.autocrlf input
    git config --global --add safe.directory '*'
    git-credential-manager configure
    git config --global credential.credentialStore secretservice

    source ~/.bashrc
    fnm use --install-if-missing 22
    fnm use --install-if-missing 20
    fnm use --install-if-missing 18
    
    echo "Install Develop tool completed"
}

install-golang() {
    version=$(curl --silent "https://go.dev/dl/?mode=json" | jq -r '.[0].version')
    sudo rm -rf /usr/local/go
    curl -o go.linux-amd64.tar.gz -L https://go.dev/dl/$version.linux-amd64.tar.gz
    sudo tar -C /usr/local -xzf go.linux-amd64.tar.gz
    rm -f ./go.linux-amd64.tar.gz
}

install-gcm() {
    curl -o gcm-linux_amd64.deb -L https://github.com/git-ecosystem/git-credential-manager/releases/download/v2.4.1/gcm-linux_amd64.2.4.1.deb
    sudo apt install -y ./gcm-linux_amd64.deb
    rm -f ./gcm-linux_amd64.deb
}

install-gitkraken() {
    curl -OL https://release.gitkraken.com/linux/gitkraken-amd64.deb
    sudo apt install -y ./gitkraken-amd64.deb
    rm -f ./gitkraken-amd64.deb
}

install-azuredatastudio() {
    curl -o azuredatastudio-linux.deb -L https://go.microsoft.com/fwlink/?linkid=2251736
    sudo apt install -y ./azuredatastudio-linux.deb
    rm -f ./azuredatastudio-linux.deb
}

install-mongodb-compass() {
    version=$(curl --silent "https://api.github.com/repos/mongodb-js/compass/releases/latest" | jq -r '.tag_name' | sed -E 's/v//g')
    curl -o mongodb-compass_amd64.deb -L https://downloads.mongodb.com/compass/mongodb-compass_$version\_amd64.deb
    sudo apt install -y ./mongodb-compass_amd64.deb
    rm -f ./mongodb-compass_amd64.deb
}

install-redis-insight() {
    curl -OL https://s3.amazonaws.com/redisinsight.download/public/latest/Redis-Insight-linux-amd64.deb
    sudo apt install -y ./Redis-Insight-linux-amd64.deb
    rm -f ./Redis-Insight-linux-amd64.deb
}

install-postman() {
    sudo rm -rf /opt/Postman
    curl -o postman-linux-x64.tar.gz -L https://dl.pstmn.io/download/latest/linux64
    tar -C /tmp -xzf postman-linux-x64.tar.gz
    rm -f ./postman-linux-x64.tar.gz
    sudo mv /tmp/Postman /opt/
    sudo tee -a /usr/share/applications/postman.desktop << END
[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/opt/Postman/Postman
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;
END
}

install-fnm() {
    curl -fsSL https://fnm.vercel.app/install | bash
}


install-env-var() {
    echo "Installing Environment Variables"

    mkdir ~/.config/environment.d
    echo -e 'GTK_USE_PORTAL=1\n INPUT_METHOD=fcitx\n XMODIFIERS=@im=fcitx\n' | tee ~/.config/environment.d/envvars.conf
    
    echo 'export PATH="$PATH:/usr/local/go/bin"' | sudo tee /etc/profile.d/golang.sh
    echo 'export EDITOR=nano' | sudo tee /etc/profile.d/envvars.sh

    echo "emulate sh -c 'source /etc/profile'" | sudo tee -a /etc/zsh/zprofile

    echo 'eval "$(fnm env --use-on-cd)"' >> ~/.bashrc
    echo 'eval "$(fnm env --use-on-cd)"' >> ~/.zshrc

    echo "Install Environment Variables completed"
}

install-rice() {
    echo "Installing rice"

    cd ~

    sed -i 's/Color=54,97,132/Color=19,209,61/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'
    sed -i 's/Blur=true/Blur=false/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'
    sed -i 's/Opacity=0.8/Opacity=1/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'

    sed -i 's/activeBackground=20,26,33,216/activeBackground=20,26,33,255/g' ~/.local/share/color-schemes/AritimDark.colors
    sed -i 's/inactiveForeground=102,106,115,216/inactiveForeground=102,106,115,255/g' ~/.local/share/color-schemes/AritimDark.colors

    rm -f ~/.local/share/plasma/desktoptheme/Aritim-Dark-Flat-Solid/widgets/clock.svgz

    # Font Config
    mkdir -p ~/.config/fontconfig/conf.d/
    curl -o ~/.config/fontconfig/conf.d/99-cjk-noto.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/fontconfig/conf.d/99-cjk-noto.conf

    # MPV
    mkdir -p ~/.config/mpv
    curl -o ~/.config/mpv/input.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/mpv/input.conf
    curl -o ~/.config/mpv/mpv.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/mpv/mpv.conf

    # Konsole
    mkdir -p ~/.local/share/konsole/
    curl -o ~/.local/share/konsole/'Aritim Dark.profile' -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/konsole/Aritim%20Dark.profile
    curl -o ~/.local/share/konsole/'Aritim Light.profile' -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/konsole/Aritim%20Light.profile

    # System Monitor
    mkdir -p ~/.local/share/plasma-systemmonitor/
    curl -o ~/.local/share/plasma-systemmonitor/frequency.page -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/plasma-systemmonitor/frequency.page

    # PipeWire
    mkdir -p ~/.config/pipewire/pipewire.conf.d/
    curl -o ~/.config/pipewire/pipewire.conf.d/pwrate.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/pipewire/pipewire.conf.d/pwrate.conf
    
    # Wireplumber quanlity issue
    # mkdir -p ~/.config/wireplumber/main.lua.d/
    # curl -o ~/.config/wireplumber/main.lua.d/51-alsa-config.lua -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/wireplumber/main.lua.d/51-alsa-config.lua

    echo "Install rice completed"
}

install-fix-gtk() {
    echo "Installing Fix GTK style"

    curl -OL 'https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/window_decorations.css'
    cp -rf ~/.config/gtk-3.0/assets ~/.config/gtk-4.0
    echo "" >> ~/.config/gtk-3.0/gtk.css
    cat window_decorations.css >> ~/.config/gtk-3.0/gtk.css
    cp -f ~/.config/gtk-3.0/gtk.css ~/.config/gtk-4.0

    echo "Install Fix GTK style completed"
}

install-disk() {
    echo "Mounting disks"

    sudo mkdir /mnt/disk1
    sudo mkdir /mnt/disk2
    sudo mkdir /mnt/disk3
    sudo mkdir /mnt/disk4

    echo 'UUID=B69458D094589529   /mnt/disk1  ntfs         defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=CE45-2496          /mnt/disk2  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=F252-07F4          /mnt/disk3  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=4437-86A5          /mnt/disk4  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab

    echo "Mount disks completed"
}

install-patch-dev() {
    echo "Patching dev tool"

    echo "Update Docker DNS"
    echo '{' | sudo tee /etc/docker/daemon.json
    echo '    "dns": ["1.1.1.1", "8.8.8.8"]' | sudo tee -a /etc/docker/daemon.json
    echo '}' | sudo tee -a /etc/docker/daemon.json
    echo "Update Docker DNS completed"

    echo "Import GPG key"
    gpg --import "/mnt/disk2/Certificates/PGP/Tran Van Duc_0x7AAECCA2_SECRET.asc"
    git config --global user.signingkey 1030E3EF7AAECCA2
    git config --global commit.gpgSign true
    echo "Import GPG key completed"

    echo "Install .NET extensions"

    dotnet tool install --global dotnet-ef
    dotnet tool install --global dnt
    dotnet new install Clean.Architecture.Solution.Template
    dotnet new install Ardalis.CleanArchitecture.Template
    dotnet new install IdentityServer4.Templates
    dotnet new install Duende.IdentityServer.Templates
    dotnet new install Avalonia.Templates
    dotnet new install HotChocolate.Templates
    dotnet new install MassTransit.Templates

    echo "Install .NET extensions completed"

    install-dev-ssl

    echo "Install NPM packages"

    npm install -g @angular/cli
    npm install -g vite
    npm install -g azurite

    echo "Install NPM packages completed"

    echo "Apply GitKraken patch"
    cd /mnt/disk2/Projects/GitCracken
    npm i
    npm run-script build
    sudo node dist/bin/gitcracken.js patcher
    echo "Apply GitKraken patch completed"

    echo "Patching dev tool completed"
}

install-dev-ssl() {
    echo "Install .NET SSL"

    dotnet dev-certs https -ep localhost.crt --format PEM

    sudo cp localhost.crt /usr/local/share/ca-certificates/localhost.crt
    sudo update-ca-certificates

    mkdir -p $HOME/.pki/nssdb
    sudo certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n localhost -i /usr/local/share/ca-certificates/localhost.crt
    sudo chmod 777 $HOME/.pki/nssdb/*

    echo "Install .NET SSL completed"
}

install-update() {
    echo "Updating packages"

    sudo apt update
    sudo apt upgrade -y

    # sudo rm -f /etc/apt/sources.list.d/skype-stable.list

    install-discord
    install-jellyfin-media-player    
    install-bitwarden
    install-meslo
    install-cascadia
    install-source-code-pro

    install-golang
    install-gcm
    install-gitkraken
    install-azuredatastudio
    install-mongodb-compass
    install-redis-insight
    install-fnm
    install-postman

    dotnet tool update --global --all
    dotnet new update

    echo "Update packages completed"
}

case $1 in
config)
  install-config
  ;;
grub-theme)
  install-grub-theme
  ;;
driver)
  install-driver
  ;;
kde)
  install-kde
  ;;
app)
  install-app
  ;;
dev)
  install-dev
  ;;
rice)
  install-rice
  ;;
fix-gtk)
  install-fix-gtk
  ;;
disk)
  install-disk
  ;;
env-var)
  install-env-var
  ;;
patch-dev)
  install-patch-dev
  ;;
update)
  install-update
  ;;
*)
  echo "command not found"
  ;;
esac
