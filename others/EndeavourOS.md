# My Legion 5 EndeavourOS KDE installation

# Boot EndeavourOS

# Connect to Internet

# Install EndeavourOS
*  Partition the disks
*  Use KDE desktop
*  Remove intel packages 

# Reboot

# Full system update
```bash
sudo pacman -Syu
sudo pacman -Syyu
```

# Setting
*  Workspace Behavior --> General Behavior --> Animation speed: Instant
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen locking --> Lock screen: off
*  Shortcut --> Global Shortcut --> Defaults
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: untick alt+f2
*  Shortcut --> Global Shortcut --> KWin --> Hide window boder: Meta+Enter
*  Shortcut --> Global Shortcut --> KWin --> Maximum window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimum window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Windows (Current Desktop): Meta+Tab
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Konsole: Ctrl+Alt+T
*  Regional Setting --> Language: English
*  Regional Settings --> Formats --> Detail Setting --> Number: Default
*  Regional Settings --> Formats --> Detail Setting --> Currency: Default
*  Regional Settings --> Date & Time --> Set time auto: True
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Poiter speed: 5
*  Input Devices --> Touchpad --> Poiter speed: 5
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On AC Power --> Wireless: On
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Wireless: On
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 15 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 30 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown

# Update time
```bash
sudo hwclock --systohc --localtime
```

# Localization
```bash
nano /etc/locale.gen

Add vi_VN UTF-8
```
```bash
sudo locale-gen
```
```bash
sudo nano /etc/locale.conf

LANG=en_US.UTF-8
LC_ADDRESS=vi_VN.UTF-8
LC_IDENTIFICATION=vi_VN.UTF-8
LC_MEASUREMENT=vi_VN.UTF-8
LC_MONETARY=C          
LC_NAME=vi_VN.UTF-8
LC_NUMERIC=C
LC_PAPER=vi_VN.UTF-8    
LC_TELEPHONE=vi_VN.UTF-8
LC_TIME=vi_VN.UTF-8
```

# Fix KDE bug
*  Fix check network bug

```bash
sudo nano /usr/lib/NetworkManager/conf.d/20-connectivity.conf
```

```bash
[connectivity]
#uri=http://www.archlinux.org/check_network_status.txt
uri=http://networkcheck.kde.org/
```

[Log in required for ethernet at home?”](https://www.reddit.com/r/ManjaroLinux/comments/keabph/log_in_required_for_ethernet_at_home/)

*  Fix shortcut bug

```bash
sudo nano ~/.config/plasma-org.kde.plasma.desktop-appletsrc
sudo nano ~/.config/kglobalshortcutsrc

Change Alt+F1 => Meta+Alt+F1
```

# Disable PC Speaker
```bash
sudo rmmod pcspkr
sudo nano /etc/modprobe.d/nobeep.conf
```

```bash
# Do not load the 'pcspkr' module on boot.
blacklist pcspkr
```

# Disable KVM to stop shutdown error (optional)
```bash
sudo nano /etc/modprobe.d/nokvm.conf
```

```bash
blacklist kvm_intel
blacklist kvm_amd
blacklist kvm
```

# Automount partition
```bash
GUI (or use pamac install --no-confirm) (optional)

ntfs3-dkms
```

```bash
sudo mkdir /mnt/disk1
sudo mkdir /mnt/disk2
sudo mkdir /mnt/disk3
sudo mkdir /mnt/disk4
sudo blkid /dev/nvme0n1p3
sudo blkid /dev/nvme0n1p5
sudo blkid /dev/nvme0n1p6
sudo blkid /dev/nvme1n1p1
```

```bash
id -u
id -g

sudo nano /etc/fstab

UUID=001C1F2A1C1F19EC   /mnt/disk1  ntfs        defaults 0 0
UUID=82C46777C4676C7B   /mnt/disk2  ntfs        defaults 0 0
UUID=A20A76C80A7698CD   /mnt/disk3  ntfs        defaults 0 0
UUID=5A2CFBC92CFB9E67   /mnt/disk4  ntfs        defaults 0 0

```

# SDDM Auto numlock
```bash
sudo nano /etc/sddm.conf

[General]
Numlock=on
```

# Disable GRUB log 
```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet nowatchdog nvme_load=YES"
```

```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Yay
```bash
yay --editmenu --nodiffmenu --save
yay -R endeavouros-keyring  endeavouros-mirrorlist  eos-plasma-sddm-config  grub2-theme-endeavouros
```

# Pamac
```bash
yay -S pamac-aur
```

# Add AUR Repository
```bash
Add or remove software
Preference
```

# Graphic driver
```bash
GUI (or use pamac install --no-confirm)

xf86-video-amdgpu
vulkan-radeon
nvidia nvidia-utils nvidia-settings
```

# Hardware video acceleration
```bash
GUI (or use pamac install --no-confirm)

libva-mesa-driver
mesa-vdpau
```

# KDE Plasma
```bash
GUI (or use pamac install --no-confirm)

--needed plasma
--needed kde-graphics 1 2 3 4 5 6 8
--needed kde-multimedia 1
--needed kde-system 1 3 4
--needed kde-utilities 1 6 10 13 15 18
--needed kdesdk 2 10
--needed kde-applications 39 70 76 93 95 103
ksysguard
```

# Libre Office
```bash
GUI (or use pamac install --no-confirm)

libreoffice-fresh
```

# Ryzen monitor
```bash
GUI (or use pamac install --no-confirm)

sudo modprobe msr

zenpower-dkms
zenmonitor

Edit zenmonitor PKGBUILD, add
mkdir -p "${pkgdir}/usr/share/polkit-1/actions"
make DESTDIR="${pkgdir}" PREFIX="/usr" install-polkit
```

# CPU Power
```bash
GUI (or use pamac install --no-confirm)
cpupower

sudo nano /etc/default/cpupower

governor='conservative'
min_freq="1.4GHz"
max_freq="2.9GHz"

sudo systemctl enable cpupower
```

# Disable CPU Turbo clock (optional)
```bash
sudo mkdir /opt/power-mode && cd $_
sudo nano no-turbo-boost.sh

#!/bin/bash
echo "0" | sudo tee /sys/devices/system/cpu/cpufreq/boost

sudo nano no-turbo-boost.service

[Unit]
Description=Disable Turbo Boost
After=acpid.socket
After=syslog.service

[Service]
User=root
Type=simple
ExecStart=/opt/power-mode/no-turbo-boost.sh
ExecStop=/opt/power-mode/no-turbo-boost.sh
TimeoutSec=30
StartLimitInterval=350

[Install]
WantedBy=multi-user.target

sudo chmod +x /opt/power-mode/no-turbo-boost.sh
cd /etc/systemd/system
sudo ln -s /opt/power-mode/no-turbo-boost.service no-turbo-boost.service
sudo chmod u+x /etc/systemd/system/no-turbo-boost.service
sudo systemctl enable no-turbo-boost
```

# Optimize makepfg
```bash
sudo nano /etc/makepkg.conf

MAKEFLAGS="-j17"
```

# Apps
```bash
GUI (or use pamac install --no-confirm)

psensor
google-chrome
vivaldi vivaldi-ffmpeg-codecs
skypeforlinux-stable-bin
telegram-desktop
caprine
clementine
mpv
gst-plugins-good
calibre
stellarium
gimp
grub-customizer
kvantum-qt5
piper
qbittorrent
htop
subtitleeditor
lrzip
lzip
p7zip
zip
unarchiver
freerdp
libgdiplus
systemdgenie
ttf-fira-code
mailspring
ocs-url
```

# Fix firefox (optional)
```bash
about:config
toolkit.legacyUserProfileCustomizations.stylesheets = true

create file chrome/userChrome.css

*|div#fullscr-toggler {display:none!important;}
```

# IDE & Compiler

# Git
```bash
GUI (or use pamac install --no-confirm)

gitflow-avh
gitkraken
```

```bash
git config --global user.name "Duc Tran"
git config --global user.email tv.duc95@gmail.com
git config --global core.editor "nano -w"
git config --global core.autocrlf input
git config --global --add safe.directory '*'
```

*  Optional: use keepassxc

```bash
git config --global credential.helper libsecret
```

*  Optional: use ksshaskpass

```bash
git config --global core.askpass /usr/bin/ksshaskpass
sudo nano ~/.config/autostart-scripts/ssh-add.sh
#!/bin/sh
ssh-add -q < /dev/null

sudo nano ~/.config/plasma-workspace/env/askpass.sh
#!/bin/sh
export SSH_ASKPASS='/usr/bin/ksshaskpass'
export GIT_ASKPASS='/usr/bin/ksshaskpass'
```

*  Crack GitKraken

```bash
npm i
npm run-script build
sudo node dist/bin/gitcracken.js patcher
```

- [GitCracken](https://github.com/5cr1pt/GitCracken)

# C++
```bash
GUI (or use pamac install --no-confirm)

cmake
clang
llvm
lldb
lld
libc++
```

# Go
```bash
GUI (or use pamac install --no-confirm)

go
go-tools
```

# Java
```bash
GUI (or use pamac install --no-confirm)

jdk11-openjdk
java11-openjfx
jdk8
```

# .NET Core
```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

dotnet-host-bin
dotnet-sdk-bin
aspnet-runtime-bin
aspnet-targeting-pack-bin
dotnet-sdk-5.0-bin
aspnet-runtime-5.0-bin
aspnet-targeting-pack-5.0-bin
```

* or

```bash
Install .NET Core 3.1

sudo chmod 777 dotnet-install.sh
sudo ./dotnet-install.sh --channel 3.1 --install-dir /usr/share/dotnet
```

- [dotnet-install scripts](https://dot.net/v1/dotnet-install.sh)

```bash
dotnet tool install --global dotnet-ef
dotnet tool install --global dnt
nano ~/.xprofile

export PATH="$PATH:/home/ductran/.dotnet/tools"
```

* Fix SQL Server SSL

```bash
cd /etc/ssl/
sudo cp openssl.cnf openssl.cnf.bak
sudo nano openssl.cnf

Add to top:
openssl_conf = default_conf

Add to bottom:
[default_conf]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
MinProtocol = TLSv1
CipherString = DEFAULT@SECLEVEL=1
```

* Azure

```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

azure-cli
```

```bash
npm install -g azurite
```

# Mono (optional)
```bash
GUI (or use pamac install --no-confirm)

mono
mono-tools
mono-addins
mono-msbuild
mono-msbuild-sdkresolver
xsp
```

* config to run ASP .NET project

```bash
sudo mkdir /etc/mono/registry
sudo chmod uog+rw /etc/mono/registry

edit web.config
<dependentAssembly>
    <assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
    <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="4.2.0.0" />
</dependentAssembly>
```

- [Access to the path “/etc/mono/registry” is denied](https://stackoverflow.com/questions/24872394/access-to-the-path-etc-mono-registry-is-denied)
- [How to get Swashbuckle / Swagger working on the Mono (.NET) Framework?](https://stackoverflow.com/questions/36062557/how-to-get-swashbuckle-swagger-working-on-the-mono-net-framework)

# Nodejs
```bash
GUI (or use pamac install --no-confirm)

nodejs
npm
```

* or

```bash
nvm
source /usr/share/nvm/init-nvm.sh
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.bashrc
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.zshrc
nvm install node
nvm install --lts
```

* Update file watcher

```bash
echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

sudo npm install -g @angular/cli
```

- [Install NodeJS via package manager](https://nodejs.org/en/download/package-manager/#arch-linux)

# Android tools
```bash
GUI (or use pamac install --no-confirm)

android-tools
```

# Flutter (optional)
```bash
GUI (or use pamac install --no-confirm)

flutter
android-sdk
android-sdk-platform-tools
android-sdk-build-tools
android-sdk-cmdline-tools-latest
android-platform
```

```bash
sudo groupadd flutterusers
sudo gpasswd -a $USER flutterusers
sudo chown -R $USER:flutterusers /opt/flutter
sudo chmod -R g+w /opt/flutter/
```

```bash
sudo groupadd android-sdk
sudo gpasswd -a $USER android-sdk
sudo chown -R $USER:android-sdk /opt/android-sdk
sudo chmod -R g+w /opt/android-sdk
```

```bash
sudo nano ~/.xprofile

#Java
export JAVA_HOME='/usr/lib/jvm/java-14-openjdk/'

# Android SDK
export ANDROID_SDK_ROOT='/opt/android-sdk'
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/"

# Flutter
export PATH="$PATH:/opt/flutter/bin"
```
```bash
flutter doctor
flutter doctor --android-licenses
```

# MariaDB (optional)
```bash
GUI (or use pamac install --no-confirm)

mariadb
```

```bash
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb
sudo mysql_secure_installation
```

- [MariaDB - ArchWiki](https://wiki.archlinux.org/index.php/MariaDB)

# PostgreSQL
```bash
GUI (or use pamac install --no-confirm)
postgresql
libxcrypt-compat
libffi6
azuredatastudio-bin
```

```bash
sudo su postgres -l
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
sudo systemctl start postgresql
sudo su postgres -l
createuser --interactive --pwprompt
exit
sudo systemctl stop postgresql
sudo systemctl disable postgresql
```

- [PostgreSQL - ArchWiki](https://wiki.archlinux.org/index.php/PostgreSQL)

# MS SQL Server
- Download https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt

```bash
gpg --import ./gpg-pubkey.txt
```

```bash
GUI (or use pamac install --no-confirm)

mssql-server
```

```bash
sudo /opt/mssql/bin/mssql-conf setup
sudo systemctl stop mssql-server
sudo systemctl disable mssql-server
```

- [Installation guidance for SQL Server on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver15)

# MongoDB
```bash
GUI (or use pamac install --no-confirm)

mongodb-bin
mongodb-compass
mongodb-tools-bin
mongosh-bin
```

```bash
sudo systemctl start mongodb
mongosh
use admin
```

```bash
db.createUser(
  {
    user: "ductran",
    pwd: "Password",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```

```bash
sudo nano /etc/mongodb.conf
```

```bash
security:
  authorization: "enabled"
```

```bash
sudo systemctl stop mongodb
sudo systemctl disable mongodb
```

# VSCode
```bash
GUI (or use pamac install --no-confirm)

visual-studio-code-bin
```

# Codeblocks
```bash
GUI (or use pamac install --no-confirm)

codeblocks
```

# Jetbrains
```bash
GUI (or use pamac install --no-confirm)

webstorm
datagrip
rider
goland

Note: edit rider PKGBUILD _installdir='/opt'
```

```bash
sudo cp /opt/rider/bin/rider.svg /usr/share/pixmaps/rider.svg
sudo rm -rf /opt/rider/jbr
sudo ark -b -o /opt /mnt/disk3/Software/jbr-linux-x64-202110301849.zip
```

```bash
nano ~/.xprofile

export DATAGRIP_JDK=/opt/jbr
export RIDER_JDK=/opt/jbr
export WEBIDE_JDK=/opt/jbr
export GOLAND_JDK=/opt/jbr
```

# Redis
```bash
GUI (or use pamac install --no-confirm)

redis
```

# RabbitMQ
```bash
GUI (or use pamac install --no-confirm)

rabbitmq
```

# Apache
```bash
GUI (or use pamac install --no-confirm)

apache
```

# Nginx
```bash
GUI (or use pamac install --no-confirm)

nginx
```

```bash
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo mkdir /etc/nginx/conf.d
sudo cp nginx.conf /etc/nginx/nginx.conf
sudo cp 127.0.0.1.conf /etc/nginx/conf.d/127.0.0.1.conf
```

# FTP Server
```bash
GUI (or use pamac install --no-confirm)

vsftpd
filezilla
```

# PostMan
```bash
GUI (or use pamac install --no-confirm)

postman-bin
```

# Docker
```bash
GUI (or use pamac install --no-confirm)

docker
docker-compose
```

```bash
sudo usermod -aG docker ductran
```

# Fcitx
```bash
GUI (or use pamac install --no-confirm)

fcitx5-im
fcitx5-chinese-addons fcitx5-unikey
```

* config

```bash
nano ~/.xprofile

export INPUT_METHOD=fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

- [fcitx shortcut](https://askubuntu.com/questions/736638/fcitx-wont-trigger-ime-on-superspace)

# iBus (optional)
```bash
GUI (or use pamac install --no-confirm)

ibus ibus-pinyin ibus-unikey ibus-bamboo

Edit ibus-bamboo PKGBUILD pkgver=0.6.7 sha256sums=('SKIP')
```

* config

```bash
sudo nano ~/.xprofile

export INPUT_METHOD=ibus
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
```

```bash
sudo nano /usr/share/applications/ibus-daemon.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx

sudo nano /usr/share/applications/ibus-daemon-kimpanel.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx --panel=/usr/lib/kimpanel-ibus-panel

sudo cp /usr/share/applications/ibus-daemon.desktop /etc/xdg/autostart/
```

# KeepassXC
```bash
GUI (or use pamac install --no-confirm)

keepassxc

create database ~/Passwords with key ~/Passwords
```

```bash
nano ~/.xprofile

export PATH="$PATH:/home/ductran/bin"
```

```bash
copy /mnt/disk2/Projects/linux-config/files/applications/ to ~/.local/share/applications/
copy /mnt/disk2/Projects/linux-config/files/bin/ to ~/bin/
```

```bash
Add keepassxc-startup, keepassxc-watch to autostart scripts
```

- [Automatically unlock KeepassXC on startup and after lock screen](https://grabski.me/tech,/linux/2020/09/02/automatically-unlock-keepassxc-on-startup-and-after-lock-screen/)

# Config file dialog
```bash
nano ~/.xprofile

export GTK_USE_PORTAL=1
```

# PulseEffects (optional)
```bash
GUI (or use pamac install --no-confirm)

pulseeffects

start PulseEffects
Audio Gain => max
```

- [Community Presets](https://github.com/wwmm/pulseeffects/wiki/Community-presets)
- [How to mimic Dolby Audio Premium on Linux with PulseEffects](https://www.linuxupdate.co.uk/2020/10/19/how-to-mimic-dolby-audio-premium-on-linux-with-pulseeffects/)

# Steam (optional)

*  NVIDIA PRIME

```bash
GUI (or use pamac install --no-confirm)

nvidia-prime
```

*  Steam

```bash
nano /etc/pacman.conf

uncomment
#[multilib]
#Include = /etc/pacman.d/mirrorlist
```

```bash
sudo pacman -Syyu
sudo pacman -S lib32-mesa
sudo pacman -S lib32-vulkan-radeon
sudo pacman -S lib32-vulkan-icd-loader
sudo pacman -S lib32-nvidia-utils
```

```bash
GUI (or use pamac install --no-confirm)

steam
```

```bash
mkdir /home/ductran/.local/share/Steam/steamapps/compatdata
ln -s /home/ductran/.local/share/Steam/steamapps/compatdata /mnt/disk3/Game/Steam/steamapps/compatdata
mkdir "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0"
ln -s "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0" "/mnt/disk3/Game/Steam/steamapps/common/Proton 7.0"
mkdir /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier
ln -s /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier /mnt/disk3/Game/Steam/steamapps/common/SteamLinuxRuntime_soldier
```

Run game witch "prime-run %command%" argument

*  Optional

```bash
sudo pacman -S nvidia-dkms
sudo pacman -S linux-zen linux-zen-headers
```

```bash
mkdir /home/ductran/.local/share/Steam/compatibilitytools.d
tar -xf Proton-6.12-GE-1.tar.gz -C ~/.steam/root/compatibilitytools.d/
```

# Chinese Font
```bash
sudo pacman -S noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
sudo pacman -S ttf-roboto ttf-dejavu ttf-droid ttf-inconsolata ttf-indic-otf ttf-liberation
sudo pacman -S terminus-font
sudo pacman -S adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts
sudo pacman -S adobe-source-han-sans-tw-fonts adobe-source-han-serif-tw-fonts
sudo pacman -S wqy-microhei wqy-zenhei wqy-bitmapfont
sudo pacman -S opendesktop-fonts
```

*  optional: typographic font

```bash
sudo pacman -S noto-fonts-sc noto-fonts-tc
sudo pacman -S ttf-arphic-ukai ttf-arphic-uming
```

# Windows Font
```bash
sudo cp /mnt/disk1/Windows/Fonts/* ~/.local/share/fonts
sudo fc-cache --force
```

- [Microsoft fonts](https://wiki.archlinux.org/index.php/Microsoft_fonts)

# GRUB Convert to Arch

```bash
sudo nano /etc/default/grub

GRUB_DISTRIBUTOR="Arch"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# GRUB Theme
*  Theme 1

```bash
yay -S grub2-theme-archxion
sudo nano /etc/default/grub

Replace #GRUB_THEME="/path/to/gfxtheme" by GRUB_THEME="/boot/grub/themes/Archxion/theme.txt"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

- [Generator/Grub2-themes](https://github.com/Generator/Grub2-themes)

*  Theme 2

```bash
sudo nano /etc/default/grub

GRUB_GFXMODE=1920x1080x32
```

```bash
sudo ./install.sh -b -t tela
```

- [vinceliuice/grub2-themes](https://github.com/vinceliuice/grub2-themes)

# Tweaks
*  Unpin all app in task bar
*  Configure System Tray --> Extra items: clipboard, Device notifier, Display Configuration, KDE Connect, Media Player, Notification, Printer, Weather report
*  Configure System Tray --> Entries --> Hidden all
*  Configure System Tray --> Entries --> Show: Notification
*  Configure Panel --> Screen Edge: Left
*  Pin favorite apps: System Settings, Dolphin, Add or Remove Software, Zenmonitor root, Vivaldi, Chrome, Firefox, qbittorrent, Skype, Telegram, Caprine, Discord, clementine, mpv, celluloid, subtitle editor, rider, goland, webstorm, datagrip, GitKraken, Visual Studio Code, pgadmin4, Postman
*  Edit panel --> Add widget --> Download new widget: Win7 volume mixer
*  Edit panel --> Add widget: Battery, Volume mixer, Network, Bluetooth, Touchpad, Input Method panel
*  Input Method panel --> Hide: fcitx, virtual keyboard, Telex, Unicode, Half width, Spell check, Macro, Full width input, No remind
*  Edit panel --> More Setting --> Auto hide

# Fix menu height
```bash
sudo nano /usr/share/plasma/plasmoids/org.kde.plasma.kickoff/contents/ui/FullRepresentation.qml

Layout.minimumHeight: implicitHeight * 1.5
```

# Theme (dark)
*  Global theme: Nordian Solid Global
*  Plasma style: Nordian Solid Plasma
*  Application Style: kvantum
*  GTK Theme: Nordian Breeze GTK
*  Window Decorators: Nordian Solid Aurorae
*  Color: kvantum
*  Icon: Breeze Nordian Dark Icon
*  SDDM: Nordian SDDM
*  Splash Screen: Nordian Solid Global
*  Kvantum: Nordian Kvantum
*  Wallpaper: Plasma Desktop Wallpaper 1591
*  Lock screen: Nordian-Plasma
*  Fix SDDM date Format

```bash
sudo nano /usr/share/sddm/themes/Nordian-SDDM/components/Clock.qml

"'The day is' dddd dd MMMM yyyy"
```

# Theme 2 (dark)
*  Global theme: Arc KDE
*  GTK Theme: Plane GTK Arc Dark
*  Icon: papirus dark
*  SDDM: KDE-Story
*  Application Style: kvantum
*  Kvantum: Arc Dark

# Theme 3 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

lightly-qt
```

*  Global theme: Breeze
*  Plasma style: Breeze Light
*  Application Style: Lightly
*  GTK Theme: Breeze
*  Window Decorators: Lightly
*  Color: Breeze Light
*  Icon: Breeze
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 4 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt
```

*  Global theme: aritim-light
*  Plasma style: aritim-light
*  Application Style: Lightly
*  GTK Theme: aritim-light
*  Window Decorators: aritim-light
*  Color: aritim-light
*  Icon: Papirus light
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 5 (dark)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt

sudo nano /usr/share/color-schemes/AritimDark.colors
[WM]
activeBackground=20,26,33,255
inactiveForeground=102,106,115,255

extract /mnt/disk2/Projects/linux-config/files/assets.zip to ~/.config/gtk-3.0/
copy /mnt/disk2/Projects/linux-config/files/share/ to /usr/share/
```

*  Global theme: aritim-dark
*  Plasma style: aritim-dark
*  Application Style: Lightly
*  GTK Theme: aritim-dark
*  Window Decorators: aritim-dark
*  Color: aritim-dark
*  Icon: Papirus dark
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 6 (dark)
*  Global theme: Qogir Dark
*  Plasma style: Qogir-dark
*  Application Style: Kvantum
*  GTK Theme: Qogir Dark
*  Icon: Qogir
*  SDDM: Qogir
*  Kvantum: Qogir Dark Solid
*  Wallpaper: Plasma 1591

```bash
mkdir ~/.icons
mkdir ~/.icons/default
cd ~/.icons/default
ln -sf cursors ~/.local/share/icons/Qogir-dark/cursors
nano index.theme

[icon theme]
Inherits=Qogir-dark

mv ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg.bak
```

# Plasmoid
*  Analog clock
*  System Monitor
*  hard disk activity
*  Network speed

# Zsh
```bash
GUI (or use pamac install --no-confirm)

zsh
zsh-completions
```
```bash
zsh
chsh -s $(which zsh)
```
```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```
Copy config files
```bash
.p10k.zsh
.zshrc
```

# Tips & Tricks
*  Pacman remove unused packages

```bash
sudo pacman -Rs $(pacman -Qdtq)
```

*  Reduce log file

```bash
sudo journalctl --vacuum-time=1weeks

sudo nano /etc/systemd/journald.conf

SystemMaxUse=50M
```

*  Fix Brightness

```bash
sudo nano /sys/class/backlight/amdgpu_bl0/brightness

255
```

*  Clear cache files

```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
```
