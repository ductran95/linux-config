# My Legion 5 Arch KDE installation using ArchLinuxGUI

# Boot Arch
*  Boot Arch Linux (x86_64)

# Connect to Internet

# Setting
*  Workspace Behavior --> General Behavior --> Animation speed: Instant
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen locking --> Lock screen: off
*  Shortcut --> Global Shortcut --> Defaults
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: untick alt+f2
*  Shortcut --> Global Shortcut --> KWin --> Hide window boder: Meta+Enter
*  Shortcut --> Global Shortcut --> KWin --> Maximum window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimum window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Windows (Current Desktop): Meta+Tab
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Yakuake: window+f12
*  Shortcut --> Global Shortcut --> Konsole: Ctrl+Alt+T
*  Startup and Shutdown --> Autostart --> Add program: Yakuake
*  Startup and Shutdown --> Autostart --> Add program: KSysGuard
*  Regional Setting --> Language: English
*  Regional Settings --> Formats --> Region: Viet Nam
*  Regional Settings --> Formats --> Detail Setting --> Number: Default
*  Regional Settings --> Formats --> Detail Setting --> Currency: Default
*  Regional Settings --> Date & Time --> Set time auto: True
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Poiter speed: 5
*  Input Devices --> Touchpad --> Poiter speed: 5
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Display and Monitor --> Compositor --> Rendering backend: OpenGL 3.1
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On AC Power --> Wireless: On
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Wireless: On
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 15 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 30 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown

# Update the system clock
```bash
sudo timedatectl set-ntp true
sudo hwclock --systohc --localtime
```

# Update packages
```bash
sudo pacman -Syyy
sudo pacman -S archlinux-keyring
sudo pacman -Syyu
```

# Graphic driver
```bash
sudo pacman -S xf86-video-amdgpu
sudo pacman -S vulkan-radeon
```

# KDE Plasma
```bash
Note: remove discover

sudo pacman -S ksysguard
sudo pacman -S --needed kde-graphics 1 2 3 4 5 7 8 10
sudo pacman -S --needed kde-multimedia 3 4
sudo pacman -S --needed kde-system 3 4
sudo pacman -S --needed kde-utilities 2 5 11 12 14 15 17
sudo pacman -S --needed kdesdk 9
sudo pacman -S --needed kde-applications 39 40 72 78 95 97
sudo pacman -R discover
```

# Remove reflector
```bash
sudo systemctl disable reflector
```

# Reboot
```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
reboot
```

# Fix KDE bug
*  Fix check network bug

```bash
sudo nano /usr/lib/NetworkManager/conf.d/20-connectivity.conf
```

```bash
[connectivity]
#uri=http://www.archlinux.org/check_network_status.txt
uri=http://networkcheck.kde.org/
```

[Log in required for ethernet at home?”](https://www.reddit.com/r/ManjaroLinux/comments/keabph/log_in_required_for_ethernet_at_home/)

*  Fix shortcut bug

```bash
sudo nano ~/.config/plasma-org.kde.plasma.desktop-appletsrc
sudo nano ~/.config/kglobalshortcutsrc

Change Alt+F1 => Meta+Alt+F1
```

# Disable PC Speaker
```bash
sudo rmmod pcspkr
sudo nano /etc/modprobe.d/nobeep.conf
```

```bash
# Do not load the 'pcspkr' module on boot.
blacklist pcspkr
```

# Disable KVM to stop shutdown error
```bash
sudo nano /etc/modprobe.d/nokvm.conf
```

```bash
blacklist kvm_intel
blacklist kvm_amd
blacklist kvm
```

# SDDM Auto numlock
```bash
sudo nano /etc/sddm.conf

Numlock=on
```

# Reboot

# Pamac
```bash
paru pamac-aur
```

# Add AUR Repository
```bash
Add or remove software
Preference
```

```bash
Enable AUR support: true
Check update from AUR: true
```

# Automount partition
```bash
GUI (or use pamac install --no-confirm)

ntfs3-dkms
```

```bash
sudo mkdir /mnt/disk1
sudo mkdir /mnt/disk2
sudo mkdir /mnt/disk3
sudo blkid /dev/nvme0n1p3
sudo blkid /dev/nvme0n1p5
sudo blkid /dev/nvme1n1p1
```

```bash
id -u
id -g

sudo nano /etc/fstab

UUID=8EAC45F5AC45D7FB   /mnt/disk1  ntfs        defaults 0 0
UUID=D4787282787262E2   /mnt/disk2  ntfs        defaults 0 0
UUID=74746E13746DD87E   /mnt/disk3  ntfs        defaults 0 0

```

# Color Management
```bash
GUI (or use pamac install --no-confirm)

colord-kde

sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-18 11-33 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-26 20-14 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
```

# Hardware video acceleration
```bash
GUI (or use pamac install --no-confirm)

libva-mesa-driver
mesa-vdpau
```

# Ryzen monitor
```bash
GUI (or use pamac install --no-confirm)

sudo modprobe msr

zenpower-dkms
zenmonitor

Edit zenmonitor PKGBUILD, add
mkdir -p "${pkgdir}/usr/share/polkit-1/actions"
make DESTDIR="${pkgdir}" PREFIX="/usr" install-polkit
```

# CPU Power
```bash
GUI (or use pamac install --no-confirm)
cpupower

sudo nano /etc/default/cpupower

governor='conservative'
min_freq="1.4GHz"
max_freq="2.9GHz"

sudo systemctl enable cpupower

sudo mkdir /opt/power-mode && cd $_
sudo nano no-turbo-boost.sh

#!/bin/bash

echo "0" | sudo tee /sys/devices/system/cpu/cpufreq/boost

sudo nano no-turbo-boost.service

[Unit]
Description=Disable Turbo Boost
After=acpid.socket
After=syslog.service

[Service]
User=root
Type=simple
ExecStart=/opt/power-mode/no-turbo-boost.sh
ExecStop=/opt/power-mode/no-turbo-boost.sh
TimeoutSec=30
StartLimitInterval=350

[Install]
WantedBy=multi-user.target

sudo chmod +x /opt/power-mode/no-turbo-boost.sh
cd /etc/systemd/system
sudo ln -s /opt/power-mode/no-turbo-boost.service no-turbo-boost.service
sudo chmod u+x /etc/systemd/system/no-turbo-boost.service
sudo systemctl enable no-turbo-boost
```

# CPU Power (old)
```bash
echo conservative | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

sudo nano /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="cpufreq.default_governor=conservative loglevel=3 quiet"

sudo LANG=en_us grub-mkconfig -o /boot/grub/grub.cfg
```

# Apps
```bash
GUI (or use pamac install --no-confirm)

psensor
google-chrome
vivaldi
skypeforlinux-stable-bin
telegram
caprine
clementine
mpv
celluloid
calibre
stellarium
gimp
grub-customizer
kvantum-qt5
piper
qbittorrent
neofetch
htop
openvpn
networkmanager-openvpn
subtitleeditor
lrzip
lzip
p7zip
zip
unarchiver
rar
motrix-bin
freerdp
libgdiplus
```

# Multimedia
```bash
GUI (or use pamac install --no-confirm)

obs-studio
```

# Fix firefox
```bash
about:config
toolkit.legacyUserProfileCustomizations.stylesheets = true

create file chrome/userChrome.css

*|div#fullscr-toggler {display:none!important;}
```

# IDE & Compiler

# Git
```bash
GUI (or use pamac install --no-confirm)

gitflow-avh
gitkraken
```

```bash
git config --global user.name "Duc Tran"
git config --global user.email tv.duc95@gmail.com
```

*  Optional: use keepassxc

```bash
git config --global credential.helper libsecret
```

*  Optional: use ksshaskpass

```bash
git config --global core.askpass /usr/bin/ksshaskpass
sudo nano ~/.config/autostart-scripts/ssh-add.sh
#!/bin/sh
ssh-add -q < /dev/null

sudo nano ~/.config/plasma-workspace/env/askpass.sh
#!/bin/sh
export SSH_ASKPASS='/usr/bin/ksshaskpass'
export GIT_ASKPASS='/usr/bin/ksshaskpass'
```

*  Crack GitKraken

```bash
npm i
npm run-script build
sudo node dist/bin/gitcracken.js patcher
```

- [GitCracken](https://github.com/5cr1pt/GitCracken)

# C++
```bash
GUI (or use pamac install --no-confirm)

gcc
gdb
cmake
clang
llvm
lldb
lld
libc++
```

# Go
```bash
GUI (or use pamac install --no-confirm)

go
go-tools
```

# Java
```bash
GUI (or use pamac install --no-confirm)

jdk-openjdk
java-openjfx
jdk11-openjdk
java11-openjfx
```

# .NET Core
```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

dotnet-sdk-bin
aspnet-runtime-bin
aspnet-targeting-pack-bin
dotnet-sdk-3.1-bin
aspnet-runtime-3.1-bin
aspnet-targeting-pack-3.1-bin
```

* or

```bash
Install .NET Core 3.1

sudo chmod 777 dotnet-install.sh
sudo ./dotnet-install.sh --channel 3.1 --install-dir /usr/share/dotnet
```

- [dotnet-install scripts](https://dot.net/v1/dotnet-install.sh)

```bash
dotnet tool install --global dotnet-ef
dotnet tool install --global dnt
sudo nano ~/.xprofile

export PATH="$PATH:/home/ductran/.dotnet/tools"
```

* Fix SQL Server SSL

```bash
cd /etc/ssl/
sudo cp openssl.conf openssl.conf.bak
sudo nano openssl.conf

Add to top:
openssl_conf = default_conf

Add to bottom:
[default_conf]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
MinProtocol = TLSv1
CipherString = DEFAULT@SECLEVEL=1
```

# Mono
```bash
GUI (or use pamac install --no-confirm)

mono
mono-tools
mono-addins
mono-msbuild
mono-msbuild-sdkresolver
xsp
```

* config to run ASP .NET project

```bash
sudo mkdir /etc/mono/registry
sudo chmod uog+rw /etc/mono/registry

edit web.config
<dependentAssembly>
    <assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
    <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="4.2.0.0" />
</dependentAssembly>
```

- [Access to the path “/etc/mono/registry” is denied](https://stackoverflow.com/questions/24872394/access-to-the-path-etc-mono-registry-is-denied)
- [How to get Swashbuckle / Swagger working on the Mono (.NET) Framework?](https://stackoverflow.com/questions/36062557/how-to-get-swashbuckle-swagger-working-on-the-mono-net-framework)

# Nodejs
```bash
GUI (or use pamac install --no-confirm)

nodejs
npm
```

* or

```bash
nvm
source /usr/share/nvm/init-nvm.sh
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.bashrc
nvm install node
nvm install --lts
```

* Update file watcher

```bash
sudo rm -rfv /etc/sysctl.d/*
echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

sudo npm install -g @angular/cli
```

- [Install NodeJS via package manager](https://nodejs.org/en/download/package-manager/#arch-linux)

# Android tools
```bash
GUI (or use pamac install --no-confirm)

android-tools
```

# Flutter
```bash
GUI (or use pamac install --no-confirm)

flutter
android-sdk
android-sdk-platform-tools
android-sdk-build-tools
android-sdk-cmdline-tools-latest
android-platform
```

```bash
sudo groupadd flutterusers
sudo gpasswd -a $USER flutterusers
sudo chown -R $USER:flutterusers /opt/flutter
sudo chmod -R g+w /opt/flutter/
```

```bash
sudo groupadd android-sdk
sudo gpasswd -a $USER android-sdk
sudo chown -R $USER:android-sdk /opt/android-sdk
sudo chmod -R g+w /opt/android-sdk
```

```bash
sudo nano ~/.xprofile

#Java
export JAVA_HOME='/usr/lib/jvm/java-14-openjdk/'

# Android SDK
export ANDROID_SDK_ROOT='/opt/android-sdk'
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/"

# Flutter
export PATH="$PATH:/opt/flutter/bin"
```
```bash
flutter doctor
flutter doctor --android-licenses
```

# MariaDB
```bash
GUI (or use pamac install --no-confirm)

mariadb
```

```bash
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb
sudo mysql_secure_installation
```

- [MariaDB - ArchWiki](https://wiki.archlinux.org/index.php/MariaDB)

# PostgreSQL
```bash
GUI (or use pamac install --no-confirm)
postgresql
pgadmin4
```

```bash
sudo su postgres -l
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
sudo systemctl start postgresql
sudo su postgres -l
createuser --interactive --pwprompt
exit
sudo systemctl stop postgresql
sudo systemctl disable postgresql
```

- [PostgreSQL - ArchWiki](https://wiki.archlinux.org/index.php/PostgreSQL)

# MS SQL Server
- Download https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt

```bash
gpg --import ./gpg-pubkey.txt
```

```bash
GUI (or use pamac install --no-confirm)

mssql-server
```

```bash
sudo /opt/mssql/bin/mssql-conf setup
sudo systemctl stop mssql-server
sudo systemctl disable mssql-server
```

- [Installation guidance for SQL Server on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver15)

# VSCode
```bash
GUI (or use pamac install --no-confirm)

visual-studio-code-bin
```

# Codeblocks
```bash
GUI (or use pamac install --no-confirm)

codeblocks
```

# Jetbrains
```bash
GUI (or use pamac install --no-confirm)

webstorm
datagrip
rider
goland

Note: edit rider PKGBUILD _installdir='/opt'
```

```bash
sudo cp /opt/rider/bin/rider.svg /usr/share/pixmaps/rider.svg
sudo rm -rf /opt/rider/jbr
sudo ark -b -o /opt /mnt/disk3/Software/IDE/jbr-linux-x64-202106011206.zip
```

```bash
sudo nano /opt/datagrip/bin/datagrip.sh
export DATAGRIP_JDK=/opt/jbr

sudo nano /opt/intellij-idea-ultimate-edition/bin/idea.sh
export IDEA_JDK=/opt/jbr

sudo nano /opt/rider/bin/rider.sh
export RIDER_JDK=/opt/jbr

sudo nano /opt/webstorm/bin/webstorm.sh
export WEBIDE_JDK=/opt/jbr
```

* or

```bash
sudo nano ~/.xprofile

export DATAGRIP_JDK=/opt/jbr
export RIDER_JDK=/opt/jbr
export WEBIDE_JDK=/opt/jbr
export GOLAND_JDK=/opt/jbr
```

# Redis
```bash
GUI (or use pamac install --no-confirm)

redis
```

# RabbitMQ
```bash
GUI (or use pamac install --no-confirm)

RabbitMQ
```

# Apache
```bash
GUI (or use pamac install --no-confirm)

apache
```

# Nginx
```bash
GUI (or use pamac install --no-confirm)

nginx
```

```bash
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo mkdir /etc/nginx/conf.d
sudo cp nginx.conf /etc/nginx/nginx.conf
sudo cp 127.0.0.1.conf /etc/nginx/conf.d/127.0.0.1.conf
```

# FTP Server
```bash
GUI (or use pamac install --no-confirm)

bftpd
```

```bash
sudo nano /etc/bftpd.conf

ANONYMOUS_USER="yes"
DENY_LOGIN="no"
```

# PostMan
```bash
GUI (or use pamac install --no-confirm)

postman-bin
```

# Fcitx
```bash
GUI (or use pamac install --no-confirm)

fcitx5-im
fcitx5-chinese-addons fcitx5-unikey
```

* config

```bash
sudo nano ~/.xprofile

export INPUT_METHOD=fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

- [fcitx shortcut](https://askubuntu.com/questions/736638/fcitx-wont-trigger-ime-on-superspace)

# iBus
```bash
GUI (or use pamac install --no-confirm)

ibus ibus-pinyin ibus-unikey ibus-bamboo

Edit ibus-bamboo PKGBUILD pkgver=0.6.7 sha256sums=('SKIP')
```

* config

```bash
sudo nano ~/.xprofile

export INPUT_METHOD=ibus
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
```

```bash
sudo nano /usr/share/applications/ibus-daemon.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx

sudo nano /usr/share/applications/ibus-daemon-kimpanel.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx --panel=/usr/lib/kimpanel-ibus-panel

sudo cp /usr/share/applications/ibus-daemon.desktop /etc/xdg/autostart/
```

# KeepassXC
```bash
GUI (or use pamac install --no-confirm)

keepassxc

create database ~/Passwords with key ~/Passwords
```

```bash
nano ~/.xprofile

export PATH="$PATH:/home/ductran/bin"
```

```bash
copy /mnt/disk2/Projects/linux-config/files/applications/ to ~/.local/share/applications/
copy /mnt/disk2/Projects/linux-config/files/bin/ to ~/bin/
```

```bash
Add keepassxc-startup, keepassxc-watch to autostart scripts
```

- [Automatically unlock KeepassXC on startup and after lock screen](https://grabski.me/tech,/linux/2020/09/02/automatically-unlock-keepassxc-on-startup-and-after-lock-screen/)

# Config file dialog
```bash
GUI (or use pamac install --no-confirm)

xdg-desktop-portal
xdg-desktop-portal-kde
```

```bash
nano ~/.xprofile

export GTK_USE_PORTAL=1
```

# PulseEffects
```bash
GUI (or use pamac install --no-confirm)

pulseeffects

start PulseEffects
Audio Gain => max
```

- [Community Presets](https://github.com/wwmm/pulseeffects/wiki/Community-presets)
- [How to mimic Dolby Audio Premium on Linux with PulseEffects](https://www.linuxupdate.co.uk/2020/10/19/how-to-mimic-dolby-audio-premium-on-linux-with-pulseeffects/)

# Steam

*  NVIDIA PRIME

```bash
GUI (or use pamac install --no-confirm)

nvidia-prime
```

*  Steam

```bash
nano /etc/pacman.conf

uncomment
#[multilib]
#Include = /etc/pacman.d/mirrorlist
```

```bash
sudo pacman -Syyu
sudo pacman -S lib32-mesa
sudo pacman -S lib32-vulkan-radeon
sudo pacman -S lib32-vulkan-icd-loader
sudo pacman -S lib32-nvidia-utils
```

```bash
GUI (or use pamac install --no-confirm)

steam
```

```bash
mkdir /home/ductran/.local/share/Steam/steamapps/compatdata
ln -s /home/ductran/.local/share/Steam/steamapps/compatdata /mnt/disk3/Game/Steam/steamapps/compatdata
```

Run game witch "prime-run %command%" argument

*  Optional

```bash
sudo pacman -S nvidia-dkms
sudo pacman -S linux-zen linux-zen-headers
```

```bash
mkdir "/home/ductran/.local/share/Steam/steamapps/common/Proton 6.3"
mkdir /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier
ln -s "/home/ductran/.local/share/Steam/steamapps/common/Proton 6.3" "/mnt/disk3/Game/Steam/steamapps/common/Proton 6.3"
ln -s /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier /mnt/disk3/Game/Steam/steamapps/common/SteamLinuxRuntime_soldier
mkdir /home/ductran/.local/share/Steam/compatibilitytools.d
tar -xf Proton-6.12-GE-1.tar.gz -C ~/.steam/root/compatibilitytools.d/
```

# Tweaks
*  Unpin all app in task bar
*  Configure desktop --> Tweak --> Show the desktop toolbox: off
*  Configure appplication laucher --> Show application by name: on
*  Pin favorite apps: System Settings, Add or Remove Software, Dolphin, Vivaldi, Chrome, Firefox, Skype, Visual Studio Code, Webstorm, Rider, GitKraken
*  Configure Panel --> Screen Edge: Left
*  Edit panel --> Add widget --> Download new widget: Win7 volume mixer
*  Configure System Tray --> Extra items: clipboard, Device notifier, Display Configuration, KDE Connect, Media Player, Notification, Printer, Weather report
*  Configure System Tray --> Entries --> Hidden all
*  Configure System Tray --> Entries --> Auto: Notification, psensor, Clipboard
*  Edit panel --> Adnetd widget: Battery, Volume mixer, Network, Bluetooth, Touchpad, Input Method panel
*  Input Method panel --> Hide: fcitx, virtual keyboard, Telex, Unicode, Half width, Spell check, Macro, Full width input, No remind
*  Edit panel --> More Setting --> Auto hide
*  Lock widget

# Fix menu height
```bash
sudo nano /usr/share/plasma/plasmoids/org.kde.plasma.kickoff/contents/ui/FullRepresentation.qml

Layout.minimumHeight: PlasmaCore.Units.gridUnit * 43
```

# Theme (dark)
*  Global theme: Nordian Solid Global
*  Plasma style: Nordian Solid Plasma
*  Application Style: kvantum
*  GTK Theme: Nordian Breeze GTK
*  Window Decorators: Nordian Solid Aurorae
*  Color: kvantum
*  Icon: Breeze Nordian Dark Icon
*  SDDM: Nordian SDDM
*  Splash Screen: Nordian Solid Global
*  Kvantum: Nordian Kvantum
*  Wallpaper: Plasma Desktop Wallpaper 1591
*  Lock screen: Nordian-Plasma
*  Fix SDDM date Format

```bash
sudo nano /usr/share/sddm/themes/Nordian-SDDM/components/Clock.qml

"'The day is' dddd dd MMMM yyyy"
```

# Theme 2 (dark)
*  Global theme: Arc KDE
*  GTK Theme: Plane GTK Arc Dark
*  Icon: papirus dark
*  SDDM: KDE-Story
*  Application Style: kvantum
*  Kvantum: Arc Dark

# Theme 3 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

lightly-qt
```

*  Global theme: Breeze
*  Plasma style: Breeze Light
*  Application Style: Lightly
*  GTK Theme: Breeze
*  Window Decorators: Lightly
*  Color: Breeze Light
*  Icon: Breeze
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 4 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt
```

*  Global theme: aritim-light
*  Plasma style: aritim-light
*  Application Style: Lightly
*  GTK Theme: aritim-light
*  Window Decorators: aritim-light
*  Color: aritim-light
*  Icon: Papirus light
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 5 (dark)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt

sudo nano /usr/share/color-schemes/AritimDark.colors
[WM]
activeBackground=20,26,33,255
inactiveForeground=102,106,115,255

extract /mnt/disk2/Projects/linux-config/files/assets.zip to ~/.config/gtk-3.0/
copy /mnt/disk2/Projects/linux-config/files/share/ to /usr/share/
```

*  Global theme: aritim-dark
*  Plasma style: aritim-dark
*  Application Style: Lightly
*  GTK Theme: aritim-dark
*  Window Decorators: aritim-dark
*  Color: aritim-dark
*  Icon: Papirus dark
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Plasmoid
*  Analog clock
*  System Monitor
*  hard disk activity
*  Network speed

# Chinese Font
```bash
sudo pacman -S noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
sudo pacman -S ttf-roboto ttf-dejavu ttf-droid ttf-inconsolata ttf-indic-otf ttf-liberation
sudo pacman -S terminus-font
sudo pacman -S adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts
sudo pacman -S adobe-source-han-sans-tw-fonts adobe-source-han-serif-tw-fonts
sudo pacman -S wqy-microhei wqy-zenhei wqy-bitmapfont
sudo pacman -S opendesktop-fonts
```

*  optional: typographic font

```bash
sudo pacman -S noto-fonts-sc noto-fonts-tc
sudo pacman -S ttf-arphic-ukai ttf-arphic-uming
```

# Windows Font
```bash
sudo cp /mnt/disk1/Windows/Fonts/* ~/.local/share/fonts
sudo fc-cache --force
```

- [Microsoft fonts](https://wiki.archlinux.org/index.php/Microsoft_fonts)

# GRUB Theme
```bash
yay -S grub2-theme-archxion
sudo nano /etc/default/grub

Replace #GRUB_THEME="/path/to/gfxtheme" by GRUB_THEME="/boot/grub/themes/Archxion/theme.txt"

sudo LANG=en_us grub-mkconfig -o /boot/grub/grub.cfg
```

- [Generator/Grub2-themes](https://github.com/Generator/Grub2-themes)

# Tips & Tricks
*  Pacman remove unused packages

```bash
sudo pacman -Rs $(pacman -Qdtq)
```

*  Clear cache files

```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
```
