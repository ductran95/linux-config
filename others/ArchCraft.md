# My Legion 5 Arch Craft installation
[Archcraft](https://wiki.archcraft.io/docs/install-archcraft/post-install)

# Boot Arch Craft
*  Boot Arch Craft Linux (x86_64) archcraft-wayland-2023.01.10-x86_64.iso

# Connect to Internet

# Install Archcraft With Calamares

# Disable PC Speaker
```bash
sudo rmmod pcspkr
sudo nano /etc/modprobe.d/nobeep.conf
```

```bash
# Do not load the 'pcspkr' module on boot.
blacklist pcspkr
```

# Fix SDDM stop
```bash
sudo nano /etc/systemd/system.conf
```

```bash
DefaultTimeoutStopSec=10s
```

# Disable KVM to stop shutdown error (optional)
```bash
sudo nano /etc/modprobe.d/nokvm.conf
```

```bash
blacklist kvm_intel
blacklist kvm_amd
blacklist kvm
```

# Remove NVIDIA
```bash
sudo pacman -Syyy
sudo pacman -R nvidia nvidia-settings nvidia-utils
```

# Disable nouveau (optional)
```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT=driver=freenonouveau nouveau.modeset=0

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Full system update
```bash
sudo pacman -Syyu
yay -Syu
yay --editmenu --nodiffmenu --save
```

# Optimize makepfg
```bash
sudo nano /etc/makepkg.conf

MAKEFLAGS="-j$(nproc)"
```

# Update mirrorlist (optional)
```bash
sudo pacman -S reflector
sudo reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

# Update time
```bash
sudo hwclock --systohc --localtime
```

# Reboot

# Ryzen monitor
```bash
sudo modprobe msr

sudo pacman -S linux-headers
yay -S zenpower-dkms
yay -S zenmonitor

Edit zenmonitor PKGBUILD, add
mkdir -p "${pkgdir}/usr/share/polkit-1/actions"
make DESTDIR="${pkgdir}" PREFIX="/usr" install-polkit
```

# CPU Power
```bash
sudo pacman -S cpupower

sudo nano /etc/default/cpupower

governor='conservative'
min_freq="1.4GHz"

sudo systemctl enable cpupower
```

# AMD Pstate

```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="amd_pstate=passive"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Automount partition
```bash
sudo mkdir /mnt/disk1
sudo mkdir /mnt/disk2
sudo mkdir /mnt/disk3
sudo mkdir /mnt/disk4
sudo blkid /dev/nvme1n1p3
sudo blkid /dev/nvme1n1p5
sudo blkid /dev/nvme1n1p6
sudo blkid /dev/nvme0n1p1
```

```bash
id -u
id -g

sudo nano /etc/fstab

UUID=22525FD4525FAB75   /mnt/disk1  ntfs         defaults,uid=1000,gid=1000 0 0
UUID=DA23-FE50          /mnt/disk2  exfat        defaults,uid=1000,gid=1000 0 0
UUID=8B74-9A30          /mnt/disk3  exfat        defaults,uid=1000,gid=1000 0 0
UUID=7111-6570          /mnt/disk4  exfat        defaults,uid=1000,gid=1000 0 0

```

# Hyprland app
```bash
yay -S xdg-desktop-portal-hyprland-git
yay -S waybar-hyprland-git
yay -S swaylock-effects
```

# Remove xfce
```bash
sudo pacman -Rnsdd xfce4
sudo pacman -Rddn geany xfce-polkit thunar-archive-plugin thunar-media-tags-plugin xarchiver
```

# KDE App
```bash
sudo pacman -S polkit-kde-agent konsole kate kdeconnect dolphin kde-cli-tools ksysguard ark ksystemlog kompare krdc gwenview filelight okular partitionmanager
```

# Reboot

# Apps
```bash
GUI (or use pamac install --no-confirm)

psensor
vivaldi
vivaldi-ffmpeg-codecs
opera
opera-ffmpeg-codecs
telegram-desktop
discord
caprine
clementine
calibre
gimp
qbittorrent
subtitleeditor
freerdp
libgdiplus
ttf-fira-code
ttf-cascadia-code

authy
google-chrome
skypeforlinux-stable-bin
mailspring
stellarium-bin
```

# Rice: Update existed
* nano ~/.config/hypr/hyprland.conf

```bash
input
	force_no_accel = true

#-- Keybindings ------------------------------------------------
$files       = dolphin
$editor      = kate
$browser     = vivaldi-satble

# -- Terminal --
bind = CTRL_ALT,    T, exec, $term
bind = SUPER_SHIFT, Return, exec, $term -f
bind = SUPER_ALT,   Return, exec, $term -s

# -- Apps --
bind = SUPER, E, exec, $files
bind = SUPER, K, exec, $editor
bind = SUPER, B, exec, $browser
bind = SUPER, M, exec, ksysguard
bind = SUPER, P, exec, psensor

# -- Misc --
bind = SUPER,    L, exec, $lockscreen
bind = SUPER_CTRL,    P, exec, $colorpicker

# -- Hyprland --
bind = SUPER_SHIFT ,F,  fullscreen, 1
bind = SUPER,       Return,  togglefloating,
bind = SUPER,       Return,  centerwindow,

# Switch between windows
bind = ALT, Tab, cyclenext,
bind = ALT, Tab, bringactivetotop,
bind = SUPER, Tab, workspace, m+1
bind = SUPER_SHIFT, Tab, workspace, m-1
```

* nano ~/.config/hypr/scripts/lockscreen

```bash
swaylock -f \
	\
	--clock \
	--indicator \
```

* nano ~/.config/hypr/scripts/startup
```bash
/usr/lib/polkit-kde-authentication-agent-1 &
```

* nano ~/.config/hypr/waybar/config

```bash
"modules-left": ["custom/menu",  "wlr/workspaces" ],
"modules-center": [ "cpu", "cpu#2", "memory", "memory#2", "disk", "disk#2" ],
"modules-right": ["pulseaudio", "pulseaudio#2", "backlight", "backlight#2", "battery", "battery#2", "network", "network#2", "clock", "clock#2", "idle_inhibitor", "tray", "custom/power"],
```

* nano ~/.config/hypr/waybar/modules

```bash
/// waybar-wlr-workspaces
"wlr/workspaces": {
      "format": "{icon}",
      "on-click": "activate",
      "on-scroll-up": "hyprctl dispatch workspace e+1",
      "on-scroll-down": "hyprctl dispatch workspace e-1",
      "format-icons": {
          "1": "",
          "2": "",
          "3": "",
          "4": "",
          "5": "",
          "6": "漣",
          "7": "",
          "8": "",
          "9": "",
          "urgent": "",
          "focused": "",
          "default": ""
      }
}

```

* nano ~/.config/hypr/waybar/style.css

```bash
#custom-menu {
	border-radius: 0px;
}

/** ********** Sway ********** **/
#workspaces {
	background-color: #2F333C;
	color: #FFFFFF;
	border-radius: 0px 12px 0px 0px;
	padding: 4px;
}

#workspaces button {
	color: #FFFFFF;
	padding: 2px 6px;
}

#workspaces button:hover {
	background-color: #1B1F28;
	color: #FFFFFF;
	border: 0px solid #1B1F28;
	border-radius: 12px;
	padding: 2px 6px;
}

#workspaces button.visible {
	background-image: linear-gradient(to right, #62AEEF , #98C379);
}

#workspaces button.active {
	background-image: linear-gradient(to right, #E06B74 , #C778DD);
}

#workspaces button.urgent {
	background-image: linear-gradient(to right, #E06B74 , #E5C07A);
}

#workspaces button.persistent {
	background-image: linear-gradient(to right, #E5C07A , #C778DD);
}

#workspaces button.visible,
#workspaces button.active,
#workspaces button.urgent,
#workspaces button.persistent {
	color: #1B1F28;
	border-radius: 12px;
	padding: 2px 6px;
}

#workspaces button.current_output {
}

#workspaces button#sway-workspace-1 {
}
```

# Rice: ArchCraft

```bash
mv ~/.config/hypr ~/.config/hypr.bak
cp -r /mnt/disk2/linux-config/dotfiles-wayland/hypr-archcraft ~/.config/hypr
```

# Rice: [1amSimp1e](https://github.com/1amSimp1e/dots)

```bash
yay -S --needed rofi dunst kitty swaylock-fancy-git pamixer brillo
sudo pacman -S rustup
rustup default stable
yay -S --needed blueberry gnunet jaq pavucontrol ripgrep socat udev wget eww-wayland
yay -S swappy
sudo pacman -S ttf-jetbrains-mono-nerd
yay -S ttf-material-design-icons
wget https://github.com/iamverysimp1e/fonts/raw/master/product-sans.zip
sudo unzip product-sans.zip -d /usr/share/fonts/TTF
```

```bash
mv ~/.config/hypr ~/.config/hypr.bak
cd /mnt/disk2/linux-config/dotfiles-wayland/hypr-1amSimp1e
cp -r dunst eww hypr kitty rofi swaylock waybar ~/.config
```

# Rice: [fufexan](https://github.com/fufexan/dotfiles)

# Rice: [Amadeus](https://github.com/AmadeusWM/dotfiles-hyprland)
```bash
yay -S --needed  wofi dunst jq eww-wayland swayidle swaylockd sway-audio-idle-inhibit-git bc pamixer papirus-icon-theme playerctl cava kitty grim slurp wl-clipboard socat swappy cliphist hyprpicker-git nm-connection-editor dictd wl-clip-persist-git blueberry
cp -r ~/dotfiles-hyprland/* ~/.config/hypr
mv ~/.config/hypr/_hyprland.conf ~/.config/hypr/hyprland.conf
```

# Theme
-  KVantum: KvArcDark
- QT5 Setting:
    + Icon Theme: Archcraft-Dark
