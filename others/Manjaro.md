# My Manjaro KDE installation

# Setting
*  Kernel: Install latest and restart
*  Hardware Configuration: Auto install proprietary driver and restart
*  Language packages: Install packages
*  Time and Date --> Hardware clock in local timezone: true
*  Workspace Behavior --> General Behavior --> Animation speed: Instant
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen locking --> Lock screen: off
*  Shortcut --> Global Shortcut --> Defaults
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: untick alt+f2
*  Shortcut --> Global Shortcut --> KWin --> Hide window boder: Meta+Enter
*  Shortcut --> Global Shortcut --> KWin --> Maximum window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimum window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Windows (Current Desktop): Meta+Tab
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Yakuake: window+f12
*  Shortcut --> Global Shortcut --> Konsole: Ctrl+Alt+T
*  Startup and Shutdown --> Autostart --> Add program: KSysGuard
*  Regional Setting --> Language: English
*  Regional Settings --> Formats --> Region: Viet Nam
*  Regional Settings --> Formats --> Detail Setting --> Number: Default
*  Regional Settings --> Formats --> Detail Setting --> Currency: Default
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Poiter speed: 3
*  Input Devices --> Touchpad --> Poiter speed: 3
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Display and Monitor --> Compositor --> Animation speed: Instant
*  Display and Monitor --> Compositor --> Rendering backend: OpenGL 3.1
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On AC Power --> Wireless: On
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Wireless: On
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 15 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 30 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown

# Full system update
```bash
sudo pacman -Syyu
sudo pacman-mirrors --fasttrack && sudo pacman -Syyu
```

# Add AUR Repository
```bash
Add or remove software
Preference
```
```bash
Enable AUR support: true
Check update from AUR: true
```

# NVIDIA drivers (optional)
```bash
sudo pacman -S optimus-manager
sudo systemctl disable bumblebeed.service
cd /etc/X11/xorg.conf.d/
ls -la
sudo mv 90-mhwd.conf 90-mhwd.conf.bak
cd /etc/X11/
ls -la
sudo nano /etc/sddm.conf
put a # before the line starting with 'DisplayCommand and 'DisplayStopCommand'

GUI (or use pamac install --no-confirm)
optimus-manager-qt

Add auto-start optimus-manager-qt
Restart
```

- [Guide: Install and configure optimus-manager for hybrid GPU setups (Intel/NVIDIA)](https://forum.manjaro.org/t/guide-install-and-configure-optimus-manager-for-hybrid-gpu-setups-intel-nvidia/92196)

# Localization
```bash
sudo nano /etc/locale.gen

uncomment en_SG.UTF-8 vi_VN.UTF-8
```
```bash
sudo locale-gen
sudo nano /etc/locale.conf

LANG=en_SG.UTF-8
LC_TIME=vi_VN.UTF-8
LC_ADDRESS=C
LC_COLLATE=C
LC_MONETARY=C
LC_NUMERIC=C
```

# Time zone
```bash
sudo hwclock --systohc --localtime
```

# Disable PC Speaker
```bash
sudo rmmod pcspkr
sudo nano /etc/modprobe.d/nobeep.conf
```

```bash
# Do not load the 'pcspkr' module on boot.
blacklist pcspkr
```

# Disable KVM to stop shutdown error (optional)
```bash
sudo nano /etc/modprobe.d/nokvm.conf
```

```bash
blacklist kvm_intel
blacklist kvm_amd
blacklist kvm
```

# Automount partition
```bash
sudo mkdir /mnt/disk1
sudo mkdir /mnt/disk2
sudo mkdir /mnt/disk3
sudo mkdir /mnt/disk4
sudo blkid /dev/nvme0n1p2
sudo blkid /dev/nvme0n1p4
sudo blkid /dev/nvme0n1p5
sudo blkid /dev/nvme1n1p1
```

```bash
id -u
id -g

sudo nano /etc/fstab

UUID=A4DAA7C3DAA7905A   /mnt/disk1  ntfs         defaults,uid=1000,gid=1000 0 0
UUID=DA23-FE50          /mnt/disk2  exfat        defaults,uid=1000,gid=1000 0 0
UUID=8B74-9A30          /mnt/disk3  exfat        defaults,uid=1000,gid=1000 0 0
UUID=7111-6570          /mnt/disk4  exfat        defaults,uid=1000,gid=1000 0 0

```

# SDDM Auto numlock
```bash
sudo nano /etc/sddm.conf

Numlock=on
```

# Swapfile (optional)
```bash
sudo dd if=/dev/zero of=/swapfile bs=1M count=8192 status=progress
sudo chmod 0600 /swapfile
sudo mkswap -U clear /swapfile
sudo swapon /swapfile
```

```bash
sudo nano /etc/fstab

/swapfile none swap defaults 0 0
```

# AMD Pstate
```bash
sudo nano /etc/default/grub

amd_pstate=passive
```

```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Fix KDE bug
*  Fix check network bug

```bash
sudo nano /usr/lib/NetworkManager/conf.d/20-connectivity.conf
```

```bash
[connectivity]
#uri=http://www.archlinux.org/check_network_status.txt
uri=http://networkcheck.kde.org/
```

[Log in required for ethernet at home?”](https://www.reddit.com/r/ManjaroLinux/comments/keabph/log_in_required_for_ethernet_at_home/)

*  Fix shortcut bug

```bash
sudo nano ~/.config/plasma-org.kde.plasma.desktop-appletsrc
sudo nano ~/.config/kglobalshortcutsrc

Change Alt+F1 => Meta+Alt+F1
```

# Optimize makepfg
```bash
sudo nano /etc/makepkg.conf

MAKEFLAGS="-j$(nproc)"
```

# Add missing packages
```bash
sudo pacman -S --needed base-devel
sudo pacman -S linux-headers
sudo pacman -S --needed kde-applications
38 39 58 89 90 99 102 121
```

# Libre Office
```bash
sudo pacman -S libreoffice-fresh
```

# Yay
```bash
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin/
makepkg -si PKGBUILD
yay --editmenu --nodiffmenu --save
```

# Ryzen monitor
```bash
GUI (or use pamac install --no-confirm)

sudo modprobe msr

zenpower-dkms
zenmonitor

Edit zenmonitor PKGBUILD, add
mkdir -p "${pkgdir}/usr/share/polkit-1/actions"
make DESTDIR="${pkgdir}" PREFIX="/usr" install-polkit
```

# CPU Power (simple)
```bash
sudo nano /etc/default/cpupower

governor='conservative'
min_freq="1.4GHz"
max_freq="2.9GHz"

sudo systemctl enable cpupower
```

# Apps
```bash
psensor
google-chrome
vivaldi
vivaldi-ffmpeg-codecs
opera
skypeforlinux-stable-bin
telegram-desktop
discord
caprine
clementine
mpv
calibre
stellarium
gimp
kvantum-qt5
qbittorrent
subtitleeditor
lrzip
lzip
zip
rar
freerdp
libgdiplus
ttf-fira-code
ttf-cascadia-code
mailspring
gst-plugin-pipewire
```

# IDE & Compiler

# Git
```bash
GUI (or use pamac install --no-confirm)

gitflow-avh
gitkraken
git-credential-manager-core-bin
```

```bash
git config --global user.name "Duc Tran"
git config --global user.email tv.duc95@gmail.com
git config --global core.editor "nano -w"
git config --global core.autocrlf input
git config --global --add safe.directory '*'
```

*  Optional: use GCM

```bash
git-credential-manager configure
git config --global credential.credentialStore secretservice
```

*  Crack GitKraken

```bash
npm i
npm run-script build
sudo node dist/bin/gitcracken.js patcher
```

- [GitCracken](https://github.com/5cr1pt/GitCracken)

# C++
```bash
GUI (or use pamac install --no-confirm)

lldb
lld
libc++
```

# Go
```bash
GUI (or use pamac install --no-confirm)

go
go-tools
```

# Java
```bash
GUI (or use pamac install --no-confirm)

jdk-openjdk
java-openjfx
```

# .NET Core
```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

dotnet-host-bin
dotnet-sdk-bin
aspnet-runtime-bin
aspnet-targeting-pack-bin
dotnet-sdk-6.0-bin
dotnet-runtime-6.0-bin
aspnet-runtime-6.0-bin
aspnet-targeting-pack-6.0-bin
```

```bash
dotnet tool install --global dotnet-ef
dotnet tool install --global dnt
dotnet new -i Ardalis.CleanArchitecture.Template
dotnet new -i IdentityServer4.Templates
dotnet new -i Duende.IdentityServer.Templates
dotnet new -i Avalonia.Templates
dotnet new -i HotChocolate.Templates
nano ~/.xprofile

export PATH="$PATH:/home/ductran/.dotnet/tools"
```

* Azure

```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

azure-cli
```

```bash
npm install -g azurite
```

# Nodejs
```bash
nvm
source /usr/share/nvm/init-nvm.sh
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.bashrc
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.zshrc
nvm install node
nvm install --lts
```

Fix npm bin link on FAT32/exFAT file system
```bash
cd ~/.nvm/versions/node/v19.4.0/lib/node_modules/npm/node_modules/bin-links/
git apply /mnt/disk2/Projects/bin-links/fat32.patch

cd ~/.nvm/versions/node/v18.13.0/lib/node_modules/npm/node_modules/bin-links/
git apply /mnt/disk2/Projects/bin-links/fat32.patch
```

* Update file watcher

```bash
echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

npm install -g @angular/cli
npm install -g vite
```

# Android tools
```bash
GUI (or use pamac install --no-confirm)

android-tools
```

# VSCode
```bash
GUI (or use pamac install --no-confirm)

visual-studio-code-bin
```

# Codeblocks
```bash
GUI (or use pamac install --no-confirm)

codeblocks
```

# Jetbrains
```bash
GUI (or use pamac install --no-confirm)

webstorm
datagrip
goland
rider

Note: edit rider PKGBUILD _installdir='/opt'
```

```bash
sudo cp /opt/rider/bin/rider.svg /usr/share/pixmaps/rider.svg
sudo mv /opt/rider/jbr /opt/jbr
```

```bash
nano ~/.xprofile

export DATAGRIP_JDK=/opt/jbr
export RIDER_JDK=/opt/jbr
export WEBIDE_JDK=/opt/jbr
export GOLAND_JDK=/opt/jbr
```

# PostMan
```bash
GUI (or use pamac install --no-confirm)

postman-bin
```

# k6
```bash
GUI (or use pamac install --no-confirm)

k6-bin
```

# OpenSSH

* SSHFS

```bash
mkdir ~/vm
mkdir ~/vm/azure
mkdir ~/vm/aws
sshfs azureuser@azure:/home/azureuser ~/vm/azure
sshfs ec2-user@aws:/home/ec2-user ~/vm/aws

```

# Fcitx
```bash
GUI (or use pamac install --no-confirm)

fcitx5-im
fcitx5-chinese-addons fcitx5-unikey
```

* config

```bash
nano ~/.xprofile

export INPUT_METHOD=fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

- [fcitx shortcut](https://askubuntu.com/questions/736638/fcitx-wont-trigger-ime-on-superspace)

# Config file dialog
```bash
nano ~/.xprofile

export GTK_USE_PORTAL=1
```

# Chinese Font
```bash
sudo pacman -S noto-fonts-cjk noto-fonts-extra
sudo pacman -S ttf-roboto
sudo pacman -S adobe-source-han-serif-cn-fonts
sudo pacman -S adobe-source-han-sans-tw-fonts adobe-source-han-serif-tw-fonts
sudo pacman -S wqy-microhei wqy-zenhei wqy-bitmapfont
sudo pacman -S opendesktop-fonts
```

# Windows Font
```bash
sudo mkdir ~/.local/share/fonts
```

# Tweaks
*  Unpin all app in task bar
*  Configure System Tray --> Extra items: clipboard, Device notifier, Display Configuration, KDE Connect, Media Player, Notification, Printer, Weather report
*  Configure System Tray --> Entries --> Hidden all
*  Configure System Tray --> Entries --> Show: Notification
*  Configure Panel --> Screen Edge: Left
*  Pin favorite apps: System Settings, Dolphin, Add or Remove Software, Zenmonitor root, Vivaldi, Chrome, Firefox, qbittorrent, Skype, Telegram, Caprine, Discord, clementine, mpv, celluloid, subtitle editor, rider, goland, webstorm, datagrip, GitKraken, Visual Studio Code, pgadmin4, Postman
*  Edit panel --> Add widget --> Download new widget: Win7 volume mixer
*  Edit panel --> Add widget: Battery, Volume mixer, Network, Bluetooth, Touchpad, Input Method panel
*  Input Method panel --> Hide: fcitx, virtual keyboard, Telex, Unicode, Half width, Spell check, Macro, Full width input, No remind
*  Edit panel --> More Setting --> Auto hide

# Fix menu height
```bash
sudo nano /usr/share/plasma/plasmoids/org.kde.plasma.kickoff/contents/ui/FullRepresentation.qml

Layout.minimumHeight: implicitHeight * 1.5
```

# Theme (dark)
*  Global theme: Nordian Solid Global
*  Plasma style: Nordian Solid Plasma
*  Application Style: kvantum
*  GTK Theme: Nordian Breeze GTK
*  Window Decorators: Nordian Solid Aurorae
*  Color: kvantum
*  Icon: Breeze Nordian Dark Icon
*  SDDM: Nordian SDDM
*  Splash Screen: Nordian Solid Global
*  Kvantum: Nordian Kvantum
*  Wallpaper: Plasma Desktop Wallpaper 1591
*  Lock screen: Nordian-Plasma
*  Fix SDDM date Format

```bash
sudo nano /usr/share/sddm/themes/Nordian-SDDM/components/Clock.qml

"'The day is' dddd dd MMMM yyyy"
```

# Theme 2 (dark)
*  Global theme: Arc KDE
*  GTK Theme: Plane GTK Arc Dark
*  Icon: papirus dark
*  SDDM: KDE-Story
*  Application Style: kvantum
*  Kvantum: Arc Dark

# Theme 3 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

lightly-qt
```

*  Global theme: Breeze
*  Plasma style: Breeze Light
*  Application Style: Lightly
*  GTK Theme: Breeze
*  Window Decorators: Lightly
*  Color: Breeze Light
*  Icon: Breeze
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 4 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt
```

*  Global theme: aritim-light
*  Plasma style: aritim-light
*  Application Style: Lightly
*  GTK Theme: aritim-light
*  Window Decorators: aritim-light
*  Color: aritim-light
*  Icon: Papirus light
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 5 (dark)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt

sudo nano /usr/share/color-schemes/AritimDark.colors
[WM]
activeBackground=20,26,33,255
inactiveForeground=102,106,115,255

extract /mnt/disk2/Projects/linux-config/files/assets.zip to ~/.config/gtk-3.0/
copy /mnt/disk2/Projects/linux-config/files/share/ to /usr/share/
```

*  Global theme: aritim-dark
*  Plasma style: aritim-dark
*  Application Style: Lightly
*  GTK Theme: aritim-dark
*  Window Decorators: aritim-dark
*  Color: aritim-dark
*  Icon: Papirus dark
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 6 (dark)
*  Global theme: Qogir Dark
*  Plasma style: Qogir-dark
*  Application Style: Kvantum
*  GTK Theme: Qogir Dark
*  Icon: Qogir
*  SDDM: Qogir
*  Kvantum: Qogir Dark Solid
*  Wallpaper: Plasma 1591

```bash
mkdir ~/.icons
mkdir ~/.icons/default
cd ~/.icons/default
ln -sf cursors ~/.local/share/icons/Qogir-dark/cursors
nano index.theme

[icon theme]
Inherits=Qogir-dark

mv ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg.bak
```

# Theme 7 (dark)
*  Global theme: Aura Global
*  GTK Theme: Aura GTK
*  Icon: papirus dark
*  SDDM: Aura SDDM
*  Application Style: Breeze
*  Wallpaper: Wallpaper_Mikael_Gustafsson.png

```bash
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons.bak
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg.bak
```

# Plasmoid
*  Analog clock
*  System Monitor
*  hard disk activity
*  Network speed
