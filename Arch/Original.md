# My Legion 5 Arch KDE installation
[Installation guide](https://wiki.archlinux.org/index.php/Installation_guide)
[KDE](https://wiki.archlinux.org/index.php/KDE#Installation)

# Boot Arch
*  Boot Arch Linux (x86_64)

# Disable speaker
```bash
rmmod pcspkr
```

# Connect to Internet
```bash
iwctl device list
iwctl adapter phy0 set-property Powered on
iwctl station wlan0 scan
iwctl station wlan0 get-networks
iwctl station wlan0 connect SSID
```

* or use

```bash
iwctl station wlan0 connect SSID --passphrase passphrase
```

# Update the system clock (optional)
```bash
timedatectl set-timezone Asia/Ho_Chi_Minh
timedatectl set-ntp true
```

# Update pacman, set mirror
```bash
pacman -Syyy
```

* optional

```bash
pacman -S python
pacman -S reflector
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syyy
```

# Partition the disks
```bash
lsblk
cfdisk /dev/nvme1n1
```

* Choose Free space  
  Enter New  
  Then Write  
  Quit  
* Verify new partition:

```bash
lsblk
mkfs.fat -F 32 /dev/nvme1n1p2
mkfs.ext4 /dev/nvme1n1p3
lsblk
```

# Mount the file systems
```bash
mount /dev/nvme1n1p3 /mnt
lsblk
```

# Install Linux
```bash
pacstrap /mnt base base-devel linux linux-firmware linux-headers nano amd-ucode
```

# Configure the system
```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

# Chroot
```bash
arch-chroot /mnt
```

# Swapfile (optional)
```bash
dd if=/dev/zero of=/swapfile bs=1M count=8192 status=progress
chmod 0600 /swapfile
mkswap -U clear /swapfile
swapon /swapfile
```

```bash
nano /etc/fstab

/swapfile none swap defaults 0 0
```

# Time zone
* List timezones

```bash
timedatectl list-timezones
```

```bash
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
hwclock --systohc --localtime
```

# Localization
```bash
nano /etc/locale.gen

uncomment en_SG.UTF-8 vi_VN.UTF-8
```
```bash
locale-gen
nano /etc/locale.conf

LANG=en_SG.UTF-8
LC_TIME=vi_VN.UTF-8
LC_ADDRESS=C
LC_COLLATE=C
LC_MONETARY=C
LC_NUMERIC=C
```

# Network configuration
```bash
nano /etc/hostname

legion5
```

```bash
nano /etc/hosts

127.0.0.1	localhost
::1			  localhost
127.0.1.1	legion5.localdomain	legion5
```

# Root password
```bash
passwd
```

# GRUB and others
```bash
pacman -S grub efibootmgr os-prober ntfs-3g networkmanager network-manager-applet wpa_supplicant dialog mtools dosfstools bluez bluez-utils openssh sshfs
```

```bash
mkdir /boot/EFI
mount /dev/nvme1n1p2 /boot/EFI
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
```

# Services
```bash
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable sshd
```

# User
```bash
useradd -m -g users -G wheel,storage,network,power ductran
passwd ductran
EDITOR=nano visudo

uncomment # %wheel ALL=(ALL) ALL
```

# Reboot
```bash
exit
umount -a
reboot
```

# Connect to internet
login as root
```bash
ip a
nmtui
```

# Xorg
```bash
pacman -S xorg
```

# Graphic driver
```bash
pacman -S xf86-video-amdgpu xf86-video-nouveau
pacman -S vulkan-radeon
```

Optional: NVIDIA Driver

```bash
pacman -S nvidia nvidia-utils nvidia-settings
```

# Sound driver
```bash
pacman -S alsa-firmware alsa-utils alsa-plugins
```

```bash
pacman -S pipewire pipewire-alsa pipewire-pulse pipewire-jack
```

* or

```bash
pacman -S pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer
```

# Input driver (optional)
```bash
pacman -S xf86-input-elographics xf86-input-evdev xf86-input-void
```

# SDDM
```bash
pacman -S sddm
systemctl enable sddm
```

# KDE Plasma
```bash
Note: remove discover

pacman -S plasma sddm-kcm
pacman -S plasma-wayland-session
pacman -S kde-graphics 1 2 3 4 5 6 7 8 10 11
pacman -S kde-multimedia 3 4
pacman -S kde-system 1 2 4 5
pacman -S kde-utilities 1 2 3 5 6 9 13 14 16 17 19 20 23
pacman -S kdesdk 2 10
pacman -S --needed kde-applications 40 41 71 73 77 83 94 96
pacman -S ksysguard
pacman -R discover
```

* or

```bash
pacman -S kde-applications
```

# xdg desktop
```bash
pacman -S xdg-desktop-portal
```

# Libre Office
```bash
pacman -S libreoffice-fresh
```

# Browser
```bash
pacman -S firefox
```

# Git
```bash
pacman -S git
```

# Exfat
```bash
pacman -S exfatprogs
```

# Hardware video acceleration
```bash
GUI (or use pamac install --no-confirm)

libva-mesa-driver
mesa-vdpau
```

# Disable PC Speaker
```bash
sudo rmmod pcspkr
sudo nano /etc/modprobe.d/nobeep.conf
```

```bash
# Do not load the 'pcspkr' module on boot.
blacklist pcspkr
```

# Disable KVM to stop shutdown error (optional)
```bash
sudo nano /etc/modprobe.d/nokvm.conf
```

```bash
blacklist kvm_intel
blacklist kvm_amd
blacklist kvm
```

# Reboot
```bash
reboot
```

# Full system update
```bash
sudo pacman -Syu
sudo pacman -Syyu
```

# Setting
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen Locking --> Lock screen automatically: off
*  Shortcut --> Global Shortcut --> Defaults
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: untick alt+f2
*  Shortcut --> Global Shortcut --> KWin --> Hide window boder: Meta+Enter
*  Shortcut --> Global Shortcut --> KWin --> Maximize window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimize window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Windows (Current Desktop): Meta+Tab
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Yakuake: Meta+F12
*  Shortcut --> Global Shortcut --> Konsole: Ctrl+Alt+T
*  Startup and Shutdown --> Autostart --> Add program: Yakuake
*  Startup and Shutdown --> Autostart --> Add program: KSysGuard
*  Regional Settings --> Date & Time --> Set time auto: True
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Poiter speed: 5
*  Input Devices --> Touchpad --> Poiter speed: 5
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Sleep
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 5 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 15 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown

# Update time
```bash
sudo hwclock --systohc --localtime
```

# Optimize makepfg
```bash
sudo nano /etc/makepkg.conf

MAKEFLAGS="-j$(nproc)"
```

# Fix KDE bug
*  Fix shortcut bug

```bash
sudo nano ~/.config/plasma-org.kde.plasma.desktop-appletsrc
sudo nano ~/.config/kglobalshortcutsrc

Change Alt+F1 => Meta+Alt+F1
```

# Automount partition
```bash
sudo mkdir /mnt/disk1
sudo mkdir /mnt/disk2
sudo mkdir /mnt/disk3
sudo mkdir /mnt/disk4
sudo blkid /dev/nvme0n1p2
sudo blkid /dev/nvme0n1p4
sudo blkid /dev/nvme0n1p5
sudo blkid /dev/nvme1n1p1
```

```bash
id -u
id -g

sudo nano /etc/fstab

UUID=A4DAA7C3DAA7905A   /mnt/disk1  ntfs         defaults,uid=1000,gid=1000 0 0
UUID=DA23-FE50          /mnt/disk2  exfat        defaults,uid=1000,gid=1000 0 0
UUID=8B74-9A30          /mnt/disk3  exfat        defaults,uid=1000,gid=1000 0 0
UUID=7111-6570          /mnt/disk4  exfat        defaults,uid=1000,gid=1000 0 0

```

# SDDM Auto numlock
```bash
sddm --example-config | sudo tee /etc/sddm.conf
sudo nano /etc/sddm.conf

Numlock=on
```

# Yay
```bash
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin/
makepkg -si PKGBUILD
yay --editmenu --nodiffmenu --save
```

# Environment variable

```bash
nano ~/.xprofile

export EDITOR=nano
export GTK_USE_PORTAL=1
```

# Pamac
```bash
yay -S pamac-aur
```

# Add AUR Repository
```bash
Add or remove software
Preference
```

```bash
Enable AUR support: true
Check update from AUR: true
```

# Update mirrorlist
```bash
sudo pacman -S reflector
sudo reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

# Color Management (optional)
```bash
GUI (or use pamac install --no-confirm)

colord-kde

sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-18 11-33 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-26 20-14 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
```

# Ryzen monitor
```bash
GUI (or use pamac install --no-confirm)

sudo modprobe msr

zenpower-dkms
zenmonitor

Edit zenmonitor PKGBUILD, add
mkdir -p "${pkgdir}/usr/share/polkit-1/actions"
make DESTDIR="${pkgdir}" PREFIX="/usr" install-polkit
```

# CPU Power (simple)
```bash
GUI (or use pamac install --no-confirm)
cpupower

sudo nano /etc/default/cpupower

governor='conservative'
min_freq="1.4GHz"

sudo systemctl enable cpupower
```

# CPU Power (advanced)
```bash
GUI (or use pamac install --no-confirm)
tlp
tlpui

sudo systemctl enable tlp
```

```bash
sudo nano /etc/tlp.d/01.cpu.conf

CPU_SCALING_GOVERNOR_ON_AC=conservative
CPU_SCALING_GOVERNOR_ON_BAT=conservative

CPU_BOOST_ON_AC=1
CPU_BOOST_ON_BAT=0
```

# Disable CPU Turbo clock (optional)
```bash
sudo mkdir /opt/power-mode && cd $_
sudo nano no-turbo-boost.sh

#!/bin/bash

echo "0" | sudo tee /sys/devices/system/cpu/cpufreq/boost

sudo nano no-turbo-boost.service

[Unit]
Description=Disable Turbo Boost
After=acpid.socket
After=syslog.service

[Service]
User=root
Type=simple
ExecStart=/opt/power-mode/no-turbo-boost.sh
ExecStop=/opt/power-mode/no-turbo-boost.sh
TimeoutSec=30
StartLimitInterval=350

[Install]
WantedBy=multi-user.target

sudo chmod +x /opt/power-mode/no-turbo-boost.sh
cd /etc/systemd/system
sudo ln -s /opt/power-mode/no-turbo-boost.service no-turbo-boost.service
sudo chmod u+x /etc/systemd/system/no-turbo-boost.service
sudo systemctl enable no-turbo-boost
```

# AMD Pstate

```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="amd_pstate=passive"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Apps
```bash
GUI (or use pamac install --no-confirm)

psensor
vivaldi
vivaldi-ffmpeg-codecs
opera
opera-ffmpeg-codecs
telegram-desktop
discord
caprine
clementine
mpv
calibre
gimp
grub-customizer
kvantum
piper
qbittorrent
neofetch
htop
powertop
openvpn
networkmanager-openvpn
subtitleeditor
lrzip
lzip
p7zip
zip
unarchiver
freerdp
libgdiplus
ttf-fira-code
ttf-cascadia-code
gst-libav
gst-plugins-base
gst-plugins-good
gst-plugin-pipewire

authy
google-chrome
skypeforlinux-stable-bin
mailspring
stellarium-bin
rar
```

# Fix firefox (optional)
```bash
about:config
toolkit.legacyUserProfileCustomizations.stylesheets = true

create file chrome/userChrome.css

*|div#fullscr-toggler {display:none!important;}
```

# IDE & Compiler

# Git
```bash
GUI (or use pamac install --no-confirm)

gitflow-avh
gitkraken
git-credential-manager-core-bin
```

```bash
git config --global user.name "Duc Tran"
git config --global user.email tv.duc95@gmail.com
git config --global core.editor "nano -w"
git config --global core.autocrlf input
git config --global --add safe.directory '*'
```

*  Optional: use GCM

```bash
git-credential-manager configure
git config --global credential.credentialStore secretservice
```

*  Optional: use secret service

```bash
git config --global credential.helper libsecret
```

*  Optional: use ksshaskpass

```bash
git config --global core.askpass /usr/bin/ksshaskpass
sudo nano ~/.config/autostart-scripts/ssh-add.sh
#!/bin/sh
ssh-add -q < /dev/null

sudo nano ~/.config/plasma-workspace/env/askpass.sh
#!/bin/sh
export SSH_ASKPASS='/usr/bin/ksshaskpass'
export GIT_ASKPASS='/usr/bin/ksshaskpass'
```

# C++
```bash
GUI (or use pamac install --no-confirm)

clang
llvm
lldb
lld
libc++
```

# Go
```bash
GUI (or use pamac install --no-confirm)

go
go-tools
```

# Java
```bash
GUI (or use pamac install --no-confirm)

jdk-openjdk
java-openjfx
```

# .NET Core
```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

dotnet-host-bin
dotnet-sdk-bin
aspnet-runtime-bin
aspnet-targeting-pack-bin
dotnet-sdk-6.0-bin
dotnet-runtime-6.0-bin
aspnet-runtime-6.0-bin
aspnet-targeting-pack-6.0-bin
```

* or

```bash
Install .NET Core 6

sudo chmod 777 dotnet-install.sh
sudo ./dotnet-install.sh --channel 6 --install-dir /usr/share/dotnet
```

- [dotnet-install scripts](https://dot.net/v1/dotnet-install.sh)

```bash
dotnet tool install --global dotnet-ef
dotnet tool install --global dnt
dotnet new -i Ardalis.CleanArchitecture.Template
dotnet new -i IdentityServer4.Templates
dotnet new -i Duende.IdentityServer.Templates
dotnet new -i Avalonia.Templates
dotnet new -i HotChocolate.Templates
dotnet new -i MassTransit.Templates

nano ~/.xprofile

export PATH="$PATH:/home/ductran/.dotnet/tools"
```

* Azure

```bash
GUI (or use pamac install --no-confirm) (use AUR bin version to get update earlier)

azure-cli
```

```bash
npm install -g azurite
```

# Mono (optional)
```bash
GUI (or use pamac install --no-confirm)

mono
mono-tools
mono-addins
mono-msbuild
mono-msbuild-sdkresolver
xsp
```

* config to run ASP .NET project

```bash
sudo mkdir /etc/mono/registry
sudo chmod uog+rw /etc/mono/registry

edit web.config
<dependentAssembly>
    <assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
    <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="4.2.0.0" />
</dependentAssembly>
```

- [Access to the path “/etc/mono/registry” is denied](https://stackoverflow.com/questions/24872394/access-to-the-path-etc-mono-registry-is-denied)
- [How to get Swashbuckle / Swagger working on the Mono (.NET) Framework?](https://stackoverflow.com/questions/36062557/how-to-get-swashbuckle-swagger-working-on-the-mono-net-framework)

# Nodejs
```bash
GUI (or use pamac install --no-confirm)

nodejs
npm
```

* or

```bash
nvm
source /usr/share/nvm/init-nvm.sh
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.bashrc
echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.zshrc
nvm install node
nvm install --lts
nvm install lts/gallium
```

Fix npm bin link on FAT32/exFAT file system
```bash
cd ~/.nvm/versions/node/v19.6.0/lib/node_modules/npm/node_modules/bin-links/
git apply /mnt/disk2/Projects/bin-links/fat32.patch

cd ~/.nvm/versions/node/v18.14.0/lib/node_modules/npm/node_modules/bin-links/
git apply /mnt/disk2/Projects/bin-links/fat32.patch

cd ~/.nvm/versions/node/v16.19.0/lib/node_modules/npm/node_modules/bin-links/
git apply /mnt/disk2/Projects/bin-links/fat32.patch
```

* Install global packages

```bash
npm install -g @angular/cli
npm install -g vite
```

* Update file watcher (optional)

```bash
echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```

- [Install NodeJS via package manager](https://nodejs.org/en/download/package-manager/#arch-linux)

# Crack GitKraken

```bash
cd /mnt/disk2/Projects/GitCracken
npm i
npm run-script build
sudo node dist/bin/gitcracken.js patcher
```

- [GitCracken](https://github.com/5cr1pt/GitCracken)

# Android tools
```bash
GUI (or use pamac install --no-confirm)

android-tools
```

# Flutter (optional)
```bash
GUI (or use pamac install --no-confirm)

flutter
android-sdk
android-sdk-platform-tools
android-sdk-build-tools
android-sdk-cmdline-tools-latest
android-platform
```

```bash
sudo groupadd flutterusers
sudo gpasswd -a $USER flutterusers
sudo chown -R $USER:flutterusers /opt/flutter
sudo chmod -R g+w /opt/flutter/
```

```bash
sudo groupadd android-sdk
sudo gpasswd -a $USER android-sdk
sudo chown -R $USER:android-sdk /opt/android-sdk
sudo chmod -R g+w /opt/android-sdk
```

```bash
sudo nano ~/.xprofile

#Java
export JAVA_HOME='/usr/lib/jvm/java-14-openjdk/'

# Android SDK
export ANDROID_SDK_ROOT='/opt/android-sdk'
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/"

# Flutter
export PATH="$PATH:/opt/flutter/bin"
```
```bash
flutter doctor
flutter doctor --android-licenses
```

# MariaDB (optional)
```bash
GUI (or use pamac install --no-confirm)

mariadb
```

```bash
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb
sudo mysql_secure_installation
```

- [MariaDB - ArchWiki](https://wiki.archlinux.org/index.php/MariaDB)

# PostgreSQL (optional)
```bash
GUI (or use pamac install --no-confirm)
postgresql
libxcrypt-compat
libffi6
azuredatastudio-bin
```

```bash
sudo su postgres -l
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
sudo systemctl start postgresql
sudo su postgres -l
createuser --interactive --pwprompt
exit
sudo systemctl stop postgresql
sudo systemctl disable postgresql
```

SSL config:

```bash
sudo su postgres -l
cd /var/lib/postgres/data

openssl req -new -x509 -days 3650 -nodes -text -out server.crt -keyout server.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx server.key

openssl req -new -nodes -text -out root.csr -keyout root.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx root.key

openssl x509 -req -in root.csr -text -days 3650 -extfile /etc/ssl/openssl.cnf -extensions v3_ca -signkey root.key -out root.crt

openssl req -new -nodes -text -out server.csr -keyout server.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx server.key

openssl x509 -req -in server.csr -text -days 3650 -CA root.crt -CAkey root.key -CAcreateserial -out server.crt
```

- [PostgreSQL - ArchWiki](https://wiki.archlinux.org/index.php/PostgreSQL)

# MS SQL Server (optional)
- Download https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt

```bash
gpg --import ./gpg-pubkey.txt
```

```bash
GUI (or use pamac install --no-confirm)

mssql-server
```

```bash
sudo /opt/mssql/bin/mssql-conf setup
sudo systemctl stop mssql-server
sudo systemctl disable mssql-server
```

- [Installation guidance for SQL Server on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver15)

# MongoDB (optional)
```bash
GUI (or use pamac install --no-confirm)

mongodb-compass
mongodb-tools-bin
mongosh-bin
mongodb-bin
```

```bash
sudo systemctl start mongodb
mongosh
use admin
```

```bash
db.createUser(
  {
    user: "ductran",
    pwd: "Password",
    roles: [ { role: 'root', db: 'admin' }, "readWriteAnyDatabase" ]
  }
)
```

```bash
sudo nano /etc/mongodb.conf
```

```bash
security:
  authorization: "enabled"
```

```bash
sudo systemctl stop mongodb
sudo systemctl disable mongodb
```

# VSCode
```bash
GUI (or use pamac install --no-confirm)

visual-studio-code-bin
```

# Codeblocks
```bash
GUI (or use pamac install --no-confirm)

codeblocks
```

# Jetbrains
```bash
GUI (or use pamac install --no-confirm)

webstorm
datagrip
goland
rider

Note: edit rider PKGBUILD _installdir='/opt'
```

```bash
sudo cp /opt/rider/bin/rider.svg /usr/share/pixmaps/rider.svg
sudo mv /opt/rider/jbr /opt/jbr
```

```bash
nano ~/.xprofile

export DATAGRIP_JDK=/opt/jbr
export RIDER_JDK=/opt/jbr
export WEBIDE_JDK=/opt/jbr
export GOLAND_JDK=/opt/jbr
```

* Install JDK with Fcitx fix (optional)

```bash
sudo rm -rf /opt/rider/jbr
sudo ark -b -o /opt /mnt/disk3/Software/jbr-linux-x64-202110301849.zip
```

# Redis (optional)
```bash
GUI (or use pamac install --no-confirm)

redis
redisinsight
```

# RabbitMQ (optional)
```bash
GUI (or use pamac install --no-confirm)

rabbitmq
```

# Apache (optional)
```bash
GUI (or use pamac install --no-confirm)

apache
```

# Nginx (optional)
```bash
GUI (or use pamac install --no-confirm)

nginx
```

```bash
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo mkdir /etc/nginx/conf.d
sudo cp nginx.conf /etc/nginx/nginx.conf
sudo cp 127.0.0.1.conf /etc/nginx/conf.d/127.0.0.1.conf
```

# FTP Server (optional)
```bash
GUI (or use pamac install --no-confirm)

vsftpd
filezilla
```

# PostMan
```bash
GUI (or use pamac install --no-confirm)

postman-bin
```

# k6
```bash
GUI (or use pamac install --no-confirm)

k6-bin
```

# Docker (optional)
```bash
GUI (or use pamac install --no-confirm)

docker
docker-compose
```

```bash
sudo usermod -aG docker ductran
```

# OpenSSH

* SSHFS

```bash
sudo mkdir /mnt/aws
sudo mkdir /mnt/oracle
sudo chmod 777 -R /mnt/aws
sudo chmod 777 -R /mnt/oracle

sshfs arch@aws:/home/arch /mnt/aws
sshfs debian@oracle:/home/debian /mnt/oracle
```

# Fcitx
```bash
GUI (or use pamac install --no-confirm)

fcitx5-im
fcitx5-chinese-addons fcitx5-unikey
```

* config

```bash
nano ~/.xprofile

export INPUT_METHOD=fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

- [fcitx shortcut](https://askubuntu.com/questions/736638/fcitx-wont-trigger-ime-on-superspace)

# iBus (optional)
```bash
GUI (or use pamac install --no-confirm)

ibus ibus-pinyin ibus-unikey ibus-bamboo

Edit ibus-bamboo PKGBUILD pkgver=0.6.7 sha256sums=('SKIP')
```

* config

```bash
sudo nano ~/.xprofile

export INPUT_METHOD=ibus
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
```

```bash
sudo nano /usr/share/applications/ibus-daemon.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx

sudo nano /usr/share/applications/ibus-daemon-kimpanel.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx --panel=/usr/lib/kimpanel-ibus-panel

sudo cp /usr/share/applications/ibus-daemon.desktop /etc/xdg/autostart/
```

# KeepassXC (optional)
```bash
GUI (or use pamac install --no-confirm)

keepassxc

create database ~/Passwords with key ~/Passwords
```

```bash
nano ~/.xprofile

export PATH="$PATH:/home/ductran/bin"
```

```bash
copy /mnt/disk2/Projects/linux-config/files/applications/ to ~/.local/share/applications/
copy /mnt/disk2/Projects/linux-config/files/bin/ to ~/bin/
```

```bash
Add keepassxc-startup, keepassxc-watch to autostart scripts
```

- [Automatically unlock KeepassXC on startup and after lock screen](https://grabski.me/tech,/linux/2020/09/02/automatically-unlock-keepassxc-on-startup-and-after-lock-screen/)

# PulseEffects (optional)
```bash
GUI (or use pamac install --no-confirm)

pulseeffects

start PulseEffects
Audio Gain => max
```

- [Community Presets](https://github.com/wwmm/pulseeffects/wiki/Community-presets)
- [How to mimic Dolby Audio Premium on Linux with PulseEffects](https://www.linuxupdate.co.uk/2020/10/19/how-to-mimic-dolby-audio-premium-on-linux-with-pulseeffects/)

# Steam (optional)

*  NVIDIA PRIME

```bash
GUI (or use pamac install --no-confirm)

nvidia-prime
```

*  Steam

```bash
nano /etc/pacman.conf

uncomment
#[multilib]
#Include = /etc/pacman.d/mirrorlist
```

```bash
sudo pacman -Syyu
sudo pacman -S lib32-mesa
sudo pacman -S lib32-vulkan-radeon
sudo pacman -S lib32-vulkan-icd-loader
sudo pacman -S lib32-nvidia-utils
```

```bash
GUI (or use pamac install --no-confirm)

steam
```

```bash
mkdir /home/ductran/.local/share/Steam/steamapps/compatdata
ln -s /home/ductran/.local/share/Steam/steamapps/compatdata /mnt/disk3/Game/Steam/steamapps/compatdata
mkdir "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0"
ln -s "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0" "/mnt/disk3/Game/Steam/steamapps/common/Proton 7.0"
mkdir /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier
ln -s /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier /mnt/disk3/Game/Steam/steamapps/common/SteamLinuxRuntime_soldier
```

Run game witch "prime-run %command%" argument

*  Optional

```bash
sudo pacman -S nvidia-dkms
sudo pacman -S linux-zen linux-zen-headers
```

```bash
mkdir /home/ductran/.local/share/Steam/compatibilitytools.d
tar -xf Proton-6.12-GE-1.tar.gz -C ~/.steam/root/compatibilitytools.d/
```

# Chinese Font
```bash
sudo pacman -S noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
sudo pacman -S ttf-roboto ttf-dejavu ttf-droid ttf-inconsolata ttf-indic-otf
sudo pacman -S terminus-font
sudo pacman -S adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts
sudo pacman -S adobe-source-han-sans-tw-fonts adobe-source-han-serif-tw-fonts
sudo pacman -S wqy-microhei wqy-zenhei wqy-bitmapfont
sudo pacman -S opendesktop-fonts
```

*  optional: typographic font

```bash
sudo pacman -S noto-fonts-sc noto-fonts-tc
sudo pacman -S ttf-arphic-ukai ttf-arphic-uming
```

# Windows Font
```bash
sudo mkdir ~/.local/share/fonts
```

- [Microsoft fonts](https://wiki.archlinux.org/index.php/Microsoft_fonts)

# GRUB Backlight Fix (optinal)

```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="acpi_backlight=video loglevel=3 quiet"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# GRUB Theme

*  Theme 1

```bash
sudo nano /etc/default/grub

GRUB_GFXMODE=1920x1080x32
```

```bash
git clone https://github.com/vinceliuice/grub2-themes.git
cd grub2-themes
sudo ./install.sh -b -t tela
```

- [vinceliuice/grub2-themes](https://github.com/vinceliuice/grub2-themes)

*  Theme 2

```bash
yay -S grub2-theme-archxion
sudo nano /etc/default/grub

Replace #GRUB_THEME="/path/to/gfxtheme" by GRUB_THEME="/boot/grub/themes/Archxion/theme.txt"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

- [Generator/Grub2-themes](https://github.com/Generator/Grub2-themes)

# Tweaks
*  Unpin all app in task bar
*  Configure System Tray --> Extra items: clipboard, Device notifier, Display Configuration, KDE Connect, Media Player, Notification, Printer, Weather report
*  Configure System Tray --> Entries --> Hidden all
*  Configure System Tray --> Entries --> Show: Notification
*  Configure Panel --> Screen Edge: Left
*  Pin favorite apps: System Settings, Dolphin, Add or Remove Software, Zenmonitor root, Vivaldi, Chrome, Firefox, qbittorrent, Skype, Telegram, Caprine, Discord, clementine, mpv, celluloid, subtitle editor, rider, goland, webstorm, datagrip, GitKraken, Visual Studio Code, pgadmin4, Postman
*  Edit panel --> Add widget: Battery, Volume mixer, Network, Bluetooth, Touchpad, Input Method panel
*  Input Method panel --> Hide: fcitx, virtual keyboard, Telex, Unicode, Half width, Spell check, Macro, Full width input, No remind
*  Edit panel --> More Setting --> Auto hide
*  Edit panel --> More Setting --> Opaque

# Fix menu height (optional)
```bash
sudo nano /usr/share/plasma/plasmoids/org.kde.plasma.kickoff/contents/ui/FullRepresentation.qml

Layout.minimumHeight: implicitHeight * 1.5
```

# Theme (dark)
```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
dexy-theme-git
```

*  Global theme: Dexy Global
*  Application Style: Breeze
*  Plasma style: Dexy Plasma
*  Color: DexyDarkColor
*  Window Decorators: Dexy Aurorae
*  Icon: Papirus Dark
*  Cursor: Breeze
*  Splash Screen: Arc Dark
*  GTK Theme: Dexy GTK
*  SDDM: ArcMidnight-dark
*  Lock screen: Arc Mountains
*  Wallpaper type: Slideshow
*  Wallpaper folders: /mnt/disk4/Photo/Desktop/

* Optional

```bash
mv /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/opaque /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/opaque.bak
mv /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/translucent /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/translucent.bak
```

# Theme 1 (dark)
*  Global theme: Nordian Solid Global
*  Plasma style: Nordian Solid Plasma
*  Application Style: kvantum
*  GTK Theme: Nordian Breeze GTK
*  Window Decorators: Nordian Solid Aurorae
*  Color: kvantum
*  Icon: Breeze Nordian Dark Icon
*  SDDM: Nordian SDDM
*  Splash Screen: Nordian Solid Global
*  Kvantum: Nordian Kvantum
*  Wallpaper: Plasma Desktop Wallpaper 1591
*  Lock screen: Nordian-Plasma
*  Fix SDDM date Format

```bash
sudo nano /usr/share/sddm/themes/Nordian-SDDM/components/Clock.qml

"'The day is' dddd dd MMMM yyyy"
```

# Theme 2 (dark)
*  Global theme: Arc KDE
*  GTK Theme: Plane GTK Arc Dark
*  Icon: papirus dark
*  SDDM: KDE-Story
*  Application Style: kvantum
*  Kvantum: Arc Dark

# Theme 3 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

lightly-qt
```

*  Global theme: Breeze
*  Plasma style: Breeze Light
*  Application Style: Lightly
*  GTK Theme: Breeze
*  Window Decorators: Lightly
*  Color: Breeze Light
*  Icon: Breeze
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 4 (light)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt
```

*  Global theme: aritim-light
*  Plasma style: aritim-light
*  Application Style: Lightly
*  GTK Theme: aritim-light
*  Window Decorators: aritim-light
*  Color: aritim-light
*  Icon: Papirus light
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 5 (dark)
*  Install

```bash
GUI (or use pamac install --no-confirm)

papirus-icon-theme
lightly-qt

sudo nano /usr/share/color-schemes/AritimDark.colors
[WM]
activeBackground=20,26,33,255
inactiveForeground=102,106,115,255

extract /mnt/disk2/Projects/linux-config/files/assets.zip to ~/.config/gtk-3.0/
copy /mnt/disk2/Projects/linux-config/files/share/ to /usr/share/
```

*  Global theme: aritim-dark
*  Plasma style: aritim-dark
*  Application Style: Lightly
*  GTK Theme: aritim-dark
*  Window Decorators: aritim-dark
*  Color: aritim-dark
*  Icon: Papirus dark
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 6 (dark)
*  Global theme: Qogir Dark
*  Plasma style: Qogir-dark
*  Application Style: Kvantum
*  GTK Theme: Qogir Dark
*  Icon: Qogir
*  SDDM: Qogir
*  Kvantum: Qogir Dark Solid
*  Wallpaper: Plasma 1591

```bash
mkdir ~/.icons
mkdir ~/.icons/default
cd ~/.icons/default
ln -sf cursors ~/.local/share/icons/Qogir-dark/cursors
nano index.theme

[icon theme]
Inherits=Qogir-dark

mv ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg.bak
```

# Theme 7 (dark)
*  Global theme: Aura Global
*  GTK Theme: Aura GTK
*  Icon: papirus dark
*  SDDM: Aura SDDM
*  Application Style: Breeze
*  Wallpaper: Wallpaper_Mikael_Gustafsson.png

```bash
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons.bak
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg.bak
```

# Plasmoid
*  Analog clock
*  System Monitor
*  hard disk activity
*  Network speed

# Zsh
```bash
GUI (or use pamac install --no-confirm)

zsh
oh-my-zsh-git
oh-my-zsh-plugin-syntax-highlighting
zsh-theme-powerlevel10k-git
powerline-fonts
awesome-terminal-fonts
ttf-meslo-nerd-font-powerlevel10k
```

```bash
sudo ln -sf /usr/share/zsh-theme-powerlevel10k/ /usr/share/oh-my-zsh/themes/powerlevel10k
zsh
chsh -s $(which zsh)
```

Copy config files

- .p10k.zsh
- .zshrc

# Tips & Tricks
*  Pacman remove unused packages

```bash
sudo pacman -Rs $(pacman -Qdtq)
```

*  Reduce log file

```bash
sudo journalctl --vacuum-time=1weeks

sudo nano /etc/systemd/journald.conf

SystemMaxUse=50M
```

*  Fix Brightness

```bash
sudo nano /sys/class/backlight/amdgpu_bl0/brightness

255
```

*  Clear cache files

```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
```
