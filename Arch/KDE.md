# My Legion 5 Arch KDE installation
[Installation guide](https://wiki.archlinux.org/index.php/Installation_guide)
[KDE](https://wiki.archlinux.org/index.php/KDE#Installation)

# Boot Arch
* Edit boot/grub/grub.cfg: comment play 600 988 1 1319 4
*  Boot Arch Linux (x86_64)

# Copy install.sh file
```bash
script install.log
mkdir /tmp/usb
mount /dev/sda1 /tmp/usb
cp /tmp/usb/install.sh ./
chmod 777 install.sh
umount /dev/sda1
```

# Prepare
```bash
./install.sh wifi <ssid> <pass>
ip a
./install.sh prepare
```

# Partition the disks
```bash
lsblk
cfdisk /dev/nvme1n1
```

* Choose Free space  
  Enter New  
  Then Write  
  Quit  
* Verify new partition:

```bash
lsblk
```

# Install Linux
* check mirrorlist
```bash
./install.sh arch <boot_partition> <root_partition>
arch-chroot /mnt
chmod 777 install.sh
lsblk
```

# Install KDE
```bash
/install.sh kde-full <boot_partition>
su ductran
/install.sh kde-aur
exit
```

```bash
passwd
passwd ductran
sudo EDITOR=nano visudo

## Comment
%wheel ALL=(ALL:ALL) NOPASSWD: ALL
```

# Reboot
```bash
exit
exit
cp install.log /mnt
cat install.log | grep -i 'timeout'
umount -a
poweroff
```

# Connect Wifi
* Use traditional encrypt

# Setting
*  Workspace Behavior --> General Behavior --> Click behavior: Double click
*  Workspace Behavior --> Desktop Effect --> Remove Screen Edge
*  Workspace Behavior --> Screen Edges --> No
*  Workspace Behavior --> Screen Locking --> Lock screen automatically: off
*  Workspace Behavior --> Virtual Desktops --> 2 rows
*  Workspace Behavior --> Virtual Desktops --> Main, Develop, Multimedia, Setting
*  Window Management --> Task Switcher --> Thumbnail Grid
*  Shortcut --> Global Shortcut --> Import Windows cheme with win key
*  Shortcut --> Global Shortcut --> Krunner: Meta+R
*  Shortcut --> Global Shortcut --> KWin --> Make window fullscreen: Meta+F11
*  Shortcut --> Global Shortcut --> KWin --> Maximize window: Meta+PageUp
*  Shortcut --> Global Shortcut --> KWin --> Minimize window: Meta+PageDown
*  Shortcut --> Global Shortcut --> KWin --> Setup window shortcut: Meta+S
*  Shortcut --> Global Shortcut --> KWin --> Show Desktop Grid: Meta+W
*  Shortcut --> Global Shortcut --> KWin --> Suspend Compositing: Untick
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 1: Meta+1
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 2: Meta+2
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 3: Meta+3
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 4: Meta+4
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 5: Meta+5
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 6: Meta+6
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 7: Meta+7
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 8: Meta+8
*  Shortcut --> Global Shortcut --> KWin --> Switch to Desktop 9: Meta+9
*  Shortcut --> Global Shortcut --> KWin --> Toggle Overview: Meta+A
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (All Desktop): Meta+G
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (Current Desktop): Meta+H
*  Shortcut --> Global Shortcut --> KWin --> Toggle Present Widnows (Window Class): Untick
*  Shortcut --> Global Shortcut --> KWin --> Toggle Window titlebar and frame: Meta+F
*  Shortcut --> Global Shortcut --> KWin --> Walk through desktop: Meta+Tab
*  Shortcut --> Global Shortcut --> KWin --> Walk through desktop (Reverse): Meta+Shift+Tab
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 1: Meta+Shift+1
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 2: Meta+Shift+2
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 3: Meta+Shift+3
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 4: Meta+Shift+4
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 5: Meta+Shift+5
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 6: Meta+Shift+6
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 7: Meta+Shift+7
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 8: Meta+Shift+8
*  Shortcut --> Global Shortcut --> KWin --> Window to Desktop 9: Meta+Shift+9
*  Shortcut --> Global Shortcut --> Plasma --> Activate Application Laucher: untick Alt+F1
*  Shortcut --> Global Shortcut --> Plasma --> Show desktop: untick Ctrl+F12
*  Shortcut --> Global Shortcut --> Konsole: Ctrl+Alt+T
*  Startup and Shutdown --> Autostart --> Add program: KSysGuard, Yakuake
*  Regional Settings --> Date & Time --> Set time auto: True
*  Input Devices --> Keyboard --> Numlock: On
*  Input Devices --> Mouse --> Acceleration profile: Flat
*  Input Devices --> Touchpad --> Acceleration profile: Flat
*  Input Devices --> Touchpad --> Tapping --> Tap-to-click: true
*  Display and Monitor --> Display Configuration --> Scale: 100%
*  Power Management --> Energy Saving --> On AC Power --> Brightness: 100%
*  Power Management --> Energy Saving --> On AC Power --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On AC Power --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On AC Power --> When laptop lid close: Turn off screen
*  Power Management --> Energy Saving --> On Battery --> Brightness: 100%
*  Power Management --> Energy Saving --> On Battery --> Dim screen: 30 min
*  Power Management --> Energy Saving --> On Battery --> Screen energy saving: 35 min
*  Power Management --> Energy Saving --> On Battery --> Suspend session: Off
*  Power Management --> Energy Saving --> On Battery --> When laptop lid close: Sleep
*  Power Management --> Energy Saving --> On Low Battery --> Brightness: 40%
*  Power Management --> Energy Saving --> On Low Battery --> Dim screen: 5 min
*  Power Management --> Energy Saving --> On Low Battery --> Screen energy saving: 10 min
*  Power Management --> Energy Saving --> On Low Battery --> Suspend session: 15 min
*  Power Management --> Advanced Settings --> At critical level: Shutdown

# Shell
```bash
chsh -s /bin/zsh
sudo hwclock --systohc --localtime
```

# GRUB
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# Install patch dev
```bash
./install.sh patch-dev
```

- [GitCracken](https://github.com/5cr1pt/GitCracken)

# Config fcitx
*  Input device --> Virtual Keyboard --> Fcitx 5
*  Regional Setting --> Input Method: Add Unikey, Pinyin, setup shortcut

# Install global theme with Discover
*  Qogir dark kde theme
*  Qogir light kde theme
*  Dexy-Global
*  Aritim-Dark
*  Aritim-Light

# Install Login Screen with Discover
*  Dexy-Color-SDDM

# Install Splash Screen with Discover
*  Dexy-Color-Splash
*  Peace-Color-Splash
*  Gently-Color-Splash

# Install GTK theme with Discover
*  Qogir theme
*  Dexy-GTK
*  Aritim-Dark
*  Aritim-Light

# Install Konsole theme with Discover
*  Aritim-Dark
*  Aritim-Light

# Install Plasma Widget with Discover
*  Control Centre

# Install rice
```bash
./install.sh rice
```

# Setting theme
*  Global theme: Aritim Dark
*  Application style: Breeze
*  GTK Application style: Aritim Dark GTK
*  Plasma style: Aritim Dark Flat Solid
*  Color: Aritim Dark
*  Decorator: Aritim Dark
*  Icon: Papirus
*  Splash screen: Peace-Color-Splash
*  SDDM: Dexy-Color-SDDM

# Fix GTK
```bash
./install.sh fix-gtk
```

# Config Konsole
*  Switch default theme: Aritim Dark

# Setting
*  User --> Change avatar
*  Config Desktop Wallpaper --> Slideshow
*  Workspace Behavior --> Screen Locking --> Appearance
*  Update default application

# Customize
*  Taskbar: Left
*  System Tray:
   - Show: Notification
   - Hide: Update, Yakuake, psensor, Disk, Display Config, KDE Connect, Clipboard, Media Player, Vault, Weather
   - Disabled: others
*  Add widget: Battery, Audio Volume, Network, Bluetooth, Touchpad, Control Center, Input Method Panel
*  Favorite: System Settings, Dolphin, System Monitor, Discover, Vivaldi, Chrome, Firefox, Opera, Skype, Telegram, Thunderbird, qBittorrent, Strawberry, mpv, jellyfin, stellarium, rider, goland, webstorm, datagrip, GitKraken, Postman, Visual Studio Code, Authy
*  Taskbar: Auto hide

# Plasmoid
*  Analog clock
*  System Monitor
*  Hard disk activity
*  Network speed

*  Update System Monitor plasmoid

```bash
edit with kate ~/.config/plasma-org.kde.plasma.desktop-appletsrc

Add sensor

highPrioritySensorIds=["cpu/all/usage"]
lowPrioritySensorIds=["memory/physical/application"]

Update color

cpu/all/usage=170,85,255
disk/all/read=245,134,41
disk/all/write=57,171,152
network/all/download=131,199,93
network/all/upload=255,92,65
```

#  Reboot

```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
reboot
```


# Tips & Tricks
*  Pacman remove unused packages

```bash
sudo pacman -Rs $(pacman -Qdtq)
```

*  Check error log

```bash
journalctl -p err..alert
```

*  Clear cache files

```bash
sudo rm -rfv /tmp/*
sudo rm -rfv ~/.cache/*
```

*  Disable CPU boost

```bash
sudo sh -c "echo 0 > /sys/devices/system/cpu/cpufreq/boost"
```

*  Reduce log file

```bash
sudo journalctl --vacuum-time=1weeks

sudo nano /etc/systemd/journald.conf

SystemMaxUse=50M
```

*  Fix Brightness

```bash
sudo nano /sys/class/backlight/amdgpu_bl0/brightness

255
```

# Windows Font
- [Microsoft fonts](https://wiki.archlinux.org/index.php/Microsoft_fonts)


# Theme (dark)
*  Install

```bash
extract /mnt/disk2/Projects/linux-config/files/assets.zip to ~/.config/gtk-3.0/
copy /mnt/disk2/Projects/linux-config/files/share/ to /usr/share/
```

*  Global theme: aritim-dark
*  Plasma style: aritim-dark
*  Application Style: Lightly
*  GTK Theme: aritim-dark
*  Window Decorators: aritim-dark
*  Color: aritim-dark
*  Icon: Papirus dark
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme (dark)
*  Global theme: Dexy Global
*  Application Style: Breeze
*  Plasma style: Dexy Plasma
*  Color: DexyDarkColor
*  Window Decorators: Dexy Aurorae
*  Icon: Papirus Dark
*  Cursor: Breeze
*  Splash Screen: Arc Dark
*  GTK Theme: Dexy GTK
*  SDDM: ArcMidnight-dark
*  Lock screen: Arc Mountains
*  Wallpaper type: Slideshow
*  Wallpaper folders: /mnt/disk4/Photo/Desktop/

* Optional

```bash
mv /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/opaque /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/opaque.bak
mv /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/translucent /home/ductran/.local/share/plasma/desktoptheme/Dexy-Plasma/translucent.bak
```

# Theme 1 (dark)
*  Global theme: Nordian Solid Global
*  Plasma style: Nordian Solid Plasma
*  Application Style: kvantum
*  GTK Theme: Nordian Breeze GTK
*  Window Decorators: Nordian Solid Aurorae
*  Color: kvantum
*  Icon: Breeze Nordian Dark Icon
*  SDDM: Nordian SDDM
*  Splash Screen: Nordian Solid Global
*  Kvantum: Nordian Kvantum
*  Wallpaper: Plasma Desktop Wallpaper 1591
*  Lock screen: Nordian-Plasma
*  Fix SDDM date Format

```bash
sudo nano /usr/share/sddm/themes/Nordian-SDDM/components/Clock.qml

"'The day is' dddd dd MMMM yyyy"
```

# Theme 2 (dark)
*  Global theme: Arc KDE
*  GTK Theme: Plane GTK Arc Dark
*  Icon: papirus dark
*  SDDM: KDE-Story
*  Application Style: kvantum
*  Kvantum: Arc Dark

# Theme 3 (light)

*  Global theme: Breeze
*  Plasma style: Breeze Light
*  Application Style: Lightly
*  GTK Theme: Breeze
*  Window Decorators: Lightly
*  Color: Breeze Light
*  Icon: Breeze
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 4 (light)

*  Global theme: aritim-light
*  Plasma style: aritim-light
*  Application Style: Lightly
*  GTK Theme: aritim-light
*  Window Decorators: aritim-light
*  Color: aritim-light
*  Icon: Papirus light
*  SDDM: Breeze
*  Splash Screen: Breeze
*  Wallpaper: Kite

# Theme 5 (dark)
*  Global theme: Qogir Dark
*  Plasma style: Qogir-dark
*  Application Style: Kvantum
*  GTK Theme: Qogir Dark
*  Icon: Qogir
*  SDDM: Qogir
*  Kvantum: Qogir Dark Solid
*  Wallpaper: Plasma 1591

```bash
mkdir ~/.icons
mkdir ~/.icons/default
cd ~/.icons/default
ln -sf cursors ~/.local/share/icons/Qogir-dark/cursors
nano index.theme

[icon theme]
Inherits=Qogir-dark

mv ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg ~/.local/share/plasma/desktoptheme/Qogir-dark/solid/widgets/panel-background.svg.bak
```

# Theme 6 (dark)
*  Global theme: Aura Global
*  GTK Theme: Aura GTK
*  Icon: papirus dark
*  SDDM: Aura SDDM
*  Application Style: Breeze
*  Wallpaper: Wallpaper_Mikael_Gustafsson.png

```bash
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/icons.bak
mv /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg /home/ductran/.local/share/plasma/desktoptheme/Aura-Plasma/widgets/configuration-icons.svg.bak
```

# Update mirrorlist (optional)
```bash
sudo reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

# Color Management (optional)
```bash
GUI (or use pamac install --no-confirm)

colord-kde

sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-18 11-33 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
sudo cp "/mnt/disk3/Software/Windows/B156HAN09.2 #1 2021-03-26 20-14 D6500 2.2 F-S XYZLUT+MTX.icm" /usr/share/color/icc/
```

# Fix firefox (optional)
```bash
about:config
toolkit.legacyUserProfileCustomizations.stylesheets = true

create file chrome/userChrome.css

*|div#fullscr-toggler {display:none!important;}
```

# Steam (optional)

*  NVIDIA PRIME

```bash
GUI (or use pamac install --no-confirm)

nvidia-prime
```

*  Steam

```bash
nano /etc/pacman.conf

uncomment
#[multilib]
#Include = /etc/pacman.d/mirrorlist
```

```bash
sudo pacman -Syyu
sudo pacman -S lib32-mesa
sudo pacman -S lib32-vulkan-radeon
sudo pacman -S lib32-vulkan-icd-loader
sudo pacman -S lib32-nvidia-utils
```

```bash
GUI (or use pamac install --no-confirm)

steam
```

```bash
mkdir /home/ductran/.local/share/Steam/steamapps/compatdata
ln -s /home/ductran/.local/share/Steam/steamapps/compatdata /mnt/disk3/Game/Steam/steamapps/compatdata
mkdir "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0"
ln -s "/home/ductran/.local/share/Steam/steamapps/common/Proton 7.0" "/mnt/disk3/Game/Steam/steamapps/common/Proton 7.0"
mkdir /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier
ln -s /home/ductran/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier /mnt/disk3/Game/Steam/steamapps/common/SteamLinuxRuntime_soldier
```

Run game witch "prime-run %command%" argument

*  Optional

```bash
sudo pacman -S nvidia-dkms
sudo pacman -S linux-zen linux-zen-headers
```

```bash
mkdir /home/ductran/.local/share/Steam/compatibilitytools.d
tar -xf Proton-6.12-GE-1.tar.gz -C ~/.steam/root/compatibilitytools.d/
```

# Flutter (optional)
```bash
GUI (or use pamac install --no-confirm)

flutter
android-sdk
android-sdk-platform-tools
android-sdk-build-tools
android-sdk-cmdline-tools-latest
android-platform
```

```bash
sudo groupadd flutterusers
sudo gpasswd -a $USER flutterusers
sudo chown -R $USER:flutterusers /opt/flutter
sudo chmod -R g+w /opt/flutter/
```

```bash
sudo groupadd android-sdk
sudo gpasswd -a $USER android-sdk
sudo chown -R $USER:android-sdk /opt/android-sdk
sudo chmod -R g+w /opt/android-sdk
```

```bash
sudo nano ~/.xprofile

#Java
export JAVA_HOME='/usr/lib/jvm/java-14-openjdk/'

# Android SDK
export ANDROID_SDK_ROOT='/opt/android-sdk'
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/"

# Flutter
export PATH="$PATH:/opt/flutter/bin"
```
```bash
flutter doctor
flutter doctor --android-licenses
```

# MariaDB (optional)
```bash
GUI (or use pamac install --no-confirm)

mariadb
```

```bash
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb
sudo mysql_secure_installation
```

- [MariaDB - ArchWiki](https://wiki.archlinux.org/index.php/MariaDB)

# PostgreSQL (optional)
```bash
GUI (or use pamac install --no-confirm)
postgresql
libxcrypt-compat
libffi6
azuredatastudio-bin
```

```bash
sudo su postgres -l
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
sudo systemctl start postgresql
sudo su postgres -l
createuser --interactive --pwprompt
exit
sudo systemctl stop postgresql
sudo systemctl disable postgresql
```

SSL config:

```bash
sudo su postgres -l
cd /var/lib/postgres/data

openssl req -new -x509 -days 3650 -nodes -text -out server.crt -keyout server.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx server.key

openssl req -new -nodes -text -out root.csr -keyout root.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx root.key

openssl x509 -req -in root.csr -text -days 3650 -extfile /etc/ssl/openssl.cnf -extensions v3_ca -signkey root.key -out root.crt

openssl req -new -nodes -text -out server.csr -keyout server.key -subj "/CN=ec2-13-212-89-13.ap-southeast-1.compute.amazonaws.com"
chmod og-rwx server.key

openssl x509 -req -in server.csr -text -days 3650 -CA root.crt -CAkey root.key -CAcreateserial -out server.crt
```

- [PostgreSQL - ArchWiki](https://wiki.archlinux.org/index.php/PostgreSQL)

# MS SQL Server (optional)
- Download https://www.openldap.org/software/download/OpenLDAP/gpg-pubkey.txt

```bash
gpg --import ./gpg-pubkey.txt
```

```bash
GUI (or use pamac install --no-confirm)

mssql-server
```

```bash
sudo /opt/mssql/bin/mssql-conf setup
sudo systemctl stop mssql-server
sudo systemctl disable mssql-server
```

- [Installation guidance for SQL Server on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver15)

# MongoDB (optional)
```bash
GUI (or use pamac install --no-confirm)

mongodb-bin
```

```bash
sudo systemctl start mongodb
mongosh
use admin
```

```bash
db.createUser(
  {
    user: "ductran",
    pwd: "Password",
    roles: [ { role: 'root', db: 'admin' }, "readWriteAnyDatabase" ]
  }
)
```

```bash
sudo nano /etc/mongodb.conf
```

```bash
security:
  authorization: "enabled"
```

```bash
sudo systemctl stop mongodb
sudo systemctl disable mongodb
```

# Mono (optional)
```bash
GUI (or use pamac install --no-confirm)

mono
mono-tools
mono-addins
mono-msbuild
mono-msbuild-sdkresolver
xsp
```

* config to run ASP .NET project

```bash
sudo mkdir /etc/mono/registry
sudo chmod uog+rw /etc/mono/registry

edit web.config
<dependentAssembly>
    <assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
    <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="4.2.0.0" />
</dependentAssembly>
```

- [Access to the path “/etc/mono/registry” is denied](https://stackoverflow.com/questions/24872394/access-to-the-path-etc-mono-registry-is-denied)
- [How to get Swashbuckle / Swagger working on the Mono (.NET) Framework?](https://stackoverflow.com/questions/36062557/how-to-get-swashbuckle-swagger-working-on-the-mono-net-framework)


# Redis (optional)
```bash
GUI (or use pamac install --no-confirm)

redis
```

# RabbitMQ (optional)
```bash
GUI (or use pamac install --no-confirm)

rabbitmq
```

# Apache (optional)
```bash
GUI (or use pamac install --no-confirm)

apache
```

# Nginx (optional)
```bash
GUI (or use pamac install --no-confirm)

nginx
```

```bash
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo mkdir /etc/nginx/conf.d
sudo cp nginx.conf /etc/nginx/nginx.conf
sudo cp 127.0.0.1.conf /etc/nginx/conf.d/127.0.0.1.conf
```

# FTP Server (optional)
```bash
GUI (or use pamac install --no-confirm)

vsftpd
filezilla
```

# iBus (optional)
```bash
GUI (or use pamac install --no-confirm)

ibus ibus-pinyin ibus-unikey ibus-bamboo

Edit ibus-bamboo PKGBUILD pkgver=0.6.7 sha256sums=('SKIP')
```

* config

```bash
sudo nano ~/.xprofile

export INPUT_METHOD=ibus
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
```

```bash
sudo nano /usr/share/applications/ibus-daemon.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx

sudo nano /usr/share/applications/ibus-daemon-kimpanel.desktop

[Desktop Entry]
Name=IBus Daemon
Exec=ibus-daemon -drx --panel=/usr/lib/kimpanel-ibus-panel

sudo cp /usr/share/applications/ibus-daemon.desktop /etc/xdg/autostart/
```

# KeepassXC (optional)
```bash
GUI (or use pamac install --no-confirm)

keepassxc

create database ~/Passwords with key ~/Passwords
```

```bash
nano ~/.xprofile

export PATH="$PATH:/home/ductran/bin"
```

```bash
copy /mnt/disk2/Projects/linux-config/files/applications/ to ~/.local/share/applications/
copy /mnt/disk2/Projects/linux-config/files/bin/ to ~/bin/
```

```bash
Add keepassxc-startup, keepassxc-watch to autostart scripts
```

- [Automatically unlock KeepassXC on startup and after lock screen](https://grabski.me/tech,/linux/2020/09/02/automatically-unlock-keepassxc-on-startup-and-after-lock-screen/)

# PulseEffects (optional)
```bash
GUI (or use pamac install --no-confirm)

pulseeffects

start PulseEffects
Audio Gain => max
```

- [Community Presets](https://github.com/wwmm/pulseeffects/wiki/Community-presets)
- [How to mimic Dolby Audio Premium on Linux with PulseEffects](https://www.linuxupdate.co.uk/2020/10/19/how-to-mimic-dolby-audio-premium-on-linux-with-pulseeffects/)

# Chinese typographic font (optional)
```bash
sudo pacman -S noto-fonts-sc noto-fonts-tc
sudo pacman -S ttf-arphic-ukai ttf-arphic-uming
```

# GRUB Backlight Fix (optinal)

```bash
sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="acpi_backlight=video loglevel=3 quiet"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

# GRUB Theme

*  Theme 1

```bash
sudo nano /etc/default/grub

GRUB_GFXMODE=1920x1080x32
```

```bash
git clone https://github.com/vinceliuice/grub2-themes.git
cd grub2-themes
sudo ./install.sh -b -t tela
```

- [vinceliuice/grub2-themes](https://github.com/vinceliuice/grub2-themes)

*  Theme 2

```bash
yay -S grub2-theme-archxion
sudo nano /etc/default/grub

Replace #GRUB_THEME="/path/to/gfxtheme" by GRUB_THEME="/boot/grub/themes/Archxion/theme.txt"

sudo grub-mkconfig -o /boot/grub/grub.cfg
```

- [Generator/Grub2-themes](https://github.com/Generator/Grub2-themes)


# SSHFS

```bash
sshfs arch@aws:/home/arch /mnt/aws
sshfs debian@oracle:/home/debian /mnt/oracle
```

# Fix KDE bug
*  Fix shortcut bug

```bash
sudo nano ~/.config/plasma-org.kde.plasma.desktop-appletsrc
sudo nano ~/.config/kglobalshortcutsrc

Change Alt+F1 => Meta+Alt+F1
```

# Fix menu height (optional)
```bash
sudo nano /usr/share/plasma/plasmoids/org.kde.plasma.kickoff/contents/ui/FullRepresentation.qml

Layout.minimumHeight: implicitHeight * 1.5
```

#  Fix GTK app UI in Wayland (optional)
```bash
gsettings set org.gnome.desktop.interface gtk-theme Aritim-Dark
gsettings set org.gnome.desktop.interface icon-theme ePapirus
gsettings set org.gnome.desktop.interface cursor-theme breeze_cursors
gsettings set org.gnome.desktop.interface font-name "Noto Sans,  10"
```

# Fix AMD GPU (optional)
```bash
nano /etc/mkinitcpio.conf

MODULES=(amdgpu)
```

```bash
mkinitcpio -p linux
```
