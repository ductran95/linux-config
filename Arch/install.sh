#!/bin/bash
set -ex

install-wifi() {
    echo "Connecting Wifi"

    rmmod pcspkr
    iwctl device list
    iwctl adapter phy0 set-property Powered on
    iwctl station wlan0 scan
    iwctl station wlan0 get-networks
    iwctl station wlan0 connect "$1" --passphrase "$2"

    ip a

    systemctl stop reflector
    systemctl disable reflector

    echo "Wifi Connected"
}

install-prepare() {
    #reflector --age 12 --country "Vietnam,Cambodia,Singapore" --protocol http,https --sort rate --save /etc/pacman.d/mirrorlist
    echo 'Server = https://mirrors.nguyenhoang.cloud/archlinux/$repo/os/$arch' | tee /etc/pacman.d/mirrorlist
    echo 'Server = https://mirror.kirbee.tech/archlinux/$repo/os/$arch' | tee /etc/pacman.d/mirrorlist
    echo 'Server = https://mirror.tvduc95.ovh/archlinux/$repo/os/$arch' | tee /etc/pacman.d/mirrorlist
    echo 'Server = http://mirror.bizflycloud.vn/archlinux/$repo/os/$arch' | tee -a /etc/pacman.d/mirrorlist
    echo 'Server = http://ossmirror.mycloud.services/os/linux/archlinux/$repo/os/$arch' | tee -a /etc/pacman.d/mirrorlist
    echo 'Server = https://mirror.sg.gs/archlinux/$repo/os/$arch' | tee -a /etc/pacman.d/mirrorlist
    echo 'Server = https://mirror.jingk.ai/archlinux/$repo/os/$arch' | tee -a /etc/pacman.d/mirrorlist
    sed -i '/#ParallelDownloads/s/^#//g' /etc/pacman.conf
    sed -i 's/#VerbosePkgLists/DisableDownloadTimeout/g' /etc/pacman.conf

    pacman -Syyy
}

install-partition() {
    echo "Formating Partition y/n"
    read -r confirm

    if [[ $confirm != 'y' ]]; then
        return 1
    fi

    mkfs.fat -F 32 $1
    mkfs.ext4 $2
    mount $2 /mnt
    lsblk

    echo "Format Partition completed"
}

install-linux() {
    echo "Installing Linux"

    pacstrap /mnt base base-devel linux linux-firmware linux-headers nano amd-ucode
    genfstab -U /mnt >> /mnt/etc/fstab

    echo "Install Linux completed"
}

install-localize() {
    echo "Updating localization"

    ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
    hwclock --systohc --localtime
    echo -e 'en_US.UTF-8 UTF-8\nen_SG.UTF-8 UTF-8\nvi_VN UTF-8' | tee -a /etc/locale.gen
    locale-gen
    echo -e 'LANG=en_SG.UTF-8\nLANGUAGE=en_SG:en\nLC_TIME=vi_VN.UTF-8\nLC_ADDRESS=C\nLC_COLLATE=C\nLC_MONETARY=C\nLC_NUMERIC=C' | tee -a /etc/locale.conf
    echo 'legion5' | tee -a /etc/hostname
    echo -e '127.0.0.1  localhost\n::1  localhost\n127.0.1.1    legion5.localdomain	legion5' | tee -a /etc/hosts

    echo "Update localization completed"
}

install-grub() {
    echo "Creating GRUB"

    pacman -S --needed --noconfirm grub efibootmgr os-prober ntfs-3g networkmanager network-manager-applet wpa_supplicant dialog mtools dosfstools bluez bluez-utils openssh sshfs git exfatprogs unzip wget
    systemctl enable NetworkManager
    systemctl enable bluetooth
    systemctl enable sshd
    mkdir /boot/EFI
    mount $1 /boot/EFI
    grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB
    grub-mkconfig -o /boot/grub/grub.cfg

    echo "Create GRUB completed"
}

install-user() {
    echo "Creating user"

    useradd -m -g users -G wheel,storage,network,power ductran
    passwd -d ductran
    sed -i '/# %wheel ALL=(ALL:ALL) ALL/s/^# //g' /etc/sudoers
    sed -i '/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

    echo "Create user completed"
}

install-cpu-power() {
    echo "Installing CPU Power"

    pacman -S --needed --noconfirm cpupower
    echo -e 'governor='conservative'\nmin_freq="1.4GHz"' | tee -a /etc/default/cpupower
    systemctl enable cpupower

    echo "Install CPU Power completed"
}

install-disable-speaker() {
    echo "Disable Speaker"

    echo 'blacklist pcspkr' | tee -a /etc/modprobe.d/nobeep.conf
    # echo -e 'blacklist kvm_intel\nblacklist kvm_amd\nblacklist kvm' | tee -a /etc/modprobe.d/nokvm.conf

    sed -i '/#GRUB_DISABLE_OS_PROBER/s/^#//g' /etc/default/grub
    sed -i 's/loglevel=3 quiet/loglevel=3 amd_pstate=passive/g' /etc/default/grub

    sed -i '/#MAKEFLAGS/s/^#//g' /etc/makepkg.conf
    sed -i 's/MAKEFLAGS="-j2"/MAKEFLAGS="-j16"/g' /etc/makepkg.conf

    sed -i '/#DefaultTimeoutStartSec/s/^#//g' /etc/systemd/system.conf
    sed -i '/#DefaultTimeoutStopSec/s/^#//g' /etc/systemd/system.conf
    sed -i '/#DefaultDeviceTimeoutSec/s/^#//g' /etc/systemd/system.conf

    echo "Disable Speaker completed"
}

install-disable-nouveau() {
    echo "Disable Nouveau"

    echo 'blacklist nouveau' | tee -a /etc/modprobe.d/blacklist-nouveau.conf
    echo 'ptions nouveau modeset=0' | tee -a /etc/modprobe.d/blacklist-nouveau.conf
    echo 'ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c0330", ATTR{power/control}="auto", ATTR{remove}="1"' | tee -a /etc/udev/rules.d/00-remove-nvidia.rules
    echo 'ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c8000", ATTR{power/control}="auto", ATTR{remove}="1"' | tee -a /etc/udev/rules.d/00-remove-nvidia.rules
    echo 'ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x040300", ATTR{power/control}="auto", ATTR{remove}="1"' | tee -a /etc/udev/rules.d/00-remove-nvidia.rules
    echo 'ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x03[0-9]*", ATTR{power/control}="auto", ATTR{remove}="1"' | tee -a /etc/udev/rules.d/00-remove-nvidia.rules

    echo "Disable Nouveau completed"
}

install-xorg() {
    echo "Installing Xorg"

    pacman -S --needed --noconfirm xorg
    pacman -S --needed --noconfirm xf86-video-amdgpu 
    # pacman -S --needed --noconfirm xf86-video-nouveau

    echo "Install Xorg completed"
}

install-mesa() {
    echo "Installing Mesa"

    pacman -S --needed --noconfirm vulkan-radeon
    pacman -S --needed --noconfirm mesa mesa-vdpau libva-mesa-driver

    echo "Install Mesa completed"
}

install-sound() {
    echo "Installing Alsa & Pipewire"

    pacman -S --needed --noconfirm alsa-firmware alsa-utils alsa-plugins
    pacman -S --needed --noconfirm pipewire pipewire-alsa pipewire-pulse pipewire-jack

    echo "Install Alsa & Pipewire completed"
}

install-sddm() {
    echo "Installing SDDM"

    pacman -S --needed --noconfirm sddm

    # mkdir -p /etc/systemd/system/sddm.service.d
    # echo -e '[Service]\nTimeoutStopSec=5s' | tee -a /etc/systemd/system/sddm.service.d/override.conf

    echo "Install SDDM completed"
}

install-grub-theme() {
    echo "Installing GRUB Theme"

    cd /tmp
    git clone --depth=1 https://github.com/vinceliuice/grub2-themes.git
    cd grub2-themes
    ./install.sh -b -t tela
    echo "Install GRUB Theme completed"
}

install-kde() {
    echo "Installing KDE Plasma"

    pacman -S --needed --noconfirm plasma sddm-kcm
    pacman -S --needed --noconfirm plasma-wayland-session
    pacman -S --needed --noconfirm colord-kde gwenview kamera kcolorchooser kdegraphics-thumbnailers kolourpaint kruler okular spectacle svgpart
    pacman -S --needed --noconfirm ffmpegthumbs
    pacman -S --needed --noconfirm dolphin kcron ksystemlog partitionmanager
    pacman -S --needed --noconfirm ark filelight kate kcharselect kdialog kgpg konsole kwalletmanager markdownpart yakuake
    pacman -S --needed --noconfirm dolphin-plugins kompare
    pacman -S --needed --noconfirm krdc krfb kdeconnect kcalc ksysguard
    pacman -S --needed --noconfirm krita elisa kwave kalzium okteta krename
    # pacman -S --needed --noconfirm kalendar korganizer kmail knotes
    pacman -S --needed --noconfirm xdg-desktop-portal
    pacman -S --needed --noconfirm xdg-desktop-portal-gtk
    
    systemctl enable sddm

    echo "Install KDE Plasma completed"
}

install-app() {
    echo "Installing Application"

    pacman -S --needed --noconfirm firefox
    pacman -S --needed --noconfirm psensor
    pacman -S --needed --noconfirm vivaldi vivaldi-ffmpeg-codecs
    pacman -S --needed --noconfirm telegram-desktop discord caprine mpv calibre grub-customizer piper qbittorrent fastfetch htop powertop openvpn networkmanager-openvpn
    pacman -S --needed --noconfirm subtitleeditor lrzip lzip p7zip zip unarchiver freerdp libgdiplus gst-libav gst-plugins-base gst-plugins-good gst-plugin-pipewire
    # pacman -S --needed --noconfirm filezilla
    pacman -S --needed --noconfirm libreoffice-fresh
    pacman -S --needed --noconfirm strawberry
    pacman -S --needed --noconfirm iperf3
    pacman -S --needed --noconfirm bitwarden

    # Font
    pacman -S --needed --noconfirm noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
    pacman -S --needed --noconfirm ttf-fira-code ttf-cascadia-code
    pacman -S --needed --noconfirm ttf-roboto ttf-dejavu ttf-droid ttf-inconsolata ttf-indic-otf
    pacman -S --needed --noconfirm terminus-font
    pacman -S --needed --noconfirm adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts
    pacman -S --needed --noconfirm adobe-source-han-sans-tw-fonts adobe-source-han-serif-tw-fonts
    pacman -S --needed --noconfirm wqy-microhei wqy-zenhei wqy-bitmapfont
    pacman -S --needed --noconfirm opendesktop-fonts
    pacman -S --needed --noconfirm powerline-fonts awesome-terminal-fonts
    pacman -S --needed --noconfirm nerd-fonts

    # Fcitx
    pacman -S --needed --noconfirm fcitx5-im
    pacman -S --needed --noconfirm fcitx5-chinese-addons fcitx5-unikey

    # zsh
    pacman -S --needed --noconfirm zsh
    pacman -S --needed --noconfirm kitty
    pacman -S --needed --noconfirm alacritty

    # Rice
    pacman -S --needed --noconfirm kvantum papirus-icon-theme

    # IDE
    pacman -S --needed --noconfirm clang llvm lldb lld libc++
    pacman -S --needed --noconfirm go go-tools
    pacman -S --needed --noconfirm rust
    pacman -S --needed --noconfirm zig
    pacman -S --needed --noconfirm dotnet-host dotnet-sdk aspnet-runtime aspnet-targeting-pack
    pacman -S --needed --noconfirm dotnet-sdk-6.0 aspnet-runtime-6.0 aspnet-targeting-pack-6.0
    pacman -S --needed --noconfirm jdk-openjdk
    pacman -S --needed --noconfirm android-tools
    pacman -S --needed --noconfirm codeblocks
    pacman -S --needed --noconfirm neovim
    pacman -S --needed --noconfirm sqlitebrowser
    pacman -S --needed --noconfirm docker
    pacman -S --needed --noconfirm docker-compose
    usermod -aG docker ductran

    # Util
    pacman -S --needed --noconfirm lazygit

    echo "Install Application completed"
}

install-yay() {
  echo "Installing yay"

    cd /tmp
    git clone --depth=1 https://aur.archlinux.org/yay-bin.git
    cd yay-bin/
    makepkg -si --noconfirm PKGBUILD
    yay --editmenu --nodiffmenu --save

    echo "Install yay completed"
}

install-aur() {
    echo "Installing aur"

    gpg --recv-keys BF38D4D02A328DFF

    yay -S --needed --noconfirm mailspring
    yay -S --needed --noconfirm google-chrome rar
    yay -S --needed --noconfirm floorp-bin
    yay -S --needed --noconfirm brave-bin
    yay -S --needed --noconfirm stellarium-bin
    yay -S --needed --noconfirm betterbird-bin
    yay -S --needed --noconfirm jellyfin-media-player
    yay -S --needed --noconfirm jellyfin-mpv-shim
    yay -S --needed --noconfirm filecentipede-bin

    # AMD Zen
    # yay -S --needed --noconfirm zenpower-dkms
    # yay -S --needed --noconfirm zenmonitor

    # Rice
    # yay -S --needed --noconfirm lightly-qt

    # IDE
    yay -S --needed --noconfirm gitflow-avh gitkraken git-credential-manager-core-bin
    yay -S --needed --noconfirm fnm-bin
    yay -S --needed --noconfirm visual-studio-code-bin
    yay -S --needed --noconfirm azuredatastudio-bin
    yay -S --needed --noconfirm k6-bin
    yay -S --needed --noconfirm mongodb-tools-bin mongosh-bin mongodb-compass
    yay -S --needed --noconfirm redis-insight-bin
    yay -S --needed --noconfirm postman-bin
    yay -S --needed --noconfirm lazydocker-bin
    yay -S --needed --noconfirm webstorm-jre datagrip-jre goland-jre

    yay -S --needed rider

    echo "Install aur completed"
}

install-kde-config() {
    echo "Installing KDE config"

    # Git
    git config --global user.name "Duc Tran"
    git config --global user.email tv.duc95@gmail.com
    git config --global core.editor "nano -w"
    git config --global core.autocrlf input
    git config --global --add safe.directory '*'
    git-credential-manager configure
    git config --global credential.credentialStore secretservice
    
    # Env
    mkdir ~/.config/environment.d
    echo -e 'EDITOR=nano\nGTK_USE_PORTAL=1\nPATH="$PATH:/home/ductran/.dotnet/tools"\nINPUT_METHOD=fcitx\nXMODIFIERS=@im=fcitx' | tee -a ~/.config/environment.d/envvars.conf

    # FNM
    cd /tmp
    echo 'eval "$(fnm env --use-on-cd)"' >> ~/.bashrc
    echo 'eval "$(fnm env --use-on-cd)"' >> ~/.zshrc
    fnm use --install-if-missing 22
    fnm use --install-if-missing 20
    fnm use --install-if-missing 18

    sudo cp /opt/rider/bin/rider.svg /usr/share/pixmaps/rider.svg

    # Mount
    sudo mkdir /mnt/disk1
    sudo mkdir /mnt/disk2
    sudo mkdir /mnt/disk3
    sudo mkdir /mnt/disk4

    echo 'UUID=964291BB4291A113   /mnt/disk1  ntfs         defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=CE45-2496          /mnt/disk2  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=F252-07F4          /mnt/disk3  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo 'UUID=4437-86A5          /mnt/disk4  exfat        defaults,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab
    echo "Install KDE config completed"
}

install-rice() {
    echo "Installing rice"

    cd ~

    sed -i 's/Color=54,97,132/Color=19,209,61/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'
    sed -i 's/Blur=true/Blur=false/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'
    sed -i 's/Opacity=0.8/Opacity=1/g' ~/.local/share/konsole/'Aritim Dark.colorscheme'

    sed -i 's/activeBackground=20,26,33,216/activeBackground=20,26,33,255/g' ~/.local/share/color-schemes/AritimDark.colors
    sed -i 's/inactiveForeground=102,106,115,216/inactiveForeground=102,106,115,255/g' ~/.local/share/color-schemes/AritimDark.colors

    rm -f ~/.local/share/plasma/desktoptheme/Aritim-Dark-Flat-Solid/widgets/clock.svgz

    # Font Config
    mkdir -p ~/.config/fontconfig/conf.d/
    curl -o ~/.config/fontconfig/conf.d/99-cjk-noto.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/fontconfig/conf.d/99-cjk-noto.conf

    # MPV
    mkdir -p ~/.config/mpv
    curl -o ~/.config/mpv/input.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/mpv/input.conf
    curl -o ~/.config/mpv/mpv.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/mpv/mpv.conf

    # Konsole
    mkdir -p ~/.local/share/konsole/
    curl -o ~/.local/share/konsole/'Aritim Dark.profile' -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/konsole/Aritim%20Dark.profile
    curl -o ~/.local/share/konsole/'Aritim Light.profile' -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/konsole/Aritim%20Light.profile

    # System Monitor
    mkdir -p ~/.local/share/plasma-systemmonitor/
    curl -o ~/.local/share/plasma-systemmonitor/frequency.page -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/share/plasma-systemmonitor/frequency.page

    # PipeWire
    mkdir -p ~/.config/pipewire/pipewire.conf.d/
    curl -o ~/.config/pipewire/pipewire.conf.d/pwrate.conf -L https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/config/pipewire/pipewire.conf.d/pwrate.conf
    
    echo "Install rice completed"
}

install-fix-gtk() {
    echo "Installing Fix GTK style"

    curl -OL 'https://gitlab.com/ductran95/linux-config/-/raw/master/dotfiles/window_decorations.css'
    cp -rf ~/.config/gtk-3.0/assets ~/.config/gtk-4.0
    echo "" >> ~/.config/gtk-3.0/gtk.css
    cat window_decorations.css >> ~/.config/gtk-3.0/gtk.css
    cp -f ~/.config/gtk-3.0/gtk.css ~/.config/gtk-4.0

    echo "Install Fix GTK style completed"
}

install-patch-dev() {
    echo "Patching dev tool"

    echo "Update Docker DNS"
    echo '{' | sudo tee /etc/docker/daemon.json
    echo '    "dns": ["1.1.1.1", "8.8.8.8"]' | sudo tee -a /etc/docker/daemon.json
    echo '}' | sudo tee -a /etc/docker/daemon.json
    echo "Update Docker DNS completed"

    echo "Import GPG key"
    gpg --import "/mnt/disk2/Certificates/PGP/Tran Van Duc_0x7AAECCA2_SECRET.asc"
    git config --global user.signingkey 1030E3EF7AAECCA2
    git config --global commit.gpgSign true
    echo "Import GPG key completed"

    echo "Install .NET extensions"

    dotnet tool install --global dotnet-ef
    dotnet tool install --global dnt
    dotnet new install Ardalis.CleanArchitecture.Template
    dotnet new install Clean.Architecture.Solution.Template
    dotnet new install IdentityServer4.Templates
    dotnet new install Duende.IdentityServer.Templates
    dotnet new install Avalonia.Templates
    dotnet new install HotChocolate.Templates
    dotnet new install MassTransit.Templates

    echo "Install .NET extensions completed"

    dotnet dev-certs https -ep localhost.crt --format PEM
    sudo trust anchor --store localhost.crt

    echo "Install NPM packages"

    npm install -g @angular/cli
    npm install -g vite
    npm install -g azurite

    echo "Install NPM packages completed"

    echo "Apply GitKraken patch"
    cd /mnt/disk2/Projects/GitCracken
    npm i
    npm run-script build
    sudo node dist/bin/gitcracken.js patcher
    echo "Apply GitKraken patch completed"

    echo "Patching dev tool completed"
}

install-cloud() {
    # Rclone
    sudo pacman -S --needed --noconfirm rclone
    sudo sed -i '/#user_allow_other/s/^#//g' /etc/fuse.conf

    # Media site
    sudo mkdir /mnt/media
    sudo chmod 777 -R /mnt/media
    sudo mkdir -p /mnt/cache/media
    sudo chmod 777 -R /mnt/cache/media

    rclone config create media onedrive --all

    sudo rclone --vfs-cache-mode writes --vfs-cache-max-size 30G mount media: /mnt/media --allow-other --config /home/ductran/.config/rclone/rclone.conf --cache-dir /mnt/cache/media --daemon

    wget https://gitlab.com/ductran95/linux-config/-/raw/master/Arch/rclone-media.service
    
    sudo mv ./rclone-media.service /etc/systemd/system/rclone-media.service
    sudo chmod 777 /etc/systemd/system/rclone-media.service

    sudo systemctl enable rclone-media

    # eLearning site
    sudo mkdir /mnt/eLearning
    sudo chmod 777 -R /mnt/eLearning
    sudo mkdir -p /mnt/cache/eLearning
    sudo chmod 777 -R /mnt/cache/eLearning

    rclone config create eLearning onedrive --all

    sudo rclone --vfs-cache-mode writes --vfs-cache-max-size 30G mount eLearning: /mnt/eLearning --allow-other --config /home/ductran/.config/rclone/rclone.conf --cache-dir /mnt/cache/eLearning --daemon

    wget https://gitlab.com/ductran95/linux-config/-/raw/master/Arch/rclone-eLearning.service
    
    sudo mv ./rclone-eLearning.service /etc/systemd/system/rclone-eLearning.service
    sudo chmod 777 /etc/systemd/system/rclone-eLearning.service

    sudo systemctl enable rclone-eLearning
}

case $1 in
arch)
  install-partition $2 $3
  install-linux
  cp install.sh /mnt
  ;;
boot)
  install-localize
  install-grub $2
  install-user
  install-mesa
  install-sound
  install-sddm
  install-disable-speaker
  install-disable-nouveau
  # install-cpu-power
  install-grub-theme
  ;;
wifi)
  install-wifi "$2" "$3"
  ;;
prepare)
  install-prepare
  ;;
partition)
  install-partition $2 $3
  ;;
linux)
  install-linux
  ;;
localize)
  install-localize
  ;;
grub)
  install-grub $2
  ;;
user)
  install-user
  ;;
xorg)
  install-xorg
  ;;
mesa)
  install-mesa
  ;;
sound)
  install-sound
  ;;
sddm)
  install-sddm
  ;;
app)
  install-app
  ;;
aur)
  install-aur
  ;;
kde)
  #install-xorg
  install-kde
  ;;
kde-full)
  install-prepare
  install-localize
  install-user
  install-grub $2
  install-mesa
  install-sound
  install-sddm
  install-disable-speaker
  install-disable-nouveau
  # install-cpu-power
  install-grub-theme
  #install-xorg
  install-kde
  install-app
  ;;
kde-aur)
  install-yay
  install-aur
  install-kde-config
  ;;
cloud)
  install-cloud
  ;;
patch-dev)
  install-patch-dev
  ;;
fix-gtk)
  install-fix-gtk
  ;;
rice)
  install-rice
  ;;
*)
  echo "command not found"
  ;;
esac
