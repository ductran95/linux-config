# My Legion 5 Arch Hyprland installation
[Installation guide](https://wiki.archlinux.org/index.php/Installation_guide)

# Boot Arch
* Edit boot/gub/grub.cfg: comment play 600 988 1 1319 4
*  Boot Arch Linux (x86_64)

# Copy install.sh file
```bash
mkdir /tmp/usb
mount /dev/sda1 /tmp/usb
cp /tmp/usb/install.sh ./
chmod 777 install.sh
umount /dev/sda1
```

# Prepare
```bash
./install.sh wifi <ssid> <pass>
./install.sh prepare
```

# Partition the disks
```bash
lsblk
cfdisk /dev/nvme1n1
```

* Choose Free space  
  Enter New  
  Then Write  
  Quit  
* Verify new partition:

```bash
lsblk
```

# Install Linux
```bash
./install.sh arch <boot_partition> <root_partition>
arch-chroot /mnt
chmod 777 install.sh
/install.sh boot <boot_partition>
```

# Password
```bash
passwd
passwd ductran
```

# Visudo
```bash
EDITOR=nano visudo
```

# Customize GRUB
```bash
nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="amd_pstate=passive"
GRUB_GFXMODE=1920x1080
GRUB_DISABLE_OS_PROBER=false
```

```bash
/install.sh grub-theme
```

# Update MAKEPKG
```bash
nano /etc/makepkg.conf

MAKEFLAGS="-j16"
```

# Reboot
```bash
exit
umount -a
reboot
```

* Login as user
* Connect to wifi using nmtui

# Install Hypr
```bash
/install.sh yay
/install.sh hypr
```

# App
```bash
/install.sh app
/install.sh zsh
```

# Hyprland rice [1amSimp1e](https://github.com/1amSimp1e/dots)
```bash
/install.sh hypr-rice-1
```

# GRUB
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
